package kr.amc.amis.executor;

import kr.amc.amis.entity.FooDTO;
import kr.amc.amis.entity.FooTO;
import kr.amc.amis.mapper.FooMapper;
import kr.amc.amis.service.FooService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@RequiredArgsConstructor
public class ServiceBeanExecutor implements ApplicationListener<ApplicationStartedEvent> {
    //
    private final FooService fooService;
    private final FooMapper fooMapper;
    private final ApplicationContext context;

    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {
        log.debug("Appliation Started...");
        fooService.case_4(FooDTO.sample(), FooTO.sample());
        fooMapper.insertFooTO(FooTO.sample());
        FooTO result = fooMapper.getFooTO(FooTO.sample().getId());
        log.debug("QueryResult : {}", result);
    }
}
