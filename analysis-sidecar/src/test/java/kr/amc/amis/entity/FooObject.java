package kr.amc.amis.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FooObject {
    private final String id = FooObject.class.getSimpleName();
    private String stringValue;
    private int intValue;
    private List<String> listValue;

    public static FooObject sample() {
        return new FooObject(
                "sample",
                3,
                Arrays.asList("a", "b", "c")
        );
    }
}
