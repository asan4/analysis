package kr.amc.amis.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class FooTO {
    @Id
    private final String id = FooTO.class.getSimpleName();
    private String stringValue;
    private int intValue;

    public static FooTO sample() {
        return new FooTO(
                "sample",
                3
        );
    }
}
