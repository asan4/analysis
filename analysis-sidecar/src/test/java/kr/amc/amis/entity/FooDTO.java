package kr.amc.amis.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FooDTO {
    private final String id = FooDTO.class.getSimpleName();
    private String stringValue;
    private int intValue;
    private List<String> listValue;

    public static FooDTO sample() {
        List<String> listValue = new ArrayList<>();
        listValue.add("a");
        listValue.add("b");
        listValue.add("c");
        return new FooDTO(
                "sample",
                3,
                listValue
        );
    }
}
