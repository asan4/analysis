package kr.amc.amis.mapper;

import kr.amc.amis.entity.FooDTO;
import kr.amc.amis.entity.FooTO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FooMapper {

    void insertFooTO(FooTO to);
    FooTO getFooTO(String id);
}
