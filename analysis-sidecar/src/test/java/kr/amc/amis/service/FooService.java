package kr.amc.amis.service;

import kr.amc.amis.entity.FooDTO;
import kr.amc.amis.entity.FooObject;
import kr.amc.amis.entity.FooTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class FooService implements Foo {
    @Autowired
    @Lazy
    private FooService self;

    /**
     * () => any
     * @workflow case-1
     * @description <--EOT
     * 파라미터가 존재하지 않고,
     * 리턴타입이 있는 경우
     * EOT
     */
    public FooObject case_1() {
        log.debug("case_1 invoked...");
        return FooObject.sample();
    }

    /**
     * () => void
     * @workflow case-2
     * @description 파라미터와 리턴 타입이 모두 존재하지 않는 경우
     */
    public void case_2() {
        log.debug("case_2 invoked...");
    }

    /**
     * any => void
     * @workflow case-3
     */
    public void case_3(String payload) {
        log.debug("case_3 invoked...");
        log.debug("case_3 payload : {}", payload);
    }

    /**
     * any' => any''
     * @workflow case-4
     */
    public List<FooTO> case_4(FooDTO dto, FooTO to) {
        log.debug("case_4 invoked...");
        dto.setStringValue("Modified...");
        return Arrays.asList(FooTO.sample());
    }


    /**
     * any => state 변경
     * @workflow case-5
     */
    public void case_5(FooDTO fooDTO) {
        log.debug("case_5 invoked...");
        fooDTO.setStringValue(fooDTO.getStringValue().concat("_modified"));
        fooDTO.getListValue().addAll(Arrays.asList("add-1", "add-2"));
    }

    /**
     * exception
     */
    public void case_6(FooTO to) {
        log.debug("case_6 invoked...");
        throw new IllegalArgumentException("case6");
    }

    /**
     * private
     */
    public void case_7() {
        log.debug("case_7 invoked...{}", self.getClass().getCanonicalName());
        case_7_internal_public();
        self.case_7_internal_public();

        case_7_internal_private();
    }

    public void case_7_internal_public() {
        log.debug("case_7_internal_public invoked...{}", this.getClass().getCanonicalName());
    }

    private void case_7_internal_private() {
        log.debug("case_7_internal_private invoked...");
    }
}
