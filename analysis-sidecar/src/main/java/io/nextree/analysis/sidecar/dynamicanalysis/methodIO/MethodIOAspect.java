package io.nextree.analysis.sidecar.dynamicanalysis.methodIO;

import io.nextree.analysis.sidecar.dynamicanalysis.methodIO.data.MethodIOArgument;
import io.nextree.analysis.sidecar.dynamicanalysis.methodIO.data.MethodIOPoint;
import io.nextree.analysis.sidecar.dynamicanalysis.methodIO.function.MethodIOPointConsumer;
import io.nextree.analysis.sidecar.dynamicanalysis.methodIO.function.MethodIODeserializer;
import io.nextree.analysis.sidecar.dynamicanalysis.methodIO.function.MethodIOSerializer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import java.util.Arrays;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;


@Slf4j
@Aspect
@RequiredArgsConstructor
public class MethodIOAspect {
    private final MethodIOPointConsumer methodIOPointConsumer;
    private final MethodIOSerializer methodIOSerializer;
    private final MethodIODeserializer methodIODeserializer;


//    @Pointcut("@target(org.springframework.stereotype.Service) && call(* *(..))")


    @Pointcut("execution(* kr.amc..*(..))")
    public void amisPackage() {}

    @Pointcut("@target(org.springframework.stereotype.Service)")
    public void serviceMethods() {}

    // TODO pointcut refactoring
//    @Pointcut("@within(org.apache.ibatis.annotations.Mapper)")
    @Pointcut("execution(* kr.amc..*Mapper.*(..))")
    public void mapperMethods() {}

    @Pointcut("amisPackage() && (serviceMethods() || mapperMethods())")
    public void condition(){}

    @Around("condition()")
    public Object around(ProceedingJoinPoint jp) throws Throwable {
        String threadKey = Thread.currentThread().getName();
        log.debug("MethodAspect start....., {}", threadKey);

        // Before
        MethodIOPoint methodIOPoint = new MethodIOPoint((MethodSignature) jp.getSignature(), jp.getSourceLocation());
        methodIOPoint.setInput(
                Arrays.stream(jp.getArgs())
                        .map(arg -> new MethodIOArgument(arg, methodIOSerializer, methodIODeserializer))
                        .collect(Collectors.toList())
        );


        Object result = null;
        // After
        try {
            result = jp.proceed();
            Optional.ofNullable(result).ifPresent(r -> methodIOPoint.setOutput(new MethodIOArgument(r, methodIOSerializer, methodIODeserializer)));
            methodIOPoint.genChangedInput(
                    Arrays.stream(jp.getArgs())
                            .map(arg -> new MethodIOArgument(arg, methodIOSerializer, methodIODeserializer))
                            .collect(Collectors.toList())
            );
        } catch (Throwable t) {
            methodIOPoint.setThrowable(t);
            throw t;
        } finally {
            methodIOPoint.setEndTime(new Date().getTime());
        }

        methodIOPointConsumer.accept(methodIOPoint);
        log.debug("MethodAspect end.....");

        if (Optional.ofNullable(methodIOPoint.getThrowable()).isPresent()) {
            throw methodIOPoint.getThrowable();
        } else {
            return result;
        }
    }

}
