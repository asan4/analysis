package io.nextree.analysis.sidecar.dynamicanalysis.methodIO.function;

import io.nextree.analysis.sidecar.dynamicanalysis.methodIO.data.MethodIOPoint;
import io.nextree.analysis.sidecar.dynamicanalysis.methodIO.data.MethodIOPointKey;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;
import java.util.stream.Stream;

@Slf4j
@RequiredArgsConstructor
public class DefaultMethodIOPointConsumer implements MethodIOPointConsumer {
    private final MethodIOSerializer serializer;
    private final Path logPath;

    @Override
    public void accept(MethodIOPoint point) {
        log.debug("Point :\n{}", toJson(point));
        if (logPath != null) {
            writeAsFile(point);
            log.debug("log is stored ... {}", logPath);
        }
    }

    private String toJson(MethodIOPoint point) {
        return Optional.ofNullable(this.serializer.serialize(point))
                .orElse("Not Serializable");
    }

    private void writeAsFile(MethodIOPoint point) {
        String log = toJson(point);
        String fileName = Stream.of(point.getThread(), String.valueOf(point.getStartTime()), MethodIOPointKey.fromKeyString(point.getKey()).map(MethodIOPointKey::toSimpleKeyString).orElse(point.getKey()))
                .map(token -> String.format("[%s]", token))
                .reduce((a,b) -> a.concat(b))
                .orElse("")
                .concat(".json");

        String filePath = logPath.endsWith(File.separator) ? logPath.toAbsolutePath().toString().concat(fileName) : String.join(File.separator, logPath.toAbsolutePath().toString(), fileName);
        try(FileOutputStream fos = new FileOutputStream(filePath)) {
            fos.write(log.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
