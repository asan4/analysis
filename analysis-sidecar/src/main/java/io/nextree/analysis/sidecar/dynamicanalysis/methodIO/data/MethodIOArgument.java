package io.nextree.analysis.sidecar.dynamicanalysis.methodIO.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.nextree.analysis.sidecar.dynamicanalysis.methodIO.function.MethodIODeserializer;
import io.nextree.analysis.sidecar.dynamicanalysis.methodIO.function.MethodIOSerializer;
import lombok.Data;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

@Data
public class MethodIOArgument {
    private final String type;
    private final Object value;
    @JsonIgnore
    private long checksum;

    public MethodIOArgument(Object object, MethodIOSerializer serializer, MethodIODeserializer deserializer) {
        if (Optional.ofNullable(object).isPresent()) {
            this.type = object.getClass().getCanonicalName();
            this.value = Optional.ofNullable(deepClone(object, serializer, deserializer))
                    .orElse("Not Serializable");
        } else {
            this.type = "Null";
            this.value = null;
        }
    }

    @Nullable
    private <T> T deepClone(@NonNull T object, MethodIOSerializer serializer, MethodIODeserializer deserializer){
        return Optional.ofNullable(serializer.serialize(object))
                .map(json -> {
                    this.checksum = getCRC32Checksum(json);
                    return deserializer.deserialize(json, (Class<T>) object.getClass());
                })
                .orElse(null);
    }

    private long getCRC32Checksum(String json) {
        byte[] bytes = json.getBytes(StandardCharsets.UTF_8);
        Checksum crc32 = new CRC32();
        crc32.update(bytes, 0, bytes.length);
        return crc32.getValue();
    }

    public boolean logicalEquals(MethodIOArgument argument) {
        return checksum == argument.getChecksum();
    }
}