package io.nextree.analysis.sidecar.dynamicanalysis.methodIO.function;

import io.nextree.analysis.sidecar.dynamicanalysis.methodIO.data.MethodIOPoint;

import java.util.function.Consumer;

public interface MethodIOPointConsumer extends Consumer<MethodIOPoint> {}