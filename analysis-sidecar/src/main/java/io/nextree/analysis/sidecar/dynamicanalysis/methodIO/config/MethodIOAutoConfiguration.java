package io.nextree.analysis.sidecar.dynamicanalysis.methodIO.config;

import io.nextree.analysis.sidecar.dynamicanalysis.methodIO.MethodIOAspect;
import io.nextree.analysis.sidecar.dynamicanalysis.methodIO.function.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@Configuration
@EnableAspectJAutoProxy
@ConditionalOnProperty(prefix = "io.nextree.analysis.sidecar.methodIO", name = "enable", havingValue = "true")
public class MethodIOAutoConfiguration {
    @Bean
    public Path methodIOLogPath(
            @Value("${io.nextree.analysis.sidecar.methodIO.logPath:logs/methodIO}") String logPath
    ) throws IOException {
        String refinedPath = logPath.replaceAll("\\\\", File.separator).replaceAll("/", File.separator).trim();
        Path path = Paths.get(refinedPath).toAbsolutePath();
        log.debug("logPath : {}", path.toString());
        if (!Files.exists(path)) {
            Files.createDirectories(path);
        }
        return path;
    }

    @ConditionalOnMissingBean
    @Bean
    public MethodIOSerializer methodIOSerializer() {
        return new DefaultMethodIOSerializer();
    }

    @ConditionalOnMissingBean
    @Bean
    public MethodIODeserializer methodIODeserializer() {
        return new DefaultMethodIOIDeserializer();
    }

    @ConditionalOnMissingBean
    @Bean
    public MethodIOPointConsumer methodIOPointConsumer(
            MethodIOSerializer methodIOSerializer,
            @Qualifier("methodIOLogPath") Path methodIOLogPath
    ) { return new DefaultMethodIOPointConsumer(methodIOSerializer, methodIOLogPath);}

    @Bean
    public MethodIOAspect methodIOAspect(
            MethodIOPointConsumer pointConsumer,
            MethodIOSerializer methodIOSerializer,
            MethodIODeserializer methodIODeserializer
            ) {
        log.debug("MethodIOAspect...");
        return new MethodIOAspect(pointConsumer, methodIOSerializer, methodIODeserializer);
    }
}
