package io.nextree.analysis.sidecar.dynamicanalysis.methodIO.data;

import lombok.Data;
import org.aspectj.lang.reflect.MethodSignature;
import org.aspectj.lang.reflect.SourceLocation;
import org.springframework.lang.Nullable;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Data
public class MethodIOPoint {
    private final String thread;
    private final String key;
    // TODO: call expression is not allowed in Spring... Consider LoadTimeWeaving...
//        private final String invokedLocation;
    private final long startTime;
    private long endTime;
    @Nullable
    private List<MethodIOArgument> input;
    @Nullable
    private List<MethodIOArgument> changedInput;
    @Nullable
    private MethodIOArgument output;
    @Nullable
    private Throwable throwable;

    public MethodIOPoint(MethodSignature methodSignature, SourceLocation sourceLocation) {
        this.thread = Thread.currentThread().getName();
        this.startTime = new Date().getTime();
        this.key = new MethodIOPointKey(methodSignature).toKeyString();
        // TODO: call expression is not allowed in Spring... Consider LoadTimeWeaving...
//            this.invokedLocation = sourceLocation.getWithinType().getCanonicalName().concat(":").concat(String.valueOf(sourceLocation.getLine()));
    }

    public void genChangedInput(List<MethodIOArgument> arguments) {
        if (this.input == null) {
            return;
        }
        this.changedInput = IntStream.range(0, arguments.size())
                .filter(idx -> !this.input.get(idx).logicalEquals(arguments.get(idx)))
                .mapToObj(arguments::get)
                .collect(Collectors.toList());
    }

    public boolean isInputChanged() {
        return this.input != null && !this.changedInput.isEmpty();
    }

}