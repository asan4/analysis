package io.nextree.analysis.sidecar.dynamicanalysis.methodIO.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.aspectj.lang.reflect.MethodSignature;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MethodIOPointKey {
    private static final String DELIM = "$";
    private static final String DELIM_REGEX = "\\$";
    private String className;
    private String methodName;
    private List<String> paramNames;

    public MethodIOPointKey(MethodSignature methodSignature) {
        this.className = methodSignature.getDeclaringType().getCanonicalName();
        this.methodName = methodSignature.getName();
        this.paramNames = Arrays.stream(methodSignature.getParameterTypes()).map(Class::getCanonicalName).collect(Collectors.toList());
    }

    public MethodIOPointKey(String className, String methodName) {
        this.className = className;
        this.methodName = methodName;
        this.paramNames = new ArrayList<>();
    }

    public String toKeyString() {
        return String.join(DELIM, this.className, this.methodName, this.paramNames.stream().reduce((a,b) -> a.concat(",").concat(b)).orElse(""));
    }

    public static Optional<MethodIOPointKey> fromKeyString(String key) {
        String[] tokens = key.split(DELIM_REGEX);

        if (tokens.length >= 2){
            String className = tokens[0];
            String methodName = tokens[1];
            MethodIOPointKey pointKey = new MethodIOPointKey(className, methodName);

            if (tokens.length == 3) {
                List<String> paramNames = Arrays.stream(tokens[2].split(",")).collect(Collectors.toList());
                pointKey.setParamNames(paramNames);
            }
            return Optional.of(pointKey);
        }

        return Optional.empty();
    }

    public String toSimpleKeyString() {
        return String.join(DELIM,
                this.className.substring(this.className.lastIndexOf(".")+1),
                this.methodName,
                this.paramNames.stream().map(paramName -> paramName.substring(paramName.lastIndexOf(".")+1))
                        .reduce((a,b) -> a.concat(",").concat(b)).orElse("")
        );
    }

    public static void main(String[] args) {
        System.out.println(Arrays.stream("a.b.c$method$a,b,c".split(DELIM_REGEX)).collect(Collectors.toList()));
    }
}