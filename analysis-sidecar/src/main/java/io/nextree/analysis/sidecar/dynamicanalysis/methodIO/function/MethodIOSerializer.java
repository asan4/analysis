package io.nextree.analysis.sidecar.dynamicanalysis.methodIO.function;

import org.springframework.lang.Nullable;

@FunctionalInterface
public interface MethodIOSerializer {
    @Nullable
    String serialize(Object object);
}
