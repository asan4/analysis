package io.nextree.analysis.sidecar.dynamicanalysis.methodIO.function;

import org.springframework.lang.Nullable;

@FunctionalInterface
public interface MethodIODeserializer {
    @Nullable
    <T> T deserialize(String json, Class<T> tClass);
}
