package io.nextree.analysis.sidecar.dynamicanalysis.methodIO.function;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class DefaultMethodIOIDeserializer implements MethodIODeserializer {
    private final ObjectMapper objectMapper;

    public DefaultMethodIOIDeserializer() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(JsonGenerator.Feature.IGNORE_UNKNOWN);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        this.objectMapper = mapper;
    }

    @Override
    public <T> T deserialize(String json, Class<T> tClass) {
        try {
            return objectMapper.readValue(json.getBytes(StandardCharsets.UTF_8), tClass);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
