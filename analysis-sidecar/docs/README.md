# Analysis-Sidecar     

```Analysis``` 프로젝트는 소스코드에 대한 정적 분석 뿐만 아니라, ```Runtime``` 환경에서 데이터의 흐름을 추적하는 ```동적 분석``` 기능도 제공합니다.     
이 모듈은 ```동적 분석``` 기능을 제공하기 위한 ```Anaylsis-Sidecar``` 모듈입니다.     

<!-- TOC -->
* [Installation](#installation)
  * [maven](#maven)
  * [gradle](#gradle)
* [Features](#features)
  * [MethodIO 분석](#methodio-분석)
    * [분석 대상](#분석-대상)
    * [분석 정보](#분석-정보)
    * [Configuration](#configuration)
      * [application.yml](#applicationyml)
      * [Serializer, Deserializer](#serializer-deserializer)
      * [MethodIOPointConsumer](#methodiopointconsumer)
  * [DTO State 추적 분석 🚧](#dto-state-추적-분석-)
<!-- TOC -->




# Installation   
> boot 모듈에 종속성을 추가합니다.    

## maven   
```xml
<dependency>
    <groupId>io.nextree</groupId>
    <artifactId>analysis-sidecar</artifactId>
    <version>0.1.0-jdk1_8-SNAPSHOT</version>
</dependency>
```

## gradle
```kotlin
implementation("io.nextree:analysis-sidecar:0.1.0-jdk1_8-SNAPSHOT");
```


# Features     

## MethodIO 분석
런타임 환경에서 호출되는 메소드의 입출력 정보를 분석합니다.    

### 분석 대상   
다음 조건을 모두 만족하는 클래스의 메소드
  - ```@Service``` 어노테이션이 붙어있는 클래스
  - ```kr.amc``` 패키지 하위에 있는 클래스    

### 분석 정보   

```java
  public class MethodIOPoint {
    private final String thread;
    private final String key;
    private final long startTime;
    private long endTime;
    @Nullable
    private List<MethodIOArgument> input;
    @Nullable
    private List<MethodIOArgument> changedInput;
    @Nullable
    private MethodIOArgument output;
    @Nullable
    private Throwable throwable;
  }
```     
- ```thread``` : 메소드가 실행된 thread key
- ```key``` : 메소드 ID ([클래스명]$[메소드명]$[파리미터명])     
- ```startTime```, ```endTime``` : 메소드 호출 시작/종료 시간    
- ```input``` : 메소드의 파라미터 정보 (타입, 값) ```파리미터가 없다면 null입니다.```   
- ```changedInput``` : 메소드의 파라미터 중 메소드 내에서 변경된 객체 정보 (타입, 값) ```변경된 input이 없다면 null입니다.```    
- ```output``` : 메소드의 리턴 정보 (타입, 값) ```void 메소드라면 null입니다.```   
- ```throwable``` : 메소드에서 발생한 예외 정보 ```예외가 발생하지 않으면 null입니다.```   


<details>
<summary>예시</summary>
<div markdown="1">       

![method.png](assets%2Fmethod.png)
![json.png](assets%2Fjson.png)




</div>
</details>

### Configuration

#### application.yml

```yaml
logging:
  level:
    io.nextree.analysis.sidecar: debug     

io.nextree:
  analysis:
    sidecar:
      methodIO:
        enable: true      # methodIO 분석 기능 on/off
```

1. sidecar 모듈의 대부분 로그 레벨은 ```debug``` 이다.
2. ```io.nextree.analysis.sidecar.methodIO``` : sidecar.methodIO 분석 모듈을 활성화 시키기 위한 설정이다.

#### Serializer, Deserializer

MethodIO 모듈에서 다루는 정보는 ```deepCopy```를 기본으로 하며, 이때 Se, Dese 모듈이 활용됩니다.     
- [MethodIOSerializer.java](..%2Fsrc%2Fmain%2Fjava%2Fio%2Fnextree%2Fanalysis%2Fsidecar%2Fdynamicanalysis%2FmethodIO%2Ffunction%2FMethodIOSerializer.java)   
- [MethodIODeserializer.java](..%2Fsrc%2Fmain%2Fjava%2Fio%2Fnextree%2Fanalysis%2Fsidecar%2Fdynamicanalysis%2FmethodIO%2Ffunction%2FMethodIODeserializer.java)

Default로 다음의 모듈이 적용됩니다.
- [DefaultMethodIOSerializer.java](..%2Fsrc%2Fmain%2Fjava%2Fio%2Fnextree%2Fanalysis%2Fsidecar%2Fdynamicanalysis%2FmethodIO%2Ffunction%2FDefaultMethodIOSerializer.java)
- [DefaultMethodIOIDeserializer.java](..%2Fsrc%2Fmain%2Fjava%2Fio%2Fnextree%2Fanalysis%2Fsidecar%2Fdynamicanalysis%2FmethodIO%2Ffunction%2FDefaultMethodIOIDeserializer.java)

아래와 같은 방법으로 Override 할 수 있습니다.    

```java
@Slf4j
@Configuration
@ConditionalOnProperty(prefix = "io.nextree.analysis.sidecar.methodIO", name = "enable", havingValue = "true")
public class OverrideMethodIOAutoConfiguration {
    @Bean
    public MethodIOSerializer overridenMethodIOSerializer() {
        //
        return object -> {
            // some logic...
        };
    }
    
    @Bean
    public MethodIODeserializer overridenMethodIODeserializer() {
        //
        return (json, clazz) -> {
            // some logic...
        };
    }
}
```

#### MethodIOPointConsumer

MethodIOPoint 객체에 대한 생성 후 호출되는 afterHook 함수입니다.
- [MethodIOPointConsumer.java](..%2Fsrc%2Fmain%2Fjava%2Fio%2Fnextree%2Fanalysis%2Fsidecar%2Fdynamicanalysis%2FmethodIO%2Ffunction%2FMethodIOPointConsumer.java)    

호출하는 모듈은 아래와 같습니다.     
- [MethodIOAspect.java:66](..%2Fsrc%2Fmain%2Fjava%2Fio%2Fnextree%2Fanalysis%2Fsidecar%2Fdynamicanalysis%2FmethodIO%2FMethodIOAspect.java)

Default로 다음의 모듈이 적용됩니다.       

- [DefaultMethodIOPointConsumer.java](..%2Fsrc%2Fmain%2Fjava%2Fio%2Fnextree%2Fanalysis%2Fsidecar%2Fdynamicanalysis%2FmethodIO%2Ffunction%2FDefaultMethodIOPointConsumer.java)

  - 지정된 Path```methodIOLogPath```와 ```MethodIOSerializer```를 통해  ```MethodIOPoint```를 json file로 출력합니다.     
  - 파일명의 Syntax는 다음과 같습니다.     
    ```bash
    [스레드명][호출시간][클래스명$메소드명$파라미터명].json
    //예시    
    [main][1679381556697][kr.amc.amis.service.FooService$case_4$kr.amc.amis.entity.FooDTO,kr.amc.amis.entity.FooTO].json      
    ```    
  - logPath는 기본적으로 ```[프로젝트 루트]/logs/methodIO``` 이며, 변경하고 하는 경우 application.yml 설정을 통해 변경할 수 있습니다.       
    ```yaml
    io.nextree:
      analysis:
        sidecar:
          methodIO:
            logPath: some/other/directory  # default : logs/methodIO
    ```

아래와 같은 방법으로 Override 할 수 있습니다.    

```java
@Slf4j
@Configuration
@ConditionalOnProperty(prefix = "io.nextree.analysis.sidecar.methodIO", name = "enable", havingValue = "true")
public class OverrideMethodIOAutoConfiguration {
    @Bean
    public MethodIOPointConsumer overridenMethodIOPointConsumer() {
        return point -> {
            log.debug("Just Log...{}", point);
        };
    }
}
```    

## DTO State 추적 분석 🚧    
DTO 별 State가 변경되는 지점에 대한 추적 분석 모듈입니다.