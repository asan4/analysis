# Analysis App 🚧    

<!-- TOC -->
* [서버 실행](#서버-실행)
    * [1. 분석 대상 소스 경로 정보 설정](#1-분석-대상-소스-경로-정보-설정)
    * [2. 클래스 중복 제거](#2-클래스-중복-제거)
    * [3. 서버 실행](#3-서버-실행)
* [프론트엔드 서버 실행](#프론트엔드-서버-실행)
<!-- TOC -->

## 서버 실행       

#### 1. 분석 대상 소스 경로 정보 설정     

> 대상 파일 : [application.yml](shared-lab%2Fsrc%2Fmain%2Fresources%2Fapplication.yml)

```yaml
analysis.targetSourceRootPath: /Users/nextree/_asan/_codes/mc-oo/mc-oo-service
```

#### 2. 클래스 중복 제거    

![class-name-duplication.png](assets%2Fclass-name-duplication.png)    


#### 3. 서버 실행     

> 대상 파일 : [App.java](shared-lab%2Fsrc%2Fmain%2Fjava%2Fio%2Fnextree%2Fapplication%2FApp.java)

- 초기 정적 분석 로그 확인    
 ![static-analysis-server-log.png](assets%2Fstatic-analysis-server-log.png)

## 프론트엔드 서버 실행    

> 대상 파일 : [package.json](front-lab%2Fpackage.json)    

- 스크립트 실행    
![static-anaylsis-front-execution.png](assets%2Fstatic-anaylsis-front-execution.png)    


- 터미널로 실행     
```bash
➜ front-lab git:(main) cd front-lab
➜ front-lab git:(main) yarn static-analysis:dev

yarn run v1.22.19
$ yarn static-analysis dev
$ yarn workspace @analysis/static-analysis dev
$ vite

  VITE v4.1.4  ready in 340 ms

  ➜  Local:   http://localhost:5173/
  ➜  Network: use --host to expose
  ➜  press h to show help
```

