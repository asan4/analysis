package io.nextree.analysis.app.watcher;

import com.github.javaparser.ParserConfiguration;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.symbolsolver.utils.SymbolSolverCollectionStrategy;
import io.nextree.analysis.staticanalysis.StaticAnalysis;
import io.nextree.analysis.staticanalysis.analyzer.StaticAnalyzer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.devtools.filewatch.ChangedFiles;
import org.springframework.boot.devtools.filewatch.FileChangeListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class MCOOWatcher implements FileChangeListener {
    private final SimpMessagingTemplate messagingTemplate;
    private final StaticAnalysis.Store store;
    private final ParserConfiguration parserConfiguration;
    private final StaticAnalyzer staticAnalyzer = StaticAnalyzer.analyze();
    @Override
    public void onChange(Set<ChangedFiles> changeSet) {
        List<CompilationUnit> changedUnits = new ArrayList<>();
        List<File> files = new ArrayList<>();
        for (ChangedFiles changedFiles : changeSet) {
            changedFiles.getFiles().forEach(f -> {
                if (!f.getFile().getName().endsWith(".java")) return;
                files.add(f.getFile());
                try {
                    StaticJavaParser.setConfiguration(parserConfiguration);
                    CompilationUnit unit = StaticJavaParser.parse(f.getFile());
                    changedUnits.add(unit);
                    log.debug(staticAnalyzer.packageAndClass(unit));
                } catch (FileNotFoundException e) {
                    log.error("cannot find file: " + f.getFile().getName());
                    e.printStackTrace();
                }
            });
        }
        store.update(changedUnits);
        messagingTemplate.convertAndSend(
                "/topic/file",
                files.stream().map(File::getName).collect(Collectors.joining(", "))
        );
    }
}
