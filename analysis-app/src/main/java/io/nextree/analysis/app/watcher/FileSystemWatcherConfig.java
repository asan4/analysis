package io.nextree.analysis.app.watcher;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.devtools.filewatch.FileSystemWatcher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.nio.file.Path;
import java.time.Duration;
@Slf4j
@Configuration
@RequiredArgsConstructor
public class FileSystemWatcherConfig {
    private final MCOOWatcher watcher;
    @Bean
    public FileSystemWatcher watch(
            @Qualifier("staticAnalysisProjectTarget") Path staticAnalysisProjectTarget
    ) {
        FileSystemWatcher fileSystemWatcher = new FileSystemWatcher(true, Duration.ofMillis(150L), Duration.ofMillis(100));
        fileSystemWatcher.addSourceDirectory(new File(staticAnalysisProjectTarget.toString()));
        fileSystemWatcher.addListener(watcher);
        fileSystemWatcher.start();
        log.debug("-----started watching-----");
        return fileSystemWatcher;
    }
}
