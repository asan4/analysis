package io.nextree.analysis.app.staticanalysis;

import io.nextree.analysis.callhierarchy.dictionary.MethodMeta;
import io.nextree.analysis.callhierarchy.qualification.QualifiedMethodSignature;
import io.nextree.analysis.callhierarchy.ref.CallMethodKey;
import io.nextree.analysis.staticanalysis.StaticAnalysis;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("static-analysis")
@RequiredArgsConstructor
public class StaticAnalysisAPI {
    private final StaticAnalysis.Configuration configuration;
    private final StaticAnalysis.Store store;

    @PostMapping("artifacts")
    public StaticAnalysis.Artifacts artifacts(@RequestBody QualifiedMethodSignature entrypoint) {
        return StaticAnalysis.start(configuration)
                .store(store)
                .callHierarchy(entrypoint)
//                .workflow(entrypoint)
//                .plantUML()
                .artifacts();
    }

    @PostMapping("reload")
    public StaticAnalysis.Artifacts reload(@RequestBody QualifiedMethodSignature entrypoint) {
        return StaticAnalysis.start(configuration)
                .store(store)
                .callHierarchy(entrypoint)
//                .workflow(entrypoint)
//                .plantUML()
                .artifacts();
    }

    @GetMapping("entrypoints/{classKey}")
    public List<QualifiedMethodSignature> entrypoints(@PathVariable("classKey") String classKey) {
        return this.store.getDictionary().findClassMeta(classKey)
                .map(callClass -> callClass.getMethods().stream()
                        .map(MethodMeta::getQualifiedMethodSignature)
                        .collect(Collectors.toList()))
                .orElse(new ArrayList<>());
    }
}
