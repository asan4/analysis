package io.nextree.analysis.app.staticanalysis.config;

import com.github.javaparser.ParserConfiguration;
import com.github.javaparser.resolution.TypeSolver;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JarTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;
import io.nextree.analysis.staticanalysis.StaticAnalysis;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Configuration
public class Config {

    @Bean
    public Path staticAnalysisProjectTarget(
            @Value("${io.nextree.analysis.app.staticAnalysis.project.target:../../mc-oo/mc-oo-service}")
            String targetSourceRootPath
    ) {
        String refinedPath = targetSourceRootPath.replaceAll("\\\\", File.separator).replaceAll("/", File.separator).trim();
        Path path = Paths.get(refinedPath).toAbsolutePath();
        log.debug("staticAnalysisProjectTarget : {}", path.toString());
        if (!Files.exists(path)) {
            throw new IllegalStateException("targetSourceRootPath is not exists.");
        }
        return path;
    }

    @Bean
    public List<Path> staticAnalysisProjectDependencies(
            @Value("${io.nextree.analysis.app.staticAnalysis.project.dependencies:}")
            String[] dependencies
    ) {
        List<Path> refinedPaths = Arrays.stream(dependencies)
                .map(path -> path.replaceAll("\\\\", File.separator).replaceAll("/", File.separator).trim())
                .map(Paths::get)
                .collect(Collectors.toList());
        log.debug("staticAnalysisProjectDependencies : {}", refinedPaths);

        return refinedPaths;
    }

    @Bean
    public ParserConfiguration staticAnalysisParserConfiguration(
            @Qualifier("staticAnalysisProjectDependencies") List<Path> staticAnalysisProjectDependencies
    ) {
        ParserConfiguration parserConfiguration = new ParserConfiguration();
        List<TypeSolver> typeSolvers = new ArrayList<>();
        typeSolvers.add(new ReflectionTypeSolver());
        typeSolvers.addAll(
                staticAnalysisProjectDependencies.stream()
                        .map(jarPath -> {
                            try {
                                return new JarTypeSolver(jarPath);
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        })
                        .collect(Collectors.toList())
        );

        CombinedTypeSolver combinedTypeSolver = new CombinedTypeSolver(typeSolvers);
        JavaSymbolSolver javaSymbolSolver = new JavaSymbolSolver(combinedTypeSolver);
        parserConfiguration.setSymbolResolver(javaSymbolSolver);
        parserConfiguration.setLanguageLevel(ParserConfiguration.LanguageLevel.JAVA_8);
        return parserConfiguration;
    }

    @Bean
    public StaticAnalysis.Configuration staticAnalysisConfiguration(
            @Qualifier("staticAnalysisProjectTarget") Path staticAnalysisProjectTarget,
            @Qualifier("staticAnalysisParserConfiguration") ParserConfiguration parserConfiguration
    ) {
        return new StaticAnalysis.Configuration(staticAnalysisProjectTarget, parserConfiguration);
    }

    @Bean
    public StaticAnalysis.Store staticAnalysisStore(
            @Qualifier("staticAnalysisConfiguration") StaticAnalysis.Configuration configuration

    ) throws IOException {
        return StaticAnalysis.start(configuration)
                .parse()
                .callDictionary()
                .store();
    }

}
