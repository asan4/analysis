import {Button, Col, Input, Layout, Row, Spin, Tooltip} from 'antd';
import { Menu } from 'antd';

//
import './App.css'
import './assets/tooltip.css'
import {CallHierarchyTree} from "./CallHierarchyTree";
import {useCallback, useEffect, useMemo, useState} from "react";
import {WorkflowTree} from "./WorkflowTree";
import {PlantUMLViewer} from "./PlantUMLViewer";
import {EntrypointSearch} from "./EntrypointSearch";
import {QueryClient, QueryClientProvider} from "@tanstack/react-query";
import useLoading from "./hooks/useLoading";
import {FileWatcherProvider} from "./context/FileWatcherProvider";
import useArtifacts from "./hooks/useArtifacts";
import dayjs, {unix} from "dayjs";
import relativeTime from 'dayjs/plugin/relativeTime';
dayjs.extend(relativeTime);


const { Header, Sider, Content } = Layout;

const headerStyle: React.CSSProperties = {
  textAlign: 'center',
  height: 'auto',
  padding: '20px 10px',
  backgroundColor: '#ffffff',
  border: '1px solid black'
};

const contentStyle: React.CSSProperties = {
  textAlign: 'center',
  minHeight: 120,
  paddingTop: 10,
  paddingInline: 50,
  backgroundColor: '#ffffff',
  height: '999999px',
  lineHeight: '120px',
  border: '1px solid black'
};

const siderStyle: React.CSSProperties = {
  textAlign: 'center',
  lineHeight: '120px',
  backgroundColor: '#ffffff',
  border: '1px solid black'
};

function App() {

  const renderMap: {[key: string]: JSX.Element} = useMemo(() => {
    return {
      'CallHierarchyTree': <CallHierarchyTree/>,
      'WorkflowTree': <WorkflowTree/>,
      'PlantUMLViewer': <PlantUMLViewer/>,
    }
  }, []);

  const [renderKey, setRenderKey] = useState<string>('CallHierarchyTree');

  const onSelectMenu = useCallback((e: any) => {
    setRenderKey(e.key);
  }, []);

  const queryClient = useMemo(() => new QueryClient(), []);

  const {loading} = useLoading();

  const {fetchedTime} = useArtifacts();

  const [fetchedTimeAgo, setFetchedTimeAgo] = useState<string>('');
  useEffect(() => {
    const fetchInterval = setInterval(() => {
      if (fetchedTime) {
        setFetchedTimeAgo(unix(fetchedTime).fromNow())
      }
    }, 1000 * 45);

    return () => clearInterval(fetchInterval);
  }, [fetchedTime]);

  return (
      <QueryClientProvider client={queryClient}>
        <FileWatcherProvider>
        <Spin spinning={loading} tip={'Reload.....'} size={'large'}>
          <Layout>
            <Sider style={siderStyle}>
              <Menu
                  onSelect={onSelectMenu}
                  items={[
                    {key: 'CallHierarchyTree', label: 'CallHierarchyTree'},
                    {key: 'WorkflowTree', label: 'WorkflowTree'},
                    {key: 'PlantUMLViewer', label: 'PlantUMLViewer'},
                  ]}
              />
            </Sider>
            <Layout>
              <Header style={headerStyle}>
                {
                  fetchedTime &&
                    <Tooltip overlayStyle={{ whiteSpace: 'pre-line' }} title={unix(fetchedTime).format()}>
                      <h2>Updated {unix(fetchedTime || 0).fromNow()}</h2>
                    </Tooltip>
                }
                <EntrypointSearch/>
              </Header>
              <Content style={contentStyle}>
                <h1>{renderKey}</h1>
                {renderMap[renderKey]}
              </Content>
            </Layout>
          </Layout>
        </Spin>
        </FileWatcherProvider>
      </QueryClientProvider>

  )
}

export default App
