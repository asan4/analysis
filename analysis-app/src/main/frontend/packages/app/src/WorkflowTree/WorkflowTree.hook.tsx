import React, {memo, useCallback, useMemo} from 'react';
import {Workflow} from "../types/types";
import {DataNode} from "antd/es/tree";
import {Tag, Tooltip} from "antd";
import useArtifacts from "../hooks/useArtifacts";

const toGroupTag = (workflow: Workflow) => {
    if (workflow.group?.key) {
        return (
          <Tooltip title={workflow.group.description} overlayStyle={{ whiteSpace: 'pre-line' }}>
              <Tag color={'default'}>{workflow.description && '*'}{workflow.group.key}</Tag>
          </Tooltip>
        )
    } else {
        return null;
    }
};

const toTags =(workflow: Workflow) => {
    return workflow.tags.map(tag => <Tag key={tag} color={'lime-inverse'}>{tag}</Tag>);
};

const toRefTag = (workflow: Workflow) => {
    
    return (
      <Tooltip title={workflow.callHierarchyRefKey} overlayStyle={{ whiteSpace: 'pre-line' }}>
          <Tag color={'default'}>ref</Tag>
      </Tooltip>
    );
    
};

const Flow = memo((props: { workflow: Workflow }) => {
    const { workflow } = props;
    return (
      <div>
          {toGroupTag(workflow)}
          <Tooltip title={workflow.description}>
              <span style={{marginRight: '10px'}}>{workflow.description && '*'}{workflow.key}</span>
          </Tooltip>
          {toRefTag(workflow)}
          {toTags(workflow)}
      </div>
    )
})

const ConditionGroup = memo((props: { workflow: Workflow }) => {
    const { workflow } = props;
    return (
      <div>
          {toGroupTag(workflow)}
          <Tooltip title={workflow.description}>
              <span style={{marginRight: '10px', color: 'darkblue'}}>{workflow.key || 'ConditionGroup'}</span>
          </Tooltip>
          {toRefTag(workflow)}
          {toTags(workflow)}
      </div>
    )
})

const Condition = memo((props: { workflow: Workflow }) => {
    const { workflow } = props;
    return (
      <div>
          {toGroupTag(workflow)}
          <Tooltip title={workflow.description}>
              <span style={{marginRight: '10px', color: 'darkblue'}}>{workflow.key || 'Condition'}</span>
          </Tooltip>
          {toRefTag(workflow)}
          {toTags(workflow)}
      </div>
    )
})

const Return = memo((props: { workflow: Workflow }) => {
    const { workflow } = props;
    return (
      <div>
          {toGroupTag(workflow)}
          <Tooltip title={workflow.description}>
              <span style={{marginRight: '10px', color: 'darkgreen'}}>{workflow.key || 'Return'}</span>
          </Tooltip>
          {toRefTag(workflow)}
          {toTags(workflow)}
      </div>
    )
})

const Throw = memo((props: { workflow: Workflow }) => {
    const { workflow } = props;
    return (
      <div>
          {toGroupTag(workflow)}
          <Tooltip title={workflow.description}>
              <span style={{marginRight: '10px', color: 'darkred'}}>{workflow.key || 'Throw'}</span>
          </Tooltip>
          {toRefTag(workflow)}
          {toTags(workflow)}
      </div>
    )
})

const Switch = memo((props: { workflow: Workflow }) => {
    const { workflow } = props;
    return (
      <div>
          {toGroupTag(workflow)}
          <Tooltip title={workflow.description}>
              <span style={{marginRight: '10px'}}>{workflow.description}{workflow.key || 'Switch'}</span>
          </Tooltip>
          {toRefTag(workflow)}
          {toTags(workflow)}
      </div>
    )
})

const Then = memo((props: { workflow: Workflow }) => {
  const { workflow } = props;
  return (
    <div>
      {toGroupTag(workflow)}
      <Tooltip title={workflow.description}>
        <span style={{marginRight: '10px', color: 'orange'}}>{workflow.description || workflow.key || 'Then'}</span>
      </Tooltip>
      {toRefTag(workflow)}
      {toTags(workflow)}
    </div>
  )
})

const Else = memo((props: { workflow: Workflow }) => {
  const { workflow } = props;
  return (
    <div>
      {toGroupTag(workflow)}
      <Tooltip title={workflow.description}>
        <span style={{marginRight: '10px', color: 'orange'}}>{workflow.description || workflow.key || 'Else'}</span>
      </Tooltip>
      {toRefTag(workflow)}
      {toTags(workflow)}
    </div>
  )
})
const toTitle = (workflow: Workflow) => {
    switch (workflow.type) {
        case "Flow":
            return <Flow workflow={workflow}/>
        case "Condition":
          return <Condition workflow={workflow}/>;
        case "Else":
          return <Else workflow={workflow}/>
        case "Then":
          return <Then workflow={workflow}/>
        case "ConditionGroup":
            return <ConditionGroup workflow={workflow}/>
        case "Return":
            return <Return workflow={workflow}/>
        case "Throw":
            return <Throw workflow={workflow}/>
        case "Switch":
            return <Switch workflow={workflow}/>

    }
};

const toDataNode = (workflow: Workflow, location = '0'): DataNode => {
    
    const node: DataNode = {key: workflow.key.concat(location), title: toTitle(workflow)}
    
    if (workflow.children && workflow.children.length) {
        node.children = workflow.children.map((child, index) => toDataNode(child, `${location}-${index}`));
    }
    
    return node;
};

export const useTreeData = () => {
    const {artifacts} = useArtifacts();
    const treedata: DataNode[] = useMemo(() => {
        if (artifacts?.workflow) {
            return [toDataNode(artifacts.workflow)];
        } else {
            return [];
        }
    }, [artifacts]);
    return treedata;
}

