import React, {useCallback, useState} from 'react';
import {Input, Tree} from 'antd';
import {useTreeData} from "./WorkflowTree.hook";
import {DataNode} from "antd/es/tree";

const { Search } = Input;

const WorkflowTree: React.FC = () => {
    const treeData = useTreeData()

    const [selected, setSelected] = useState<DataNode|undefined>(undefined);
    const onSelect = useCallback((selectedKeys: any, info: any) => {
        const { node } = info;
        if (Array.isArray(node.children) && node.children.length) {
            setSelected(node);
        } else {
            setSelected(undefined);
        }
    }, []);

    return (
        <div style={{width: '100%', height: '800px'}}>
            <Tree
                style={{border: '1px solid', minHeight: '500px'}}
                treeData={treeData}
                onSelect={(keys, info) => onSelect(keys, info)}
            />
            {
                selected &&
                <div>
                    <h2>Selected</h2>
                    <Tree
                        style={{border: '1px solid', minHeight: '500px'}}
                        treeData={[selected]}
                    />
                </div>
            }
        </div>
    );
};

export default WorkflowTree