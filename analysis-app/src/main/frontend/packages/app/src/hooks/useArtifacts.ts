import {useAtom, useStore} from 'jotai'
import axios from 'axios';
import {useCallback, useEffect, useLayoutEffect} from "react";
import useLoading from "./useLoading";
import {globalArtifacts, globalArtifactsFetchedTime, globalEntrypoint, useFileWatcher} from "../context/types";
import {Artifacts} from "../types/staticAnalysis/Artifacts";
import {QualifiedMethodSignature} from "../types/callhierarchy/qualification/QualifiedMethodSignature";
import dayjs from "dayjs";

const useArtifacts = (initialEntryPoint?: QualifiedMethodSignature) => {
    const [entryPoint, setEntryPoint] = useAtom(globalEntrypoint);
    const [artifacts, setArtifacts] = useAtom(globalArtifacts);
    const [fetchedTime, setFetchedTime] = useAtom(globalArtifactsFetchedTime);
    const {toggle} = useLoading();
    const {needReload, reloadEnd} = useFileWatcher();

    useEffect(() => {
        if (initialEntryPoint) {
            setEntryPoint(initialEntryPoint);
        }
    }, []);
    

    const fetchArtifacts = useCallback(async (): Promise<Artifacts|undefined> => {
        if (entryPoint) {
            const artifacts = await axios.post(
                '/api/static-analysis/artifacts',
                entryPoint
            );
            return artifacts.data;
        } else {
            return undefined;
        }
    }, [entryPoint]);

    const reload = useCallback(async () => {
        if (entryPoint) {
            reloadEnd();
            toggle();
            const artifacts = await axios.post(
                '/api/static-analysis/reload',
                entryPoint
            );
            setArtifacts(artifacts.data);
            toggle()
        }
    }, [entryPoint]);
    
    useLayoutEffect(() => {
        if (needReload) {
            reload();
        };
    }, [needReload, reload])

    useEffect(() => {
        if (entryPoint) {
            fetchArtifacts().then(_artifacts => setArtifacts(_artifacts));
        }
    }, [entryPoint]);

    useEffect(() => {
        if (artifacts) {
            setFetchedTime(dayjs().unix());
        }
    }, [artifacts])
    return {artifacts, setEntryPoint, reload, fetchedTime};
}

export default useArtifacts;