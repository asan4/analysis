import {useCallback, useEffect, useState} from "react";
import axios from "axios";
import {QualifiedMethodSignature} from "../types/callhierarchy/qualification/QualifiedMethodSignature";

const useEntryPoints = (initialClassKey?: string) => {
    const [classKey, setClassKey] = useState<string>(initialClassKey || 'kr.amc.amis.mc.oo.od.service.CrtfcOrdrBizService');
    const [entryPoints, setEntryPoints] = useState<QualifiedMethodSignature[]>([]);

    const fetchEntryPoints = useCallback(async (): Promise<QualifiedMethodSignature[]> => {
        const res = await axios.get(
            `/api/static-analysis/entrypoints/${classKey}`
        )
        return res.data;
    }, [classKey]);

    useEffect(() => {
        fetchEntryPoints().then(data => setEntryPoints(data));
    }, [classKey, fetchEntryPoints]);

    return {entryPoints, classKey, setClassKey}
}

export default useEntryPoints;