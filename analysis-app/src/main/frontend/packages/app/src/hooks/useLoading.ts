import {useAtom} from 'jotai'
import {useCallback} from "react";
import {globalLoading} from "../context/types";


const useLoading = () => {
    const [loading, setLoading] = useAtom(globalLoading);

    const toggle = useCallback(() => {
        setLoading(prev => !prev);
    }, []);

    return {loading, toggle};
}

export default useLoading;