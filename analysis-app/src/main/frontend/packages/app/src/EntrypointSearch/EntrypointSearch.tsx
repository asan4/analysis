import React, {useCallback, useState} from 'react';
import {Button, Col, Input, Row, Select} from "antd";
import useArtifacts from "../hooks/useArtifacts";
import useEntryPoints from "../hooks/useEntryPoints";
import {QualifiedMethodSignature} from "../types/callhierarchy/qualification/QualifiedMethodSignature";

const EntrypointSearch = () => {
    const {setEntryPoint, reload, fetchedTime} = useArtifacts();
    const {setClassKey, entryPoints, classKey} = useEntryPoints();

    const onSearchClassKey = useCallback((value: string) => {
        setClassKey(value);
    }, [setClassKey]);

    const onClickReload = useCallback(() => {
        reload();
    }, [reload]);

    const handleSelect = useCallback((value: string) => {
        const entryPoint: QualifiedMethodSignature = JSON.parse(value);
        setEntryPoint(entryPoint);
    }, [setEntryPoint]);

    return (
        <Row gutter={10}>
            <Col span={8} style={{padding: '10px', display: 'flex', flexDirection:'column', justifyContent: 'center'}}>
                <Input.Search
                    style={{margin: '5px 0px'}}
                    addonBefore={'CanonicalClassName'}
                    defaultValue={classKey}
                    onSearch={onSearchClassKey}
                />
            </Col>
            <Col span={12} style={{padding: '10px', display: 'flex', flexDirection:'column', justifyContent: 'center'}}>
                {
                    entryPoints.length > 0 &&
                    <Select
                        defaultValue={'Select EntryPoint......'}
                        onChange={handleSelect}
                        options={entryPoints
                                .map(ep => ({
                                    label: ep.valueAsShortString,
                                    value: JSON.stringify(ep)
                                }))
                            || []
                        }
                    />
                }

            </Col>
            <Col span={3} style={{padding: '10px', display: 'flex', flexDirection:'column', justifyContent: 'center'}}>
                <Button type={'default'} onClick={onClickReload}>Reload</Button>
            </Col>
        </Row>
    );
}

export default EntrypointSearch;