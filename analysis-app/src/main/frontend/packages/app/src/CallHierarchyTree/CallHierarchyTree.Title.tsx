import React, {ReactElement, useCallback, useMemo} from 'react';

import {CallHierarchy} from "../types/callhierarchy/CallHierarchy";
import {TaggedInfo} from "../types/callhierarchy/info/TaggedInfo";
import {Tag, Tooltip} from "antd";
import {ASTType} from "../types/callhierarchy/ASTType";
import {MethodHierarchy} from "../types/callhierarchy/MethodHierarchy";
import * as ClassMeta from "../types/callhierarchy/dictionary/ClassMeta";
import * as MethodMeta from "../types/callhierarchy/dictionary/MethodMeta";
import {ConditionBlockHierarchy} from "../types/callhierarchy/ConditionBlockHierarchy";
import {Property} from "csstype";
import {ThrowHierarchy} from "../types/callhierarchy/ThrowHierarchy";
import {ReturnHierarchy} from "../types/callhierarchy/ReturnHierarchy";
import {EntryHierarchy, SwitchBlockHierarchy} from "../types/callhierarchy/SwitchBlockHierarchy";


interface Props {
    callHierarchy: CallHierarchy;
}

const CallHierarchyTreeTitle = (props: Props) => {
    const {callHierarchy} = props;

    const renderClassFeature = useCallback((feature: ClassMeta.Feature): ReactElement[] => {
        return Object.entries(feature).filter(([key, value]) => value)
            .map(([key, value]) => {
                return (
                    <Tag color={'blue'}>#{key}</Tag>
                );
            });
    }, []);

    const renderMethodFeature = useCallback((feature: MethodMeta.Feature): ReactElement[] => {
        return Object.entries(feature).filter(([key, value]) => value)
            .map(([key, value]) => {
                return (
                    <Tag color={'blue'}>#{key}</Tag>
                );
            });
    }, []);

    const renderTaggedInfo = useCallback((taggedInfo: TaggedInfo): ReactElement[] => {
        return Object.entries(taggedInfo).map(([key, value]) => {
            const valueString = typeof value === 'string' ? value : (value as string[]).join('\n');
            return (
                <Tooltip key={key} overlayStyle={{ whiteSpace: 'pre-line' }} title={valueString}>
                    <Tag color={'green-inverse'}>@{key}</Tag>
                </Tooltip>
            );
        });
    }, []);

    const renderMethodTitle = useCallback((method: MethodHierarchy) => {
        return (
            <div>
                {method.classFeature && renderClassFeature(method.classFeature)}
                {renderTaggedInfo(method.classTaggedInfo || {})}
                <Tooltip key={method.qualifiedMethodSignature.valueAsString} overlayStyle={{ whiteSpace: 'pre-line' }} title={method.qualifiedMethodSignature.valueAsString}>
                <span style={{marginRight: '10px'}}>
                    {method.qualifiedMethodSignature.valueAsShortString}
                </span>
                </Tooltip>

                {renderTaggedInfo(method.methodTaggedInfo || {})}
                {method.methodFeature && renderMethodFeature(method.methodFeature)}
                {method.isCircularCall && <Tag color={'red-inverse'}>Circular</Tag>}
                {method.isExternalMethodCall && <Tag color={'geekblue'}>External</Tag>}
            </div>
        );
    }, [renderClassFeature, renderMethodFeature, renderTaggedInfo]);

    const renderConditionBlockTitle = useCallback((conditionBlock: ConditionBlockHierarchy) => {
        return (
            <div>
                <Tag color={'orange-inverse'}>#ConditionBlock</Tag>
                <span style={{marginRight: '10px', color: "darkblue", fontStyle: 'italic'}}>{conditionBlock.conditionExpr}</span>
                {renderTaggedInfo(conditionBlock.taggedInfo || {})}
            </div>
        )
    }, [renderTaggedInfo]);

    const renderSwitchBlockTitle = useCallback((switchBlock: SwitchBlockHierarchy) => {
        return (
            <div>
                <Tag color={'orange-inverse'}>#SwitchBlock</Tag>
                <span style={{marginRight: '10px', color: "darkblue", fontStyle: 'italic'}}>{switchBlock.selectorExpr}</span>
                {renderTaggedInfo(switchBlock.taggedInfo || {})}
            </div>
        )
    }, [renderTaggedInfo]);

    const renderSwitchBlockEntryTitle = useCallback((switchBlockEntry: EntryHierarchy) => {
        return (
            <div>
                <Tag color={'orange'}>#SwitchBlock_Entry</Tag>
                <span style={{marginRight: '10px', color: "darkblue", fontStyle: 'italic'}}>{switchBlockEntry.label}</span>
            </div>
        )
    }, [renderTaggedInfo]);

    const renderReturnTitle = useCallback((_return: ReturnHierarchy) => {
        return (
            <div>
                <Tag color={'orange-inverse'}>#Return</Tag>
                <span style={{marginRight: '10px', color: "darkblue", fontStyle: 'italic'}}>{_return.returnStmt}</span>
            </div>
        )
    }, [renderTaggedInfo]);

    const renderThrowTitle = useCallback((_throw: ThrowHierarchy) => {
        return (
            <div>
                <Tag color={'orange-inverse'}>#Throw</Tag>
                <span style={{marginRight: '10px', color: "darkblue", fontStyle: 'italic'}}>{_throw.throwStmt}</span>
            </div>
        )
    }, [renderTaggedInfo]);

    const renderSimpleTitle = useCallback((type: ASTType, color: Property.Color, taggedInfo?: TaggedInfo) => {
        return (
            <div>
                <Tag color={color}>#{type}</Tag>
                {taggedInfo ? renderTaggedInfo(taggedInfo) : null}
            </div>
        )
    }, [renderTaggedInfo])


    const TitleRender = useMemo<{[key in ASTType]: (ch: CallHierarchy) => ReactElement}>(() => ({
        Method: ch => renderMethodTitle(ch as MethodHierarchy),
        ConditionBlock: ch => renderConditionBlockTitle(ch as ConditionBlockHierarchy),
        ConditionBlock_Condition: ch => renderSimpleTitle(ch.type, 'orange'),
        ConditionBlock_Then: ch => renderSimpleTitle(ch.type, 'orange'),
        ConditionBlock_Else: ch => renderSimpleTitle(ch.type, 'orange'),
        Throw: ch => renderThrowTitle(ch as ThrowHierarchy),
        Return: ch => renderReturnTitle(ch as ReturnHierarchy),
        SwitchBlock: ch => renderSwitchBlockTitle(ch as SwitchBlockHierarchy),
        SwitchBlock_Entry: ch => renderSwitchBlockEntryTitle(ch as EntryHierarchy),
        SwitchBlock_Selector: ch => renderSimpleTitle(ch.type, 'orange'),
    }), [renderMethodTitle, renderConditionBlockTitle, renderSimpleTitle, renderThrowTitle, renderReturnTitle]);


    return (
        <>
            {TitleRender[callHierarchy.type](callHierarchy)}
        </>
    )
}

export default CallHierarchyTreeTitle;