import React, {useMemo} from 'react';
import {DataNode} from "antd/es/tree";
import useArtifacts from "../hooks/useArtifacts";
import {CallHierarchy} from "../types/callhierarchy/CallHierarchy";
import {MethodHierarchy} from "../types/callhierarchy/MethodHierarchy";
import {ASTType} from "../types/callhierarchy/ASTType";
import CallHierarchyTreeTitle from "./CallHierarchyTree.Title";

export const useTreeData = () => {
    const { artifacts } = useArtifacts();
    return useMemo(() => artifacts ? [genDataNode(artifacts.callHierarchy)] : [], [artifacts]);
}

const genDataNode = (callHierarchy: CallHierarchy, location: string = '0'): DataNode => {
    const node: DataNode = {
        key: (callHierarchy.type === ASTType.Method ?
            (callHierarchy as MethodHierarchy).qualifiedMethodSignature.valueAsString
            : callHierarchy.type).concat(location),
        title: <CallHierarchyTreeTitle callHierarchy={callHierarchy}/>
    };

    if (callHierarchy.children && callHierarchy.children.length) {
        node.children = callHierarchy.children.map((child, index) => genDataNode(child, `${location}-${index}`));
    }
    return node;
}
