import React, {useMemo} from 'react';
import plantumlEncoder from 'plantuml-encoder';
import useArtifacts from "../hooks/useArtifacts";


export const usePUML = () => {
    const {artifacts} = useArtifacts();


    const encodedPuml = useMemo(() => {
        if (artifacts?.plantUML) {
            return plantumlEncoder.encode(artifacts.plantUML);
        }
        return 'SyfFKj2rKt3CoKnELR1Io4ZDoSa70000';
    }, [artifacts]);

    return {puml: encodedPuml};
}