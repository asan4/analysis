import React, {useMemo} from 'react';
import {usePUML} from "./WorkflowTree.hook";


const PlantUMLViewer: React.FC = () => {
    const {puml} = usePUML()

    const url = useMemo(() => {
        return `http://www.plantuml.com/plantuml/img/${puml}`;
    }, [puml]);


    return (
        <div style={{width: '100%', height: '800px'}}>
            <img src={url}/>
        </div>
    );
};

export default PlantUMLViewer