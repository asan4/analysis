export interface WorkflowGroup {
    key: string,
    tags: string[],
    description: string,
}

export interface Workflow {
    key: string,
    tags: string[],
    description: string,
    callHierarchyRefKey: string,
    group?: WorkflowGroup,
    children?: Workflow[]
}