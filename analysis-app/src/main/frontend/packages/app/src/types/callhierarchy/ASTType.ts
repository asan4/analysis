export enum ASTType {
    Method = 'Method',
    ConditionBlock = 'ConditionBlock',
    ConditionBlock_Condition = 'ConditionBlock_Condition',
    ConditionBlock_Then = 'ConditionBlock_Then',
    ConditionBlock_Else = 'ConditionBlock_Else',
    SwitchBlock = 'SwitchBlock',
    SwitchBlock_Selector = 'SwitchBlock_Selector',
    SwitchBlock_Entry = 'SwitchBlock_Entry',
    Return = 'Return',
    Throw = 'Throw'
}