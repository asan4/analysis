import {NextRef} from "./NextRef";
import {QualifiedMethodSignature} from "../qualification/QualifiedMethodSignature";

export interface MethodRef extends NextRef {
    qualifiedMethodSignature: QualifiedMethodSignature;
}