import {QualifiedMethodSignature} from "../qualification/QualifiedMethodSignature";
import {MetricInfo} from "../info/MetricInfo";
import {TaggedInfo} from "../info/TaggedInfo";

export interface MethodMeta {
    qualifiedMethodSignature: QualifiedMethodSignature;
    feature: Feature;
    metricInfo: MetricInfo | null;
    taggedInfo: TaggedInfo | null;
}

export interface Feature {
    getter: boolean;
    setter: boolean;
}