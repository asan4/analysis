import {CallHierarchy} from "./CallHierarchy";
import {ASTType} from "./ASTType";

export interface ThrowHierarchy extends CallHierarchy {
    type: ASTType.Throw;
    throwStmt: string;
}