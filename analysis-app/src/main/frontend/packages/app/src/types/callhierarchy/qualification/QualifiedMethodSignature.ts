import {QualifiedClassName} from "./QualifiedClassName";

export interface QualifiedMethodSignature {
    qualifiedClassName: QualifiedClassName;
    methodName: string;
    qualifiedParamNames: QualifiedClassName[];

    valueAsString: string;
    valueAsShortString: string;
}