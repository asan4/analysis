export interface TaggedInfo {
    [key: string]: string | string[]
}