export enum WorkflowType {
    Flow = 'Flow',
    ConditionGroup = 'ConditionGroup',
    Condition = 'Condition',
    Return = 'Return',
    Throw = 'Throw',
    Switch = 'Switch',
    Then = 'Then',
    Else = 'Else'
}