import {CallHierarchy} from "./CallHierarchy";
import {QualifiedMethodSignature} from "./qualification/QualifiedMethodSignature";
import * as ClassMeta from "./dictionary/ClassMeta";
import * as MethodMeta from "./dictionary/MethodMeta";
import {TaggedInfo} from "./info/TaggedInfo";
import {MetricInfo} from "./info/MetricInfo";
import {ASTType} from "./ASTType";

export interface MethodHierarchy extends CallHierarchy {
    type: ASTType.Method;
    qualifiedMethodSignature: QualifiedMethodSignature;
    classFeature: ClassMeta.Feature;
    classTaggedInfo: TaggedInfo | null;
    methodFeature: MethodMeta.Feature;
    methodTaggedInfo: TaggedInfo | null;
    methodMetricInfo: MetricInfo | null;
    isCircularCall: boolean;
    isExternalMethodCall: boolean;
}