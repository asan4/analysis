import {CallHierarchy} from "../callhierarchy/CallHierarchy";
import {Workflow} from "../workflow/Workflow";

export interface Artifacts {
    callHierarchy: CallHierarchy;
    workflow: Workflow;
    plantUML: string;
}