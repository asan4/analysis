export interface WorkflowGroup {
    key: string;
    tags: string[];
    description: string;
}