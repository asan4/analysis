import {ASTType} from "./ASTType";

export interface CallHierarchy {
    type: ASTType;
    children: CallHierarchy[]
}