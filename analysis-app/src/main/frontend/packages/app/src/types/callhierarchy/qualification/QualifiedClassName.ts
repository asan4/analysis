export interface QualifiedClassName {
    packageName: string;
    simpleClassName: string;
    valueAsString: string;
    valueAsShortString: string;
}