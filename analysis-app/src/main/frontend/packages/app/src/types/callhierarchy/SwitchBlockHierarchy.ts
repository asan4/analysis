import {TaggedInfo} from "./info/TaggedInfo";
import {CallHierarchy} from "./CallHierarchy";
import {ASTType} from "./ASTType";

export interface SwitchBlockHierarchy extends CallHierarchy {
    type: ASTType.SwitchBlock;
    selectorExpr: string;
    taggedInfo: TaggedInfo | null;
}

export interface SelectorHierarchy extends CallHierarchy {
    type: ASTType.SwitchBlock_Selector;
}

export interface EntryHierarchy extends CallHierarchy {
    type: ASTType.SwitchBlock_Entry;
    label: string;
}