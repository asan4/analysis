import {CallHierarchy} from "./CallHierarchy";
import {ASTType} from "./ASTType";

export interface ReturnHierarchy extends CallHierarchy {
    type: ASTType.Return;
    returnStmt: string;
}