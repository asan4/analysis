import {ASTType} from "../ASTType";

export interface NextRef {
    type: ASTType;
}