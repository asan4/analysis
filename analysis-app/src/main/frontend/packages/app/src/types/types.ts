export interface Foo {

}
// export interface TaggedInfo {
//     [key: string]: string | string[]
// }
//
// export interface CallMethod {
//     key: string,
//     name: string,
//     feature: {isGetter?: boolean, isSetter?: boolean},
//     paramTypes: string[],
//     returnType: string,
//     taggedInfo?: TaggedInfo
//     nextCallMethodKeys: string[];
//     variableMap?: {[key: string]: string}   // (변수명, 변수타입)
// }
//
// export type CallMethodSignature = Pick<CallMethod,
//     'key' |
//     'name' |
//     'feature' |
//     'paramTypes' |
//     'returnType' |
//     'taggedInfo'
// >;
//
// export interface CallClass {
//     key: string;
//     name: string;
//     feature: {isController?: boolean, isService?: boolean, isExtService?: boolean, isMapper?: boolean},
//     methods: CallMethod[],
//     taggedInfo?: TaggedInfo
//     fieldMap?: {[key: string]: string}   // (필드명, 필드타입)
// }
//
// export type CallClassSignature = Pick<CallClass,
//     'key' |
//     'name' |
//     'feature' |
//     'taggedInfo'
// >;
//
// export interface CallHierarchy {
//     key: string,
//     type: CallHierarchyType,
//     children?: CallHierarchy[]
// }
//
// export interface MethodHierarchy extends CallHierarchy {
//     type: 'Method'
//     classSignature: CallClassSignature,
//     methodSignature: CallMethodSignature,
//     metricInfo: MetricInfo
//     circularCall: boolean;
// }
// export interface ConditionHierarchy extends CallHierarchy {
//     type: 'Condition' | 'ConditionGroup'
//     conditionSignature: ConditionSignature
// }
//
// export interface ConditionSignature {
//     conditionExpr: string | null,
//     taggedInfo: TaggedInfo | null
// }
//
// export interface ReturnHierarchy extends CallHierarchy {
//     type: 'Return'
//     returnSignature: ReturnSignature | null
// }
//
// export interface ReturnSignature{
//     returnStmt: string | null;
// }
//
// export interface ThrowHierarchy extends CallHierarchy {
//     type: 'Throw'
//     throwStmt: string
//     throwSignature: ThrowSignature
// }
//
// export interface ThrowSignature {
//     throwStmt: string;
// }
//
//
// export interface MetricInfo {
//     cognitiveComplexity: number
// }
//
// export interface WorkflowGroup {
//     key: string,
//     tags: string[],
//     description: string,
// }
//
// export interface Workflow {
//     key: string,
//     type: WorkflowType;
//     tags: string[],
//     description: string,
//     callHierarchyRefKey: string,
//     group?: WorkflowGroup,
//     children?: Workflow[]
// }
//
// export type WorkflowType =
//   'Flow' |
//   'ConditionGroup' |
//   'Condition' |
//   'Return' |
//   'Throw' |
//   'Switch' |
//   'Then' |
//   'Else'
//
// export interface CallMethodKey {
//     className: string,
//     methodName: string,
//     paramTypes: string[]
// }
//
// export interface Artifacts {
//     callHierarchy: CallHierarchy,
//     workflow: Workflow,
//     plantUML: string,
// }
//
// export type CallHierarchyType =
//     'Method' |
//     'Condition' |
//     'ConditionGroup' |
//     'Switch' |
//     'SwitchSelector' |
//     'SwitchEntry' |
//     'Return' |
//     'Throw';