import {NextRef} from "./NextRef";
import {TaggedInfo} from "../info/TaggedInfo";

export interface ConditionBlockRef extends NextRef {
    conditionRefQueue: NextRef[];
    thenRefQueue: NextRef[];
    elseRefQueue: NextRef[];
    conditionExpr: string;
    taggedInfo: TaggedInfo;
}