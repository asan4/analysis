import {QualifiedClassName} from "../qualification/QualifiedClassName";
import {MethodMeta} from "./MethodMeta";
import {TaggedInfo} from "../info/TaggedInfo";

export interface ClassMeta {
    qualifiedClassName: QualifiedClassName;
    feature: Feature;
    method: MethodMeta[];
    taggedInfo: TaggedInfo | null;
}

export interface Feature {
    controller: boolean;
    service: boolean;
    extService: boolean;
    mapper: boolean;
}
