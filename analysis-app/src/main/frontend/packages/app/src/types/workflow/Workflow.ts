import {WorkflowType} from "./WorkflowType";
import {WorkflowGroup} from "./WorkflowGroup";

export interface Workflow {
    type: WorkflowType;
    key: string;
    tags: string[];
    description: string;
    sourceRef: string;
    group: WorkflowGroup;
    children: Workflow[];
}