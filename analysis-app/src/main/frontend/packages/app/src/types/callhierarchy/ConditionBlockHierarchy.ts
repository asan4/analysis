import {TaggedInfo} from "./info/TaggedInfo";
import {CallHierarchy} from "./CallHierarchy";
import {ASTType} from "./ASTType";

export interface ConditionBlockHierarchy extends CallHierarchy {
    type: ASTType.ConditionBlock;
    conditionExpr: string;
    taggedInfo: TaggedInfo | null;
}

export interface ConditionHierarchy extends CallHierarchy {
    type: ASTType.ConditionBlock_Condition;
}

export interface ThenHierarchy extends CallHierarchy {
    type: ASTType.ConditionBlock_Then;
}
export interface ElseHierarchy extends CallHierarchy {
    type: ASTType.ConditionBlock_Else;
    taggedInfo: TaggedInfo | null;
}