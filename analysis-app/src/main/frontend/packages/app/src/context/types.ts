import {atom} from "jotai/index";
import {createContext, Dispatch, SetStateAction, useContext} from "react";
import {Artifacts} from "../types/staticAnalysis/Artifacts";
import {QualifiedMethodSignature} from "../types/callhierarchy/qualification/QualifiedMethodSignature";

export const globalArtifacts = atom<Artifacts|undefined>(undefined);
export const globalArtifactsFetchedTime = atom<number|undefined>(undefined);
export const globalEntrypoint = atom<QualifiedMethodSignature|undefined>(undefined);
export const globalLoading = atom<boolean>(false);
export const FileWatcherContext = createContext<FileWatcherTrigger>({needReload: false, reloadEnd: () => {}});
type FileWatcherTrigger = {
	reloadEnd: () => void;
	needReload: boolean;
}
export const useFileWatcher = () => useContext(FileWatcherContext);

export type FileChangeMessage = {
	title: string
	time: number
	type: 'JAVA_FILE' | 'XML_FILE' | 'YAML_FILE'
	changedFiles: string[]
}