import { IFrame, Client } from '@stomp/stompjs';
import {PropsWithChildren, ReactElement, useLayoutEffect, useMemo, useState} from "react";
import {FileWatcherContext} from "./types";
import useLoading from "../hooks/useLoading";

export const FileWatcherProvider = (props: PropsWithChildren): ReactElement<PropsWithChildren> => {
	
	const [needReload, setNeedReload] = useState(false);
	const {toggle} = useLoading();
	
	useLayoutEffect(() => {
		const client = new Client({
			brokerURL: `ws://${window.location.host}/ws`,
			debug: (str) => {
				console.log("debug:", str);
			},
			onConnect: (frame: IFrame) => {
				console.log('connected WebSocket');
				client.subscribe('/topic/file', (message) => {
					toggle();
					console.log('message delivered: ', message.body);
					setNeedReload(true);
				});
			}
		});
		client.activate();
	}, [toggle]);
	
	const ctxValue = useMemo(() => ({needReload, reloadEnd: () => {
		setNeedReload(false);
		toggle();
		}}), [needReload, toggle])
	
	return (
		<FileWatcherContext.Provider value={ctxValue}>
			{props.children}
		</FileWatcherContext.Provider>
	)
}