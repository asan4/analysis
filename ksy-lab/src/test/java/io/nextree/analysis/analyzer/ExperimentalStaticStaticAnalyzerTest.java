package io.nextree.analysis.analyzer;


import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import io.nextree.analysis.TestContext;
import io.nextree.analysis.callhierarchy.info.TaggedInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.List;

@Slf4j
class ExperimentalStaticStaticAnalyzerTest implements TestContext {

    @Test
    void ifStmtTest() {
        final String fileName = "IfSample.java";
        CompilationUnit compilationUnit = parse(fileName);
        log.debug("compilationUnit : {}", compilationUnit);
    }

    @Test
    void commentTest() {
        final String fileName = "CommentedSimpleService.java";
        CompilationUnit cu = parse(fileName);
        ClassOrInterfaceDeclaration clazz = analyzer.type(cu);
        List<MethodDeclaration> methods = analyzer.methods(clazz);
        methods.forEach(method -> {
            method.getJavadocComment().ifPresentOrElse(
                    comment -> {
                        log.debug("+++++++++++++++ Method[{}] Comment Info%n", method.getName());
                        TaggedInfo taggedInfo = analyzer.comment(comment);
                        taggedInfo.entrySet().stream().forEach(entry -> {
                            log.debug("\t{} : {}", entry.getKey(), entry.getValue());
                        });

                    },
                    () -> {}
            );
        });
    }
}
