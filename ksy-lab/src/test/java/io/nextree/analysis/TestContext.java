package io.nextree.analysis;

import com.github.javaparser.ast.CompilationUnit;
import io.nextree.analysis.analyzer.ExperimentalStaticAnalyzer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public interface TestContext {
    String EXPERIMENT_PATH = String.join(File.separator, "src", "test", "resources", "experiment");
    ExperimentalStaticAnalyzer analyzer = ExperimentalStaticAnalyzer.analyze();
    default File file(String filename) {
        return new File(String.join(File.separator, EXPERIMENT_PATH, filename));
    }

    default CompilationUnit parse(String filename) {
        try(FileInputStream fis = new FileInputStream(file(filename))) {
            return analyzer.parse(fis);
        }catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
