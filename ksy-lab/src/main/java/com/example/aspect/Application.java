package com.example.aspect;

import com.example.aspect.callee.Bar;
import com.example.aspect.callee.FooDTO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Application {
    public static void main(String[] args) {
        log.info("Start.....");
        FooDTO fooDTO = new FooDTO("알파", "브라보", "찰리");
        Bar bar = new Bar("A", "B", "C");
        log.info("invoke get.....");
        String fooA = fooDTO.getAlpha();
        String barB = bar.getBravo();


        log.info("invoke set.....");
        fooDTO.setAlpha(fooA.concat("_MODIFIED"));
        bar.setBravo(barB.concat("_MODIFIED"));

        log.info("DONE.....");
    }
}
