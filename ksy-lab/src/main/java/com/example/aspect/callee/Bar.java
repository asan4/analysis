package com.example.aspect.callee;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Bar {
    private String alpha;
    private String bravo;
    private String charlie;
}
