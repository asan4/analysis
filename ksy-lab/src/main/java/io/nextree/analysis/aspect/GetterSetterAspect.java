package io.nextree.analysis.aspect;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import java.util.logging.Logger;


@Slf4j
//@Aspect
public class GetterSetterAspect {

//    @Pointcut("call(* *..*TO.get*()) || call(* *..*TO.set*(..))")
    public void getterSetter() {}

//    @Around("getterSetter()")
    public Object makeInfo(ProceedingJoinPoint point) throws Throwable {
        log.debug("===========Aspect is runnning...");
        ObjectMapper objectMapper = new ObjectMapper();
        Object dto = point.getTarget();
        log.debug(dto.toString());
        String beforeJson = objectMapper.writeValueAsString(dto);

        Object result = point.proceed();

        String afterJson = objectMapper.writeValueAsString(dto);
        Signature callee = point.getSignature();
        Class caller = point.getSourceLocation().getWithinType();
        String calledLocation = point.getSourceLocation().toString();

        Info info = new Info(
                dto.getClass(),
                beforeJson, afterJson,
                callee.toLongString(), caller, calledLocation
        );
        log.debug(String.format("Info : %s%s", System.lineSeparator(), info.beforeAndAfterToPrettyJson().toPrettyJson()));
        log.debug("===========Aspect is done...");
        return result;
    }


    @Data
    public static class Info {
        private final Class dtoClass;
        private final String beforeJson;
        private final String afterJson;

        private final String callee;
        private final Class caller;
        private final String calledLocation;

        public Info beforeAndAfterToPrettyJson() {
            try {
                ObjectMapper objectMapper = new ObjectMapper().deactivateDefaultTyping();
                String prettyBeforeJson = objectMapper.writeValueAsString(objectMapper.readValue(beforeJson, Object.class));
                String prettyAfterJson = objectMapper.writeValueAsString(objectMapper.readValue(afterJson, Object.class));
                return new Info(
                        dtoClass,
                        prettyBeforeJson, prettyAfterJson,
                        callee, caller, calledLocation
                );
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                return this;
            }

        }

        public String toPrettyJson() {
            try {
                return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                return "";
            }

        }
    }
}
