package io.nextree.analysis.analyzer;


import io.nextree.analysis.staticanalysis.analyzer.StaticAnalyzer;

public class ExperimentalStaticAnalyzer extends StaticAnalyzer
{
    public static ExperimentalStaticAnalyzer analyze() {
        return new ExperimentalStaticAnalyzer();
    }
}
