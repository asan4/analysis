# Analysis API (🚧)    

## 종속성 추가   

- maven   
```xml
<dependency>
    <groupId>io.nextree</groupId>
    <artifactId>analysis-api</artifactId>
    <version>0.1.0-jdk11-SNAPSHOT</version>
</dependency>
```

- gradle
```kotlin
implementation("io.nextree:analysis-api:0.1.0-jdk11-SNAPSHOT");
```


