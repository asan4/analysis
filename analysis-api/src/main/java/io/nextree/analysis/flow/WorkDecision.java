package io.nextree.analysis.flow;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor
public class WorkDecision extends Workflow {
    private String decision;
    private List<Workflow> childrenOnMatched;
    private List<Workflow> childrenOnNonMatched;

    public WorkDecision(WorkflowSignature signature) {
        super(WorkflowType.Decision, signature);
    }
}
