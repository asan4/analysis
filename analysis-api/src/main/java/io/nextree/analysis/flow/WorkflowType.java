package io.nextree.analysis.flow;

public enum WorkflowType {
    Process,
    Decision,
}
