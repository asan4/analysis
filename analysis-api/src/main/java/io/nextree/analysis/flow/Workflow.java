package io.nextree.analysis.flow;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.annotation.Nullable;

@Getter
@NoArgsConstructor
public abstract class Workflow {
    protected WorkflowType type;
    protected WorkflowSignature signature;
    @Nullable
    protected WorkflowSignature groupSignature;

    protected Workflow(WorkflowType type, WorkflowSignature signature) {
        this.type = type;
        this.signature = signature;
    }

    protected Workflow(WorkflowType type, WorkflowSignature signature, @Nullable WorkflowSignature groupSignature) {
        this.type = type;
        this.signature = signature;
        this.groupSignature = groupSignature;
    }
}
