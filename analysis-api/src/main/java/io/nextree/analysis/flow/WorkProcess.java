package io.nextree.analysis.flow;

import lombok.Getter;

import javax.annotation.Nullable;
import java.util.List;

@Getter
public class WorkProcess extends Workflow {
    private String sourceRef;
    private List<Workflow> children;

    public WorkProcess(WorkflowSignature signature) {
        super(WorkflowType.Process, signature);
    }

    public WorkProcess(WorkflowSignature signature, @Nullable WorkflowSignature groupSignature) {
        super(WorkflowType.Process, signature, groupSignature);
    }
}
