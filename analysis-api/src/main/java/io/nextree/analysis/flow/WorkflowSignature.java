package io.nextree.analysis.flow;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class WorkflowSignature {
    private final String key;
    private List<String> tags;
    private String description;
}
