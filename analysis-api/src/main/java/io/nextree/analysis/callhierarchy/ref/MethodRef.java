package io.nextree.analysis.callhierarchy.ref;

import com.github.javaparser.ast.expr.*;
import io.nextree.analysis.callhierarchy.ASTType;
import io.nextree.analysis.callhierarchy.qualification.QualifiedMethodSignature;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public class MethodRef extends NextRef {
    private QualifiedMethodSignature qualifiedMethodSignature;

    public MethodRef(MethodCallExpr expr) {
        super(ASTType.Method);
        try{
            this.qualifiedMethodSignature = QualifiedMethodSignature.fromKey(expr.resolve().getQualifiedSignature());
        }catch (Exception e) {
            log.warn(expr.asMethodCallExpr().getNameAsString().concat(": method signature is not solved"));
            log.trace("unresolved... ", e);
        }
    }

    public MethodRef(MethodReferenceExpr expr) {
        super(ASTType.Method);
        try{
            this.qualifiedMethodSignature = QualifiedMethodSignature.fromKey(expr.resolve().getQualifiedSignature());
        }catch (Exception e) {
            log.warn(expr.asMethodReferenceExpr().getIdentifier().concat(": method signature is not solved"));
            log.trace("unresolved... ", e);
        }
    }


}
