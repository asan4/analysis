package io.nextree.analysis.callhierarchy.dictionary;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.comments.JavadocComment;
import io.nextree.analysis.callhierarchy.info.TaggedInfo;
import io.nextree.analysis.callhierarchy.qualification.QualifiedClassName;
import io.nextree.analysis.staticanalysis.analyzer.StaticAnalyzer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public class ClassMeta {
    private QualifiedClassName qualifiedClassName;
    private Feature feature;
    private List<MethodMeta> methods;
    @Nullable
    private TaggedInfo taggedInfo;

    public ClassMeta(CompilationUnit compilationUnit) {
        this.build(compilationUnit);
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Feature {
        private boolean controller;
        private boolean service;
        private boolean extService;
        private boolean mapper;
    }

    private void build(CompilationUnit compilationUnit) {
        buildQualifiedClassName(compilationUnit)
                .buildFeature(compilationUnit)
                .buildMethods(compilationUnit)
                .buildTaggedInfo(compilationUnit);
    }

    private ClassMeta buildQualifiedClassName(CompilationUnit compilationUnit) {
        this.qualifiedClassName = QualifiedClassName.fromCanonicalClassName(StaticAnalyzer.analyze().canonicalName(compilationUnit));
        return this;
    }

    private ClassMeta buildFeature(CompilationUnit compilationUnit) {
        List<String> annotations = StaticAnalyzer.analyze().classAnnotations(compilationUnit);
        this.feature = new Feature(
                annotations.contains("Controller"),
                annotations.contains("Service") && !this.qualifiedClassName.getSimpleClassName().contains("ExtService"),
                annotations.contains("Service") && this.qualifiedClassName.getSimpleClassName().contains("ExtService"),
                annotations.contains("Mapper")
        );
        return this;
    }

    private ClassMeta buildMethods(CompilationUnit compilationUnit) {
        StaticAnalyzer staticAnalyzer = StaticAnalyzer.analyze();
        ClassOrInterfaceDeclaration clazz = staticAnalyzer.type(compilationUnit);

        List<MethodMeta> methodMetas = new ArrayList<>();

        // Getter, Setter
        Map<String, String> fieldMap = genFieldMap(compilationUnit);
        String canonicalClassName = qualifiedClassName.getValueAsString();
        clazz.getAnnotations().forEach(annotation -> {
            if(annotation.getNameAsString().equals("Getter"))
                fieldMap.keySet().forEach(fieldName -> methodMetas.add(MethodMeta.genGetterMethodMeta(canonicalClassName, fieldName)));
            if(annotation.getNameAsString().equals("Setter"))
                fieldMap.keySet().forEach(fieldName -> methodMetas.add(MethodMeta.genSetterMethodMeta(canonicalClassName, fieldName, fieldMap.get(fieldName))));
        });

        List<MethodDeclaration> methods = staticAnalyzer.methods(clazz);
        methods.forEach(method -> methodMetas.add(new MethodMeta(method)));

        this.methods = methodMetas;
        return this;
    }

    private Map<String, String> genFieldMap(CompilationUnit compilationUnit) {
        StaticAnalyzer staticAnalyzer = StaticAnalyzer.analyze();
        ClassOrInterfaceDeclaration clazz = staticAnalyzer.type(compilationUnit);
        List<VariableDeclarator> variables = staticAnalyzer.variables(clazz);

        return variables.stream().collect(Collectors.toMap(VariableDeclarator::getNameAsString, (variable) -> {
            if(variable.getType().resolve().isReferenceType())
                return variable.getType().resolve().asReferenceType().getQualifiedName();
            else
                return variable.getTypeAsString();
        }));
    }

    private ClassMeta buildTaggedInfo(CompilationUnit compilationUnit) {
        StaticAnalyzer staticAnalyzer = StaticAnalyzer.analyze();

        Optional<Comment> compilationUnitComment = compilationUnit.getComment();
        if(compilationUnitComment.isPresent() && compilationUnitComment.get().isJavadocComment()) {
            this.taggedInfo = staticAnalyzer.comment(compilationUnitComment.get().asJavadocComment());
        } else {
            ClassOrInterfaceDeclaration clazz = staticAnalyzer.type(compilationUnit);
            Optional<JavadocComment> classComment = clazz.getJavadocComment();
            this.taggedInfo = classComment.map(staticAnalyzer::comment).orElse(null);
        }
        return this;
    }
}
