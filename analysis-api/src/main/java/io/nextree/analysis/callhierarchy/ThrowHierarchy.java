package io.nextree.analysis.callhierarchy;

import io.nextree.analysis.callhierarchy.ref.ThrowRef;
import lombok.Getter;

@Getter
public class ThrowHierarchy extends CallHierarchy{
    private String throwStmt;

    public ThrowHierarchy(ThrowRef throwRef) {
        this.type = ASTType.Throw;
        this.children = null;
        this.throwStmt = throwRef.getThrowStmt();
    }
}
