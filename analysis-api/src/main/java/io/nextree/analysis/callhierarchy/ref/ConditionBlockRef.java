package io.nextree.analysis.callhierarchy.ref;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.comments.JavadocComment;
import com.github.javaparser.ast.expr.ConditionalExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.stmt.DoStmt;
import com.github.javaparser.ast.stmt.IfStmt;
import com.github.javaparser.ast.stmt.WhileStmt;
import io.nextree.analysis.callhierarchy.ASTType;
import io.nextree.analysis.callhierarchy.info.TaggedInfo;
import io.nextree.analysis.staticanalysis.analyzer.StaticAnalyzer;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@Getter
public class ConditionBlockRef extends NextRef {
    private List<NextRef> conditionRefQueue;
    private List<NextRef> thenRefQueue;
    private List<NextRef> elseRefQueue;
    private String conditionExpr;
    private Map<ASTType, TaggedInfo> taggedInfo;

    public ConditionBlockRef(Node node, MethodDeclaration methodDeclaration) {
        super(ASTType.ConditionBlock);
        genRef(node, methodDeclaration);
    }

    private void genRef(Node node, MethodDeclaration methodDeclaration) {
        List<NextRef> conditionRefQueue = null;
        List<NextRef> thenRefQueue = null;
        List<NextRef> elseRefQueue = null;
        String conditionExpr = "";

        if(node instanceof IfStmt){
            conditionRefQueue = genNextRefs(((IfStmt) node).getCondition(), methodDeclaration, new ArrayList<>());
            thenRefQueue = genNextRefs(((IfStmt) node).getThenStmt(), methodDeclaration, new ArrayList<>());
            if(((IfStmt) node).getElseStmt().isPresent())
                elseRefQueue = genNextRefs(((IfStmt) node).getElseStmt().get(), methodDeclaration, new ArrayList<>());
            conditionExpr = ((IfStmt) node).getCondition().toString();
        }else if(node instanceof ConditionalExpr){
            conditionRefQueue = genNextRefs(((ConditionalExpr) node).getCondition(), methodDeclaration, new ArrayList<>());
            thenRefQueue = genNextRefs(((ConditionalExpr) node).getThenExpr(), methodDeclaration, new ArrayList<>());
            elseRefQueue = genNextRefs(((ConditionalExpr) node).getElseExpr(), methodDeclaration, new ArrayList<>());
            conditionExpr = ((ConditionalExpr) node).getCondition().toString();
        }else if (node instanceof DoStmt){
            conditionRefQueue = genNextRefs(((DoStmt) node).getCondition(), methodDeclaration, new ArrayList<>());
            thenRefQueue = genNextRefs(((DoStmt) node).getBody(), methodDeclaration, new ArrayList<>());
            conditionExpr = ((DoStmt) node).getCondition().toString();
        }else if (node instanceof WhileStmt){
            conditionRefQueue = genNextRefs(((WhileStmt) node).getCondition(), methodDeclaration, new ArrayList<>());
            thenRefQueue = genNextRefs(((WhileStmt) node).getBody(), methodDeclaration, new ArrayList<>());
            conditionExpr = ((WhileStmt) node).getCondition().toString();
        }

        this.conditionRefQueue = conditionRefQueue;
        this.thenRefQueue = thenRefQueue;
        this.elseRefQueue = elseRefQueue;
        this.conditionExpr = conditionExpr;
        genTaggedInfo(node);
    }

    private void genTaggedInfo(Node node) {
        AtomicReference<JavadocComment> conditionGroupComment = new AtomicReference<>(null);
        AtomicReference<JavadocComment> elseComment = new AtomicReference<>(null);

        Map<ASTType, TaggedInfo> taggedInfo = new HashMap<>();

        node.getComment().ifPresent(comment -> {
            if(comment.isJavadocComment())
                conditionGroupComment.set(comment.asJavadocComment());
        });

        if(node instanceof IfStmt) {
            ((IfStmt) node).getElseStmt().ifPresent(elseStmt -> {
                if(!(elseStmt instanceof IfStmt))
                    elseStmt.getComment().ifPresent(comment -> {
                        if(comment.isJavadocComment())
                            elseComment.set(comment.asJavadocComment());
                    });
            });
        }
        if(node instanceof ConditionalExpr) {
            Expression elseExpr = ((ConditionalExpr) node).getElseExpr();
            if(!(elseExpr instanceof ConditionalExpr)) {
                elseExpr.getComment().ifPresent(comment -> {
                    if (comment.isJavadocComment())
                        elseComment.set(comment.asJavadocComment());
                });
            }
        }

        if(conditionGroupComment.get() != null)
            taggedInfo.put(ASTType.ConditionBlock, StaticAnalyzer.analyze().comment(conditionGroupComment.get()));
        if(elseComment.get() != null)
            taggedInfo.put(ASTType.ConditionBlock_Else, StaticAnalyzer.analyze().comment(elseComment.get()));

        this.taggedInfo = taggedInfo;
    }
}
