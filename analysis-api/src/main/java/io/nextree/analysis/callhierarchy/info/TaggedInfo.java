package io.nextree.analysis.callhierarchy.info;

import java.util.HashMap;
import java.util.Map;

public class TaggedInfo extends HashMap<String, Object> {

    public TaggedInfo() {
    }

    public TaggedInfo(Map<? extends String, ?> m) {
        super(m);
    }
}