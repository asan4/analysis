package io.nextree.analysis.callhierarchy.ref;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Deprecated
public class CallMethodKey {
    private String className;
    private String methodName;
    private List<String> paramTypes;

    @Override
    public String toString() {
        return toKey();
    }


    public String toKey() {
//        return String.join(":", className, methodName, paramTypes.stream().reduce((a,b) -> String.join(",", a,b)).orElse(""));
        return className
                .concat(".")
                .concat(methodName)
                .concat("(")
                .concat(paramTypes.stream().reduce((a,b) -> String.join(", ", a,b)).orElse(""))
                .concat(")");
    }

    public static CallMethodKey fromKey(String methodKeyString) {

//        String[] tokens = methodKeyString.split(":");
//        if (tokens.length < 2) {
//            throw new IllegalArgumentException("Key is not valid : " + methodKeyString);
//        }
//        String className = tokens[0];
//        String methodName = tokens[1];
//        List<String> paramTypes = tokens.length == 3 ? tokens[2].equals("") ? new ArrayList<>() : Arrays.stream(tokens[2].split(",")).collect(Collectors.toList()) : new ArrayList<>();
        String canonicalMethod = methodKeyString.substring(0, methodKeyString.indexOf('('));
        String paramStrings = methodKeyString.substring(methodKeyString.indexOf('('));

        String className = canonicalMethod.substring(0, canonicalMethod.lastIndexOf('.'));
        String methodName = canonicalMethod.substring(canonicalMethod.lastIndexOf('.')+1);
        List<String> paramTypes = paramStrings.equals("()") ? new ArrayList<>() :
                Arrays.stream(paramStrings.substring(1, paramStrings.length()-1).split(","))
                        .map(String::trim)
                        .collect(Collectors.toList());
        return new CallMethodKey(className, methodName, paramTypes);
    }

    public static void main(String[] args) {
        String methodKeyString = "a.b.c.method(a.b.c, d.e.f)";
        String paramStrings = methodKeyString.substring(methodKeyString.indexOf('('));
        List<String> paramTypes = paramStrings.equals("()") ? new ArrayList<>() :
                Arrays.stream(paramStrings.substring(1, paramStrings.length()-1).split(","))
                                .map(String::trim)
                                        .collect(Collectors.toList());
        System.out.println(paramTypes);
    }
}