package io.nextree.analysis.callhierarchy.qualification;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class QualifiedClassName {
    private String packageName;
    private String simpleClassName;

    public String getValueAsString() {
        return packageName.isBlank() ? simpleClassName : String.join(".", packageName, simpleClassName);
    }

    public String getValueAsShortString() {
        return simpleClassName;
    }

    public static QualifiedClassName fromCanonicalClassName(String key) {
        String packageName = "";
        String simpleClassName = "";
        if (isPrimitive(key)) {
            simpleClassName = key;
        } else {
            packageName = key.substring(0, key.lastIndexOf("."));
            simpleClassName = key.substring(key.lastIndexOf(".") + 1);
        }
        return new QualifiedClassName(packageName, simpleClassName);
    }

    private static boolean isPrimitive (String key) {
        return !key.contains(".");
    }
}
