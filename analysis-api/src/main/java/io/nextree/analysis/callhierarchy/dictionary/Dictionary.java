package io.nextree.analysis.callhierarchy.dictionary;

import java.util.*;

public class Dictionary {
    private Map<String, ClassMeta> classMetaMap;
    private Map<String, MethodMeta> methodMetaMap;

    private Dictionary() {
        this.classMetaMap = new HashMap<>();
        this.methodMetaMap = new HashMap<>();
    }

    public static Dictionary init() {
        return new Dictionary();
    }

    public Optional<ClassMeta> findClassMeta(String qualifiedClassName) {
        return Optional.ofNullable(classMetaMap.get(qualifiedClassName));
    }
    public Optional<MethodMeta> findMethodMeta(String qualifiedMethodSignature) {
        return Optional.ofNullable(methodMetaMap.get(qualifiedMethodSignature));
    }

    public Dictionary addClassMeta(ClassMeta classMeta) {
        if (findClassMeta(classMeta.getQualifiedClassName().getValueAsString()).isPresent()) {
            throw new IllegalArgumentException("ClassMeta already exists with key :" + classMeta.getQualifiedClassName().getValueAsString());
        }
        this.classMetaMap.put(classMeta.getQualifiedClassName().getValueAsString(), classMeta);
        return this;
    }

    public Dictionary addMethodMeta(MethodMeta methodMeta) {
        if (findClassMeta(methodMeta.getQualifiedMethodSignature().getValueAsString()).isPresent()) {
            throw new IllegalArgumentException("MethodMeta already exists with key :" + methodMeta.getQualifiedMethodSignature().getMethodName());
        }
        this.methodMetaMap.put(methodMeta.getQualifiedMethodSignature().getValueAsString(), methodMeta);
        return this;
    }

    public long sizeOfClassMetaMap() {
        return classMetaMap.size();
    }

    public long sizeOfMethodMetaMap() {
        return methodMetaMap.size();
    }
}
