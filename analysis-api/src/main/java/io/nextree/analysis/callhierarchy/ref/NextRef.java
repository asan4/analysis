package io.nextree.analysis.callhierarchy.ref;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.MethodReferenceExpr;
import com.github.javaparser.ast.nodeTypes.NodeWithCondition;
import com.github.javaparser.ast.stmt.*;
import io.nextree.analysis.callhierarchy.ASTType;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
public abstract class NextRef {

    protected ASTType type;
    public NextRef(ASTType type) {
        this.type = type;
    }

    public static List<NextRef> genNextRefs(MethodDeclaration methodDeclaration) {
        if(methodDeclaration == null) return  null;

        Optional<BlockStmt> body = methodDeclaration.getBody();
        if(body.isEmpty()) return null;

        List<Node> nodes = body.get().getChildNodes();
        List<NextRef> nextRefs = new ArrayList<>();
        nodes.forEach(node -> {
            nextRefs.addAll(genNextRefs(node, methodDeclaration, new ArrayList<>()));
        });

        return nextRefs;
    }

    public static List<NextRef> genNextRefs(Node node, MethodDeclaration methodDeclaration, List<NextRef> nextRefs) {
        List<NextRef> addNextRefs = new ArrayList<>(nextRefs);

        // 매개변수로 받은 노드의 타입에 따라 NextRef 타입을 정해 생성
        if(node instanceof ReturnStmt){
            // ReturnStmt 내 탐색은 ReturnRef 에서 하도록
            addNextRefs.add(new ReturnRef((ReturnStmt)node, methodDeclaration));
        } else if(node instanceof ThrowStmt) {
            // 내부 탐색 필요 없음
            addNextRefs.add(new ThrowRef((ThrowStmt)node));
        }else if (node instanceof NodeWithCondition){
            // NodeWithCondition 내 탐색은 ConditionBlockRef 에서 하도록
            if(node instanceof DoStmt){
                // DoStmt 내부에 있는 노드 NextRef 생성
                addNextRefs = genNextRefs(((DoStmt) node).getBody(), methodDeclaration, addNextRefs);
            }
            addNextRefs.add(new ConditionBlockRef(node, methodDeclaration));
        } else if (node instanceof SwitchStmt){
            addNextRefs.add(new SwitchBlockRef((SwitchStmt)node, methodDeclaration));
        } else {
            if(node instanceof MethodCallExpr){
                // node 가 MethodCallExpr 인 경우에는 MethodRef 생성 후, 내부 탐색
                MethodRef methodRef = new MethodRef(((MethodCallExpr) node).asMethodCallExpr());
                addNextRefs.add(methodRef);
            }
            if(node instanceof MethodReferenceExpr){
                MethodRef methodRef = new MethodRef(((MethodReferenceExpr) node).asMethodReferenceExpr());
                addNextRefs.add(methodRef);
            }
            //
            if(node.getChildNodes().size() != 0) {
                for (Node childNode : node.getChildNodes()) {
                    addNextRefs = genNextRefs(childNode, methodDeclaration, addNextRefs);
                }
            }
        }

        return addNextRefs;
    }

}
