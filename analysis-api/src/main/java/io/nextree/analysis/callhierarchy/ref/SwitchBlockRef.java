package io.nextree.analysis.callhierarchy.ref;

import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.stmt.BreakStmt;
import com.github.javaparser.ast.stmt.Statement;
import com.github.javaparser.ast.stmt.SwitchEntry;
import com.github.javaparser.ast.stmt.SwitchStmt;
import com.github.javaparser.printer.configuration.PrettyPrinterConfiguration;
import io.nextree.analysis.callhierarchy.ASTType;
import io.nextree.analysis.callhierarchy.info.TaggedInfo;
import io.nextree.analysis.staticanalysis.analyzer.StaticAnalyzer;
import lombok.Getter;

import java.util.*;

@Getter
public class SwitchBlockRef extends NextRef {
    private String selectorExpr;
    private TaggedInfo taggedInfo;
    private List<NextRef> selectorRefQueue;
    private Map<String, List<NextRef>> entryRefQueue;

    public SwitchBlockRef(SwitchStmt switchStmt, MethodDeclaration methodDeclaration) {
        super(ASTType.SwitchBlock);
        genRef(switchStmt, methodDeclaration);
    }

    private void genRef(SwitchStmt switchStmt, MethodDeclaration methodDeclaration) {
        // selector
        this.selectorExpr = switchStmt.getSelector().toString();
        this.selectorRefQueue = genNextRefs(switchStmt.getSelector(), methodDeclaration, new ArrayList<>());

        Map<String, List<NextRef>> entryRefQueue = new LinkedHashMap<>();

        NodeList<SwitchEntry> entries = switchStmt.getEntries();
        for (SwitchEntry entry : entries) {
            int index = entries.indexOf(entry);

            // label
            String label = "";
            if(entry.getLabels().size() != 0)
                label = entry.getLabels().get(0).toString(new PrettyPrinterConfiguration().setPrintComments(false));
            else
                label = "default";

            // statement
            List<Statement> allStatement = new ArrayList<>();
            findStmt: while(true) {
                for (Statement statement : entries.get(index).getStatements()) {
                    if(statement instanceof BreakStmt)
                        break findStmt;
                    allStatement.add(statement);
                }
                if(entries.size()-1 > index)
                    index ++;
                else break;
            }

            // to nextRef
            List<NextRef> nextRefs = new ArrayList<>();
            allStatement.forEach(statement -> {
                nextRefs.addAll(genNextRefs(statement, methodDeclaration, new ArrayList<>()));
            });

            entryRefQueue.put(label, nextRefs);
        }
        this.entryRefQueue = entryRefQueue;

        Optional<Comment> comment = switchStmt.getComment();
        if(comment.isPresent() && comment.get().isJavadocComment())
            this.taggedInfo = StaticAnalyzer.analyze().comment(comment.get().asJavadocComment());
    }
}
