package io.nextree.analysis.callhierarchy;

import io.nextree.analysis.callhierarchy.dictionary.Dictionary;
import io.nextree.analysis.callhierarchy.info.TaggedInfo;
import io.nextree.analysis.callhierarchy.ref.SwitchBlockRef;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

@Getter
public class SwitchBlockHierarchy extends CallHierarchy{
    private String selectorExpr;
    private TaggedInfo taggedInfo;

    public SwitchBlockHierarchy(Dictionary dictionary, SwitchBlockRef switchBlockRef, Set<String> hierarchyMethodKeySet, Consumer<CallHierarchy> consumer) {
        this.selectorExpr = switchBlockRef.getSelectorExpr();
        this.taggedInfo = switchBlockRef.getTaggedInfo();
        this.type = ASTType.SwitchBlock;

        List<CallHierarchy> children  = new ArrayList<>();
        List<CallHierarchy> hierarchyInSelector = nextHierarchy(switchBlockRef.getSelectorRefQueue(), dictionary, hierarchyMethodKeySet, consumer);
        children.add(new SelectorHierarchy(hierarchyInSelector));
        switchBlockRef.getEntryRefQueue().entrySet().forEach(entry -> {
            List<CallHierarchy> hierarchyInEntry = nextHierarchy(entry.getValue(), dictionary, hierarchyMethodKeySet, consumer);
            children.add(new EntryHierarchy(hierarchyInEntry, entry.getKey()));
        });
        this.children = children;
    }

    @Getter
    public static class SelectorHierarchy extends CallHierarchy {
        private SelectorHierarchy(List<CallHierarchy> children) {
            super(ASTType.SwitchBlock_Selector);
            this.children = children;
        }
    }

    @Getter
    public static class EntryHierarchy extends CallHierarchy {
        private String label;
        private EntryHierarchy(List<CallHierarchy> children, String label) {
            super(ASTType.SwitchBlock_Entry);
            this.children = children;
            this.label = label;
        }
    }
}
