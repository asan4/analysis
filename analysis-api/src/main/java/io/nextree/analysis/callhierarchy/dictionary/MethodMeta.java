package io.nextree.analysis.callhierarchy.dictionary;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.comments.JavadocComment;
import io.nextree.analysis.callhierarchy.info.MetricInfo;
import io.nextree.analysis.callhierarchy.info.TaggedInfo;
import io.nextree.analysis.callhierarchy.qualification.QualifiedMethodSignature;
import io.nextree.analysis.callhierarchy.ref.NextRef;
import io.nextree.analysis.staticanalysis.analyzer.StaticAnalyzer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;

@Getter
public class MethodMeta {
    private QualifiedMethodSignature qualifiedMethodSignature;
    private Feature feature;
    @Nullable
    private MetricInfo metricInfo;
    @Nullable
    private TaggedInfo taggedInfo;
    //
    @Nullable
    private List<NextRef> nextRefQueue;


    private MethodMeta(QualifiedMethodSignature qualifiedMethodSignature, Feature feature) {
        this.qualifiedMethodSignature = qualifiedMethodSignature;
        this.feature = feature;
    }

    public MethodMeta(MethodDeclaration methodDeclaration) {
        qualifiedMethodSignatureBuild(methodDeclaration)
                .featureBuild()
                .nextRefQueueBuild(methodDeclaration)
                .taggedInfoBuild(methodDeclaration);
        this.metricInfo = new MetricInfo(methodDeclaration);
    }

    public static MethodMeta genGetterMethodMeta(String qualifiedClassName, String fieldName) {
        String name = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
        QualifiedMethodSignature qualifiedMethodSignature = QualifiedMethodSignature.fromKey(String.format("%s.%s(%s)", qualifiedClassName, name, ""));
        Feature feature = new Feature(true, false);

        return new MethodMeta(qualifiedMethodSignature, feature);
    }
    public static MethodMeta genSetterMethodMeta(String qualifiedClassName, String fieldName, String fieldType) {
        String name = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
        QualifiedMethodSignature qualifiedMethodSignature = QualifiedMethodSignature.fromKey(String.format("%s.%s(%s)", qualifiedClassName, name, fieldType));
        Feature feature = new Feature(false, true);

        return new MethodMeta(qualifiedMethodSignature, feature);
    }

    @Setter
    @Getter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Feature {
        private boolean getter;
        private boolean setter;
    }

    private MethodMeta qualifiedMethodSignatureBuild(MethodDeclaration methodDeclaration) {
        this.qualifiedMethodSignature =  QualifiedMethodSignature.fromKey(methodDeclaration.resolve().getQualifiedSignature());
        return this;
    }
    private MethodMeta featureBuild() {
        String name = qualifiedMethodSignature.getMethodName();
        this.feature = new Feature(name.startsWith("get"), name.startsWith("set"));
        return this;
    }

    private MethodMeta nextRefQueueBuild(MethodDeclaration methodDeclaration) {
        this.nextRefQueue = NextRef.genNextRefs(methodDeclaration);
        return this;
    }

    private MethodMeta taggedInfoBuild(MethodDeclaration methodDeclaration) {
        StaticAnalyzer staticAnalyzer = StaticAnalyzer.analyze();

        Optional<JavadocComment> comment = methodDeclaration.getJavadocComment();
        this.taggedInfo = comment.map(staticAnalyzer::comment).orElse(null);
        return this;
    }
}
