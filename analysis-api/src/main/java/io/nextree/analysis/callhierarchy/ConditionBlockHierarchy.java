package io.nextree.analysis.callhierarchy;

import io.nextree.analysis.callhierarchy.dictionary.Dictionary;
import io.nextree.analysis.callhierarchy.info.TaggedInfo;
import io.nextree.analysis.callhierarchy.ref.ConditionBlockRef;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

@Getter
public class ConditionBlockHierarchy extends CallHierarchy{
    private String conditionExpr;
    private TaggedInfo taggedInfo;


    public ConditionBlockHierarchy(Dictionary dictionary, ConditionBlockRef conditionBlockRef, Set<String> hierarchyMethodSignatureSet, Consumer<CallHierarchy> consumer) {
        this.type = ASTType.ConditionBlock;
        this.conditionExpr = conditionBlockRef.getConditionExpr();
        this.taggedInfo = conditionBlockRef.getTaggedInfo().get(ASTType.ConditionBlock);

        List<CallHierarchy> callHierarchy = new ArrayList<>();

        List<CallHierarchy> hierarchyInCondition = nextHierarchy(conditionBlockRef.getConditionRefQueue(), dictionary, hierarchyMethodSignatureSet, consumer);
        callHierarchy.add(new ConditionHierarchy(hierarchyInCondition));

        List<CallHierarchy> hierarchyInThen = nextHierarchy(conditionBlockRef.getThenRefQueue(), dictionary, hierarchyMethodSignatureSet, consumer);
        callHierarchy.add(new ThenHierarchy(hierarchyInThen));

        if(conditionBlockRef.getElseRefQueue() != null){
            List<CallHierarchy> hierarchyInElse = nextHierarchy(conditionBlockRef.getElseRefQueue(), dictionary, hierarchyMethodSignatureSet, consumer);
            callHierarchy.add(new ElseHierarchy(hierarchyInElse, conditionBlockRef));
        }

        this.children = callHierarchy;
    }

    @Getter
    public static class ConditionHierarchy extends CallHierarchy {
        private ConditionHierarchy(List<CallHierarchy> children) {
            super(ASTType.ConditionBlock_Condition);
            this.children = children;
        }
    }
    @Getter

    public static class ThenHierarchy extends CallHierarchy {
        private ThenHierarchy(List<CallHierarchy> children) {
            super(ASTType.ConditionBlock_Then);
            this.children = children;
        }
    }

    @Getter
    public static class ElseHierarchy extends CallHierarchy {
        private TaggedInfo taggedInfo;
        private ElseHierarchy(List<CallHierarchy> children, ConditionBlockRef conditionBlockRef) {
            super(ASTType.ConditionBlock_Else);
            this.children = children;
            this.taggedInfo = conditionBlockRef.getTaggedInfo().get(ASTType.ConditionBlock_Else);
        }
    }
}
