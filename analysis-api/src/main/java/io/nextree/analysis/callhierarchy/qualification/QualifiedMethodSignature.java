package io.nextree.analysis.callhierarchy.qualification;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class QualifiedMethodSignature {
    private QualifiedClassName qualifiedClassName;
    private String methodName;
    private List<QualifiedClassName> qualifiedParamNames;

    public String getValueAsString() {
        List<String> qualifiedParamNames = this.qualifiedParamNames.stream().map(QualifiedClassName::getValueAsString).collect(Collectors.toList());
        String params = String.join(", ", qualifiedParamNames);

        return String.format("%s.%s(%s)", this.qualifiedClassName.getValueAsString(), this.methodName, params);
    }

    public String getValueAsShortString() {
        return String.format("%s.%s(%s)",
                this.qualifiedClassName.getValueAsShortString(),
                this.methodName,
                this.qualifiedParamNames.stream().map(QualifiedClassName::getValueAsShortString).collect(Collectors.joining(", "))
            );
    }

    public static QualifiedMethodSignature fromKey(String key) {
        String classAndMethodName = key.substring(0, key.indexOf("("));
        String canonicalClassName = classAndMethodName.substring(0, classAndMethodName.lastIndexOf("."));
        String methodName = classAndMethodName.substring(classAndMethodName.lastIndexOf(".") + 1);
        String params = key.substring(key.indexOf("(") + 1, key.indexOf(")"));
        List<QualifiedClassName> paramList = new ArrayList<>();
        if(!params.equals(""))
            paramList = Arrays.asList(params.split(", ")).stream().map(QualifiedClassName::fromCanonicalClassName).collect(Collectors.toList());
        return new QualifiedMethodSignature(QualifiedClassName.fromCanonicalClassName(canonicalClassName), methodName, paramList);
    }
}
