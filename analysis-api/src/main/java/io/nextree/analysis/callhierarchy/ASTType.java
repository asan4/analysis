package io.nextree.analysis.callhierarchy;

public enum ASTType {
    Method,
    ConditionBlock,
    ConditionBlock_Condition,
    ConditionBlock_Then,
    ConditionBlock_Else,
    SwitchBlock,
    SwitchBlock_Selector,
    SwitchBlock_Entry,
    Return,
    Throw
}
