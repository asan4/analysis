package io.nextree.analysis.callhierarchy.ref;

import com.github.javaparser.ast.stmt.ThrowStmt;
import io.nextree.analysis.callhierarchy.ASTType;
import lombok.Getter;

@Getter
public class  ThrowRef extends NextRef {
    private String throwStmt;

    public ThrowRef(ThrowStmt node) {
        super(ASTType.Throw);
        genRef(node);
    }

    private void genRef(ThrowStmt throwStmt) {
        this.throwStmt = throwStmt.getExpression().toString();
    }
}
