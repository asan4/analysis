package io.nextree.analysis.callhierarchy;

import io.nextree.analysis.callhierarchy.dictionary.ClassMeta;
import io.nextree.analysis.callhierarchy.dictionary.Dictionary;
import io.nextree.analysis.callhierarchy.dictionary.MethodMeta;
import io.nextree.analysis.callhierarchy.info.MetricInfo;
import io.nextree.analysis.callhierarchy.info.TaggedInfo;
import io.nextree.analysis.callhierarchy.qualification.QualifiedMethodSignature;
import io.nextree.analysis.callhierarchy.ref.MethodRef;
import lombok.Getter;
import org.springframework.lang.Nullable;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

@Getter
public class MethodHierarchy extends CallHierarchy{
    private QualifiedMethodSignature qualifiedMethodSignature;
    private ClassMeta.Feature classFeature;
    @Nullable
    private TaggedInfo classTaggedInfo;
    private MethodMeta.Feature methodFeature;
    @Nullable
    private TaggedInfo methodTaggedInfo;
    @Nullable
    private MetricInfo methodMetricInfo;
    private boolean isCircularCall;
    private boolean isExternalMethodCall;

    public MethodHierarchy() {
        super(ASTType.Method);
    }

    public MethodHierarchy(Dictionary dictionary, ClassMeta classMeta, MethodMeta methodMeta, Set<String> hierarchyMethodSignatureSet, Consumer<CallHierarchy> consumer) {
        this.qualifiedMethodSignature = methodMeta.getQualifiedMethodSignature();
        this.classFeature = classMeta.getFeature();
        this.classTaggedInfo = classMeta.getTaggedInfo();
        this.methodFeature = methodMeta.getFeature();
        this.methodTaggedInfo = methodMeta.getTaggedInfo();
        this.methodMetricInfo = methodMeta.getMetricInfo();
        this.type = ASTType.Method;
        if(methodMeta.getNextRefQueue() != null)
            this.children = nextHierarchy(methodMeta.getNextRefQueue(), dictionary, hierarchyMethodSignatureSet, consumer);
        this.isCircularCall = false;
        this.isExternalMethodCall = false;
        consumer.accept(this);
    }

    // TODO: 여기부터
    public MethodHierarchy(MethodRef methodRef) {
        // externalMethodCall
        this.qualifiedMethodSignature = methodRef.getQualifiedMethodSignature();
        this.type = ASTType.Method;
        this.isCircularCall = false;
        this.isExternalMethodCall = true;
    }

    public MethodHierarchy(MethodMeta methodMeta) {
        // Circular Call
        this.qualifiedMethodSignature = methodMeta.getQualifiedMethodSignature();
        this.type = ASTType.Method;
        this.methodMetricInfo = methodMeta.getMetricInfo();
        this.isCircularCall = true;
        this.isExternalMethodCall = false;
    }

    public static MethodHierarchy fromCallObjects(Dictionary dictionary, MethodRef methodRef, Set<String> parentHierarchyMethodKeySet, Consumer<CallHierarchy> consumer) {
        QualifiedMethodSignature qualifiedMethodSignature = methodRef.getQualifiedMethodSignature();
        if(methodRef.getQualifiedMethodSignature() == null)
            return null;

        Optional<MethodMeta> methodMeta = dictionary.findMethodMeta(qualifiedMethodSignature.getValueAsString());

        if(methodMeta.isEmpty()) {
            return new MethodHierarchy(methodRef);
        }

        // dictionary 존재하지 않는 경우
        Optional<ClassMeta> classMeta = dictionary.findClassMeta(qualifiedMethodSignature.getQualifiedClassName().getValueAsString());
        if(classMeta.isEmpty())
            return null;

        // CircularCall
        if(parentHierarchyMethodKeySet.contains(qualifiedMethodSignature.getValueAsString())){
            return new MethodHierarchy(methodMeta.get());
        }

        Set<String> hierarchyMethodKeySet = new LinkedHashSet<>(parentHierarchyMethodKeySet);
        hierarchyMethodKeySet.add(qualifiedMethodSignature.getValueAsString());
        return new MethodHierarchy(dictionary, classMeta.get(), methodMeta.get(), hierarchyMethodKeySet, consumer);
    }

}
