package io.nextree.analysis.callhierarchy;

import io.nextree.analysis.callhierarchy.dictionary.Dictionary;
import io.nextree.analysis.callhierarchy.ref.ReturnRef;
import lombok.Getter;

import java.util.Set;
import java.util.function.Consumer;


@Getter
public class ReturnHierarchy extends CallHierarchy{
    private String returnStmt;

    public ReturnHierarchy(Dictionary dictionary, ReturnRef returnRef, Set<String> hierarchyMethodSignatureSet, Consumer<CallHierarchy> consumer) {
        this.returnStmt = returnRef.getReturnStmt();
        this.type = ASTType.Return;
        if (returnRef.getReturnRefQueue() != null)
            this.children = nextHierarchy(returnRef.getReturnRefQueue(), dictionary, hierarchyMethodSignatureSet, consumer);
    }
}
