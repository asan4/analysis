package io.nextree.analysis.callhierarchy.ref;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.stmt.ReturnStmt;
import io.nextree.analysis.callhierarchy.ASTType;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class ReturnRef extends NextRef {
    private String returnStmt;
    private List<NextRef> returnRefQueue;

    public ReturnRef(ReturnStmt node, MethodDeclaration methodDeclaration) {
        super(ASTType.Return);
        genRef(node, methodDeclaration);
    }

    private void genRef(ReturnStmt node, MethodDeclaration methodDeclaration) {
        String returnStmt = "";
        if(node.getExpression().isPresent())
            returnStmt = node.getExpression().get().toString();
        // nextKey 생성
        List<NextRef> nextRefs = new ArrayList<>();
        if(node.getChildNodes().size() != 0) {
            for (Node childNode : node.getChildNodes()) {
                nextRefs = genNextRefs(childNode, methodDeclaration, nextRefs);
            }
        }

        this.returnStmt = returnStmt;
        this.returnRefQueue = nextRefs;
    }
}
