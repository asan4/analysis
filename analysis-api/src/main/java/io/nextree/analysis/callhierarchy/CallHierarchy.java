package io.nextree.analysis.callhierarchy;

import io.nextree.analysis.callhierarchy.dictionary.ClassMeta;
import io.nextree.analysis.callhierarchy.dictionary.Dictionary;
import io.nextree.analysis.callhierarchy.dictionary.MethodMeta;
import io.nextree.analysis.callhierarchy.qualification.QualifiedMethodSignature;
import io.nextree.analysis.callhierarchy.ref.*;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@NoArgsConstructor
@Getter
public abstract class CallHierarchy {
    protected ASTType type;
    protected List<CallHierarchy> children;

    protected CallHierarchy(ASTType type) {
        this.type = type;
    }

    public static CallHierarchy makeCallHierarchy(Dictionary dictionary, QualifiedMethodSignature entryPointSignature, Consumer<CallHierarchy> consumer) {
        ClassMeta classMeta = dictionary.findClassMeta(entryPointSignature.getQualifiedClassName().getValueAsString())
                .orElseThrow(() -> new NoSuchElementException(String.format("No CallClass with key : %s", entryPointSignature.getQualifiedClassName().getValueAsString())));
        MethodMeta methodMeta = dictionary.findMethodMeta(entryPointSignature.getValueAsString())
                .orElseThrow(() -> new NoSuchElementException(String.format("No CallMethod with key : %s", entryPointSignature.getValueAsString())));

        return new MethodHierarchy(dictionary, classMeta, methodMeta, new LinkedHashSet<>(), consumer);
    }

    public static CallHierarchy makeCallHierarchy(Dictionary dictionary, QualifiedMethodSignature entryPointSignature) {
        return makeCallHierarchy(dictionary, entryPointSignature, c -> {});
    }

    public List<CallHierarchy> nextHierarchy(List<NextRef> nextRefs, Dictionary dictionary, Set<String> hierarchyMethodSignatureSet, Consumer<CallHierarchy> consumer) {
        return nextRefs.stream().map(nextKey -> nextHierarchy(dictionary, nextKey, hierarchyMethodSignatureSet, consumer))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public CallHierarchy nextHierarchy(Dictionary dictionary, NextRef nextRef, Set<String> hierarchyMethodSignatureSet, Consumer<CallHierarchy> consumer) {
        if(nextRef instanceof ConditionBlockRef)
            return new ConditionBlockHierarchy(dictionary, (ConditionBlockRef) nextRef, hierarchyMethodSignatureSet, consumer);
        if(nextRef instanceof SwitchBlockRef)
            return new SwitchBlockHierarchy(dictionary, (SwitchBlockRef) nextRef, hierarchyMethodSignatureSet, consumer);
        if(nextRef instanceof ReturnRef)
            return new ReturnHierarchy(dictionary, (ReturnRef) nextRef, hierarchyMethodSignatureSet, consumer);
        if(nextRef instanceof ThrowRef)
            return new ThrowHierarchy((ThrowRef) nextRef);
        return MethodHierarchy.fromCallObjects(dictionary, (MethodRef) nextRef, hierarchyMethodSignatureSet, consumer);
    }
}
