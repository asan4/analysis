package io.nextree.analysis.callhierarchy.info;

import com.github.javaparser.ast.body.MethodDeclaration;
import io.nextree.analysis.staticanalysis.analyzer.StaticAnalyzer;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MetricInfo {
    private Long cognitiveComplexity;

    public MetricInfo(MethodDeclaration methodDeclaration) {
        this.cognitiveComplexity = StaticAnalyzer.analyze().cognitiveComplexity(methodDeclaration);
    }
}
