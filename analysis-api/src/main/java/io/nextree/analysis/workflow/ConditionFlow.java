package io.nextree.analysis.workflow;

import io.nextree.analysis.callhierarchy.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class ConditionFlow extends Workflow {
    public static Workflow fromConditionalCall(CallHierarchy callHierarchy, Predicate<CallHierarchy> predicate) {
        if (callHierarchy instanceof ConditionBlockHierarchy) return new ConditionFlow((ConditionBlockHierarchy) callHierarchy, predicate);
        else if (callHierarchy instanceof SwitchBlockHierarchy) return new ConditionFlow((SwitchBlockHierarchy) callHierarchy, predicate);
        else throw new IllegalArgumentException("CallHierarchy has invalid type: " + callHierarchy.getType());
    }

    public ConditionFlow(ConditionBlockHierarchy hierarchy, Predicate<CallHierarchy> predicate) {
        this.callHierarchyRefKey = hierarchy.getConditionExpr();
        this.type = fromHierarchyType(hierarchy);
        this.fromTaggedInfo(hierarchy.getTaggedInfo());
        this.description = Optional.ofNullable(hierarchy.getConditionExpr()).orElse(hierarchy.getType().name());
        if (Objects.nonNull(hierarchy.getChildren()) && !hierarchy.getChildren().isEmpty()) {
            this.children = (hierarchy.getChildren().stream().filter(predicate).map(Workflow::fromCallHierarchy).collect(Collectors.toList()));
            this.children.forEach(child -> child.setParent(this));
        } else this.children = new ArrayList<>();
    }
    public ConditionFlow(SwitchBlockHierarchy hierarchy, Predicate<CallHierarchy> predicate) {
        // TODO
    }

    private WorkflowType fromHierarchyType(CallHierarchy hierarchy) {
        switch (hierarchy.getType()) {
            case Throw:
                return WorkflowType.Throw;
            case Return:
                return WorkflowType.Return;
            case ConditionBlock:
                return WorkflowType.ConditionGroup;
            case ConditionBlock_Condition:
                return WorkflowType.Condition;
            case ConditionBlock_Then:
                return WorkflowType.Then;
            case ConditionBlock_Else:
                return WorkflowType.Else;
            case Method:
                return WorkflowType.Flow;
            case SwitchBlock:
            case SwitchBlock_Entry:
            case SwitchBlock_Selector:
                return WorkflowType.Switch;
            default:
                throw new IllegalArgumentException();
        }
    }
}
