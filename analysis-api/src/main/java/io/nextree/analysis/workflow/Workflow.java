package io.nextree.analysis.workflow;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.nextree.analysis.callhierarchy.ASTType;
import io.nextree.analysis.callhierarchy.CallHierarchy;
import io.nextree.analysis.callhierarchy.ConditionBlockHierarchy;
import io.nextree.analysis.callhierarchy.MethodHierarchy;
import io.nextree.analysis.callhierarchy.info.TaggedInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class Workflow {
    protected String key;
    protected List<String> tags;
    protected String description;
    protected String callHierarchyRefKey;
    protected WorkflowType type;
    @Nullable
    protected WorkflowGroup group;
    //
    protected List<Workflow> children;
    @JsonIgnore
    protected Workflow parent;

    public static Workflow fromCallHierarchy(CallHierarchy callHierarchy) {
        return fromCallHierarchy(callHierarchy, Predication.isWorkflowDefinedAnyChildren);
    }

    public static Workflow fromCallHierarchy(CallHierarchy callHierarchy, Predicate<CallHierarchy> predicate) {
        switch (callHierarchy.getType()) {
            case Method:
            case Throw:
            case Return:
                return ActivityFlow.fromActivityCall(callHierarchy, predicate);
            case ConditionBlock:
            case ConditionBlock_Condition:
            case ConditionBlock_Then:
            case ConditionBlock_Else:
            case SwitchBlock:
            case SwitchBlock_Selector:
            case SwitchBlock_Entry:
                return ConditionFlow.fromConditionalCall(callHierarchy, predicate);
            default:
                throw new IllegalArgumentException();
        }
    }

    protected void fromTaggedInfo(TaggedInfo taggedInfo) {
        if (Objects.isNull(taggedInfo)) {
            this.key = "";
            this.tags = new ArrayList<>();
            this.description = "";
        } else {
            this.key = Optional.ofNullable(taggedInfo.get("workflow")).map(String.class::cast).orElse("");
            this.tags = Optional.ofNullable(taggedInfo.get("tags")).map(ts -> (List<String>) ts).orElse(new ArrayList<>());
            this.description = Optional.ofNullable(taggedInfo.get("description")).map(String.class::cast).orElse("");
        }
    }

    protected void fromGroupTaggedInfo(TaggedInfo groupTaggedInfo) {
        if (groupTaggedInfo != null && groupTaggedInfo.containsKey("workflow")) {
            this.group = WorkflowGroup.builder()
                    .key((String) groupTaggedInfo.get("workflow"))
                    .tags(Optional.ofNullable(groupTaggedInfo.get("tags")).map(ts -> (List<String>) ts).orElse(new ArrayList<>()))
                    .description(Optional.ofNullable(groupTaggedInfo.get("description")).map(String.class::cast).orElse(""))
                    .build();
        }
    }


    public static class Predication {
        public static final Predicate<CallHierarchy> isTaggedInfoDefined = ch -> Optional.ofNullable(ch.getType().equals(ASTType.Method) ? ((MethodHierarchy)ch).getMethodTaggedInfo() : null).isPresent();
        public static final Predicate<CallHierarchy> isWorkflowDefinedInMethod = ch -> isTaggedInfoDefined.test(ch) && Optional.ofNullable(ch.getType().equals(ASTType.Method) ? ((MethodHierarchy)ch).getMethodTaggedInfo().get("workflow") : null).map(v -> !((String)v).isBlank()).orElse(false);
        public static final Predicate<CallHierarchy> isWorkflowDefinedInCondition = ch -> List.of(ASTType.ConditionBlock, ASTType.ConditionBlock_Condition).contains(ch.getType()) &&
                Optional.ofNullable(((ConditionBlockHierarchy) ch).getTaggedInfo()).isPresent() &&
                Optional.ofNullable(((ConditionBlockHierarchy) ch).getTaggedInfo().get("workflow")).isPresent();
        public static final Predicate<CallHierarchy> isWorkflowDefined = ch -> isWorkflowDefinedInMethod.test(ch) || isWorkflowDefinedInCondition.test(ch);
        public static final Predicate<CallHierarchy> isWorkflowDefinedAnyChildren = ch -> {
            boolean matched = isWorkflowDefined.test(ch);
            if (!matched && Objects.nonNull(ch.getChildren()) && !ch.getChildren().isEmpty()) {
                return ch.getChildren().stream().anyMatch(child -> anyChildrenMatch(child, isWorkflowDefined));
            } else {
                return matched;
            }
        };

        private static boolean anyChildrenMatch(CallHierarchy callHierarchy, Predicate<CallHierarchy> predicate) {
            boolean matched = predicate.test(callHierarchy);
            if (!matched && Objects.nonNull(callHierarchy.getChildren()) && !callHierarchy.getChildren().isEmpty()) {
                return callHierarchy.getChildren().stream().anyMatch(child -> anyChildrenMatch(child, predicate));
            } else {
                return matched;
            }
        }
    }

//    public static class WorkflowBuilder {
//        public WorkflowBuilder fromTaggedInfo(TaggedInfo taggedInfo) {
//            if (Objects.isNull(taggedInfo)) return this.key("").tags(new ArrayList<>()).description("");
//            else return this
//                    .key(Optional.ofNullable(taggedInfo.get("workflow")).map(String.class::cast).orElse(""))
//                    .tags(Optional.ofNullable(taggedInfo.get("tags")).map(ts -> (List<String>) ts).orElse(new ArrayList<>()))
//                    .description(Optional.ofNullable(taggedInfo.get("description")).map(String.class::cast).orElse(""));
//        }
//
//        public WorkflowBuilder children(CallHierarchy callHierarchy, Predicate<CallHierarchy> predicate) {
//            if (Objects.nonNull(callHierarchy.getChildren()) && !callHierarchy.getChildren().isEmpty()) {
//                return this.children(callHierarchy.getChildren().stream().filter(predicate).map(Workflow::fromCallHierarchy).collect(Collectors.toList()));
//            } else return this;
//        }
//
//        public WorkflowBuilder children(List<Workflow> workflows) {
//            this.children = workflows;
//            return this;
//        }
//
//        public WorkflowBuilder type(CallHierarchyType type) {
//            switch (type) {
//                case Method:
//                    this.type = WorkflowType.Flow;
//                    break;
//                case Condition:
//                    this.type = WorkflowType.Condition;
//                    break;
//                case ConditionGroup:
//                    this.type = WorkflowType.ConditionGroup;
//                    break;
//                case Return:
//                    this.type = WorkflowType.Return;
//                    break;
//                case Throw:
//                    this.type = WorkflowType.Throw;
//                    break;
//                case Switch:
//                case SwitchEntry:
//                case SwitchSelector:
//                    this.type = WorkflowType.Switch;
//                    break;
//            }
//            return this;
//        }
//    }
}
