package io.nextree.analysis.workflow;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkflowGroup {
    private String key;
    private List<String> tags;
    private String description;
    //
}
