package io.nextree.analysis.workflow;

import io.nextree.analysis.callhierarchy.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class ActivityFlow extends Workflow {
    public static Workflow fromActivityCall(CallHierarchy callHierarchy, Predicate<CallHierarchy> predicate) {
        if (callHierarchy instanceof MethodHierarchy) return new ActivityFlow((MethodHierarchy) callHierarchy, predicate);
        else if (callHierarchy instanceof ThrowHierarchy) return new ActivityFlow((ThrowHierarchy) callHierarchy, predicate);
        else if (callHierarchy instanceof ReturnHierarchy) return new ActivityFlow((ReturnHierarchy) callHierarchy, predicate);
        else throw new IllegalArgumentException("CallHierarchy has invalid type: " + callHierarchy.getType());
    }

    public ActivityFlow(MethodHierarchy hierarchy, Predicate<CallHierarchy> predicate) {
        this.type = WorkflowType.Flow;
        this.callHierarchyRefKey = hierarchy.getQualifiedMethodSignature().getValueAsString();
        this.fromTaggedInfo(hierarchy.getMethodTaggedInfo());
        this.fromGroupTaggedInfo(hierarchy.getClassTaggedInfo());
        try {
            this.children = Optional.ofNullable((hierarchy.getChildren())
                            .stream().filter(predicate)
                            .map(Workflow::fromCallHierarchy)
                            .collect(Collectors.toList()))
                    .orElseGet(ArrayList::new);
        } catch (NullPointerException e) {
            this.children = new ArrayList<>();
        } finally {
            this.children.forEach((child) -> child.parent = this);
        }
    }

    public ActivityFlow(ReturnHierarchy hierarchy, Predicate<CallHierarchy> predicate) {
        this.type = WorkflowType.Return;
        this.callHierarchyRefKey = hierarchy.getType().name();
        this.description = Optional.ofNullable(hierarchy.getReturnStmt()).orElse("Return");
        if (Objects.nonNull(hierarchy.getChildren()) && !hierarchy.getChildren().isEmpty()) {
            this.children = (hierarchy.getChildren().stream().filter(predicate).map(Workflow::fromCallHierarchy).collect(Collectors.toList()));
            this.children.forEach(wf -> wf.setParent(this));
        }
    }

    public ActivityFlow(ThrowHierarchy hierarchy, Predicate<CallHierarchy> predicate) {
        this.type = WorkflowType.Return;
        this.callHierarchyRefKey = hierarchy.getType().name();
        this.description = Optional.ofNullable(hierarchy.getThrowStmt()).orElse("Throw");
        if (Objects.nonNull(hierarchy.getChildren()) && !hierarchy.getChildren().isEmpty()) {
            this.children = (hierarchy.getChildren().stream().filter(predicate).map(Workflow::fromCallHierarchy).collect(Collectors.toList()));
            this.children.forEach(wf -> wf.setParent(this));
        }
    }
}
