package io.nextree.analysis.workflow;

public enum WorkflowType {
    Flow,
    ConditionGroup,
    Condition,
    Return,
    Throw,
    Switch,
    Then,
    Else
}
