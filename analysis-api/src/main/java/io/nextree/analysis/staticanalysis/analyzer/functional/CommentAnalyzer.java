package io.nextree.analysis.staticanalysis.analyzer.functional;

import com.github.javaparser.ast.comments.JavadocComment;
import io.nextree.analysis.callhierarchy.info.TaggedInfo;

import java.util.*;
import java.util.stream.Collectors;

public interface CommentAnalyzer {
    String TAG_PREFIX = "@";
    String LINE_SEPARATOR = System.lineSeparator();
    String LIST_VALUE_DELIMITER = ",";
    String LIST_VALUE_START = "[";
    String LIST_VALUE_END = "]";
    String MULTI_LINE_INDICATOR = "<<";

    /**
     * @workflow 의약품 처방
     * @tags [x,y,z]
     * @description <<EOT
     * Hello!
     * World!
     * EOT
     */
    default TaggedInfo comment(JavadocComment comment) {
        String content = comment.getContent();
        Map<String, Object> taggedInfo = new HashMap<>();

        String multilineKey = null;
        String multilineToken = null;
        for (String lineString : content.split(LINE_SEPARATOR)) {
            String refinedLineString = lineString.trim();
            if (refinedLineString.startsWith("*")) {
                refinedLineString = refinedLineString.substring(1).trim();
            }
            //
            if (refinedLineString.startsWith(TAG_PREFIX)) {
                if (multilineToken != null || multilineKey != null) {
                    multilineKey = null;
                    multilineToken = null;
                }
                try {
                    int tagEndIdx = refinedLineString.indexOf(" ");
                    String tagKey = refinedLineString.substring(1, tagEndIdx);
                    String valueString = refinedLineString.substring(tagEndIdx+1);

                    if (valueString.startsWith(MULTI_LINE_INDICATOR)) {
                        multilineKey = tagKey;
                        multilineToken = valueString.substring(MULTI_LINE_INDICATOR.length());
                    } else if (valueString.startsWith(LIST_VALUE_START) && valueString.endsWith(LIST_VALUE_END)) {
                        List<String> listValues = Arrays.stream(valueString.substring(1, valueString.length()-1).split(LIST_VALUE_DELIMITER)).map(String::trim).collect(Collectors.toList());
                        taggedInfo.put(tagKey, listValues);
                    } else {
                        taggedInfo.put(tagKey, valueString);
                    }
                } catch (Exception ignored) {}
            } else if (multilineKey != null && multilineToken != null) {
                if (refinedLineString.equals(multilineToken)) {
                    multilineKey = null;
                    multilineToken = null;
                } else {
                    String enhancedValue = Optional.ofNullable(taggedInfo.get(multilineKey))
                            .map(origin -> ((String) origin).concat("\n"))
                            .orElse("")
                            .concat(refinedLineString);
                    taggedInfo.put(multilineKey, enhancedValue);
                }
            } else {
                multilineKey = null;
                multilineToken = null;
            }
        }
        return new TaggedInfo(taggedInfo);
    }
}
