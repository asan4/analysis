package io.nextree.analysis.staticanalysis.analyzer;


import io.nextree.analysis.staticanalysis.analyzer.functional.*;

public class StaticAnalyzer implements
        ASTAnalyzer,
        CognitiveComplexityAnalyzer,
        CommentAnalyzer,
        ProjectDictionaryAnalyzer
{public static StaticAnalyzer analyze() {
        return new StaticAnalyzer();
    }}
