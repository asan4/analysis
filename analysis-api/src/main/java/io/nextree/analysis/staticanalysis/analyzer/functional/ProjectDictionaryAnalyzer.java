package io.nextree.analysis.staticanalysis.analyzer.functional;

import com.github.javaparser.ast.CompilationUnit;
import io.nextree.analysis.callhierarchy.dictionary.ClassMeta;
import io.nextree.analysis.callhierarchy.dictionary.Dictionary;

import java.util.Map;

public interface ProjectDictionaryAnalyzer {
    default Dictionary makeCallDictionary(Map<String, CompilationUnit> compilationUnits) {
        Dictionary dictionary = Dictionary.init();

        // callClassMap / callMethodMap 생성
        compilationUnits.values().forEach(unit -> {
            ClassMeta classMeta = new ClassMeta(unit);
            dictionary.addClassMeta(classMeta);
            classMeta.getMethods().forEach(dictionary::addMethodMeta);
        });

        return dictionary;
    }
}
