package io.nextree.analysis.staticanalysis.analyzer.functional;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.expr.CastExpr;
import com.github.javaparser.ast.expr.ConditionalExpr;
import com.github.javaparser.ast.stmt.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface CognitiveComplexityAnalyzer {

    List<String> complexityIncreasingElementNames = Stream.of(
            BinaryExpr.class,
            BreakStmt.class,
            CatchClause.class,
            ConditionalExpr.class,
            ContinueStmt.class,
            DoStmt.class,
            ForEachStmt.class, ForStmt.class,
            IfStmt.class,
            ReturnStmt.class,
            SwitchStmt.class,
            CastExpr.class,
            ThrowStmt.class
    ).map(Class::getName).collect(Collectors.toList());

    default long cognitiveComplexity(MethodDeclaration method) {
        return method.getBody().map(body ->
                body.getStatements().stream()
                        .map(this::cognitiveComplexity)
                        .reduce(1L, (a,b) -> a+b)
        ).orElse(1L);
    }

    default long cognitiveComplexity(Node node) {
        long score = 0;
        if (complexityIncreasingElementNames.contains(node.getClass().getName())) {
            score++;
        }
        if (!node.getChildNodes().isEmpty()) {
            score += node.getChildNodes().stream().map(this::cognitiveComplexity)
                    .reduce(0L, Long::sum);
        }

        return score;
    }
}
