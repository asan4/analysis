package io.nextree.analysis.staticanalysis;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javaparser.ParserConfiguration;
import com.github.javaparser.ast.CompilationUnit;
import io.nextree.analysis.callhierarchy.CallHierarchy;
import io.nextree.analysis.callhierarchy.dictionary.Dictionary;
import io.nextree.analysis.callhierarchy.qualification.QualifiedMethodSignature;
import io.nextree.analysis.plantuml.sequence.converter.factory.WorkflowFunctionFactory;
import io.nextree.analysis.plantuml.sequence.converter.impl.WorkflowConverter;
import io.nextree.analysis.staticanalysis.analyzer.StaticAnalyzer;
import io.nextree.analysis.workflow.Workflow;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StopWatch;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

@Slf4j
public class StaticAnalysis {
    private static final StaticAnalyzer STATIC_STATIC_ANALYZER = StaticAnalyzer.analyze();
    private final Configuration configuration;
    private final StopWatch stopWatch;
    private Set<Task> _tasks;
    private final Store.StoreBuilder _store;
    private final Artifacts.ArtifactsBuilder _artifacts;

    private StaticAnalysis(Configuration configuration) {
        this.configuration = configuration;
        this._tasks = new HashSet<>();
        this._store = Store.builder();
        this._artifacts = Artifacts.builder();
        this.stopWatch = new StopWatch(StaticAnalysis.class.getSimpleName());
    }

    public static StaticAnalysis start(Configuration configuration) {
        StaticAnalysis staticAnalysis = new StaticAnalysis(configuration);
        log.debug("started with source({})", configuration.getTargetPath().toAbsolutePath());
        return staticAnalysis;
    }

    public StaticAnalysis store(Store store) {
        this._store.compilationUnits(store.getCompilationUnits())
                .dictionary(store.getDictionary());
        this._tasks.addAll(Set.of(Task.CompilationUnit, Task.CallDictionary));
        return this;
    }

    public StaticAnalysis parse() throws IOException {
        stopWatch.start(Task.CompilationUnit.name());
        log.debug("Task[{}] try...", Task.CompilationUnit);
        _store.compilationUnits(STATIC_STATIC_ANALYZER.parseProject(configuration.getTargetPath(), configuration.getParserConfiguration()));
        this._tasks.add(Task.CompilationUnit);
        stopWatch.stop();
        log.debug("Task[{}] is done. {} Compilation Unit is created. \n{}", Task.CompilationUnit.name(), _store.build().getCompilationUnits().size(), stopWatch.prettyPrint());
        return this;
    }

    public StaticAnalysis callDictionary() {
        stopWatch.start(Task.CallDictionary.name());
        log.debug("Task[{}] try...", Task.CallDictionary);
        checkRequiredTasks("callDictionary", Task.CompilationUnit);

        _store.dictionary(STATIC_STATIC_ANALYZER.makeCallDictionary(_store.build().getCompilationUnits()));

        this._tasks.add(Task.CallDictionary);
        stopWatch.stop();
        log.debug("Task[{}] is done. {} classes, {} methods are stored in dictionary...\n{}", Task.CallDictionary.name(), _store.build().getDictionary().sizeOfClassMetaMap(), _store.build().getDictionary().sizeOfMethodMetaMap(), stopWatch.prettyPrint());
        return this;
    }

    public StaticAnalysis callHierarchy(QualifiedMethodSignature entryPoint, Consumer<CallHierarchy> accumulator) {
        stopWatch.start(Task.CallHierarchy.name());
        log.debug("Task[{}] try...", Task.CallHierarchy);
        checkRequiredTasks("callHierarchy", Task.CallDictionary);

        _artifacts.callHierarchy(CallHierarchy.makeCallHierarchy(_store.build().getDictionary(), entryPoint, accumulator));
        this._tasks.add(Task.CallHierarchy);
        stopWatch.stop();
        log.debug("Task[{}] is done...\n{}", Task.CallHierarchy.name(), stopWatch.prettyPrint());
        return this;
    }

    public StaticAnalysis callHierarchy(QualifiedMethodSignature entryPoint) {
        return callHierarchy(entryPoint, c -> {});
    }

    public StaticAnalysis workflow(Predicate<CallHierarchy> callHierarchyPredicate) {
        log.debug("Task[{}] try...", Task.Workflow.name());
        stopWatch.start(Task.Workflow.name());
        checkRequiredTasks("workflow", Task.CallHierarchy);

        _artifacts.workflow(Workflow.fromCallHierarchy(_artifacts.build().getCallHierarchy(), callHierarchyPredicate));
        this._tasks.add(Task.Workflow);
        stopWatch.stop();
        log.debug("Task[{}] is done...\n{}", Task.Workflow.name(), stopWatch.prettyPrint());
        return this;
    }

    public StaticAnalysis workflow() {
        log.debug("Task[{}] try...", Task.Workflow.name());
        stopWatch.start(Task.Workflow.name());
        checkRequiredTasks("workflow", Task.CallHierarchy);

        _artifacts.workflow(Workflow.fromCallHierarchy(_artifacts.build().getCallHierarchy()));
        this._tasks.add(Task.Workflow);
        stopWatch.stop();
        log.debug("Task[{}] is done...\n{}", Task.Workflow.name(), stopWatch.prettyPrint());
        return this;
    }

    public StaticAnalysis workflow(QualifiedMethodSignature entryPoint, Predicate<CallHierarchy> callHierarchyPredicate) {
        return callHierarchy(entryPoint)
                .workflow(callHierarchyPredicate);
    }

    public StaticAnalysis workflow(QualifiedMethodSignature entryPoint) {
        return callHierarchy(entryPoint)
                .workflow();
    }

    public StaticAnalysis plantUML() {
        log.debug("Task[{}] try...", "PlantUML");
        stopWatch.start("PlantUML");
        checkRequiredTasks("plantUML", Task.Workflow);
        WorkflowConverter proxy = new WorkflowConverter(
                WorkflowFunctionFactory.defaultNoteSelector,
                WorkflowFunctionFactory.defaultParticipantTypeSelector,
                WorkflowFunctionFactory.defaultParticipantFilter
        );
        _artifacts.plantUML(proxy.from(_artifacts.build().getWorkflow()).build());
        stopWatch.stop();
        log.debug("Task[{}] is done..., \n{}", "PlantUML", stopWatch.prettyPrint());
        return this;
    }

    private void checkRequiredTasks(String action, Task... requiredTasks) {
        if (Arrays.stream(requiredTasks).allMatch(requiredTask -> this._tasks.contains(requiredTask))) {
            return;
        }
        throw new IllegalStateException(String.format("For '%s' action, [%s] tasks are required. But current tasks is %s", action, Arrays.stream(requiredTasks).map(Task::name).reduce("", (a, b) -> a.concat(", ").concat(b)), this._tasks));
    }

    //
    public Artifacts artifacts() {
        return this._artifacts.build();
    }

    public Store store() {
        return this._store.build();
    }

    private enum Task {
        CompilationUnit,
        CallDictionary,
        CallHierarchy,
        Workflow,
    }

    @Getter
    public static class Configuration {
        private final Path targetPath;
        private final ParserConfiguration parserConfiguration;

        public Configuration(Path targetPath, ParserConfiguration parserConfiguration) {
            this.targetPath = targetPath;
            this.parserConfiguration = parserConfiguration;
            validate();
        }

        public Configuration(Path targetPath) {
            this(targetPath, new ParserConfiguration());
        }

        public void validate() {
            if (!Files.isDirectory(targetPath)) {
                throw new IllegalArgumentException(String.format("TargetPath is not directory. %s", targetPath.toAbsolutePath().toString()));
            }
        }
    }

    @Getter
    @Builder
    public static class Artifacts {
        private CallHierarchy callHierarchy;
        private Workflow workflow;
        private String plantUML;

        private void exportArtifact(String path, Object artifact) {
            try(FileOutputStream fos = new FileOutputStream(path)) {
                String json = artifact instanceof String ? (String) artifact : new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(artifact);
                fos.write(json.getBytes(StandardCharsets.UTF_8));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void exportArtifacts(String directory) {
            Optional.ofNullable(callHierarchy)
                    .ifPresent(artifact -> exportArtifact(String.join(File.separator, directory, "callHierarchy.json"), artifact));
            Optional.ofNullable(workflow)
                    .ifPresent(artifact -> exportArtifact(String.join(File.separator, directory, "workflow.json"), artifact));
            Optional.ofNullable(plantUML)
                    .ifPresent(artifact -> exportArtifact(String.join(File.separator, directory, "workflow.puml"), artifact));
        }
    }

    @Getter
    @Builder
    public static class Store {
        private Map<String, CompilationUnit> compilationUnits;
        private Dictionary dictionary;

        public Store replace(Store store) {
            this.compilationUnits = store.compilationUnits;
            this.dictionary = store.dictionary;
            return this;
        }

        public Store replace(Dictionary dictionary) {
            this.dictionary = dictionary;
            return this;
        }

        public Store replace(Map<String, CompilationUnit> units) {
            this.compilationUnits = units;
            return dictionary();
        }

        public Store update(Collection<CompilationUnit> units) {
            units.forEach((unit) -> compilationUnits.put(STATIC_STATIC_ANALYZER.packageAndClass(unit), unit));
            return dictionary();
        }

        public Store update(Map<String, CompilationUnit> units) {
            compilationUnits.putAll(units);
            return dictionary();
        }

        private Store dictionary() {
            dictionary = STATIC_STATIC_ANALYZER.makeCallDictionary(compilationUnits);
            return this;
        }

        public void clear() {
            this.compilationUnits = new LinkedHashMap<>();
            this.dictionary = Dictionary.init();
        }
    }
}
