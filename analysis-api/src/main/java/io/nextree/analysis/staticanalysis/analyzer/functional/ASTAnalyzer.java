package io.nextree.analysis.staticanalysis.analyzer.functional;

import com.github.javaparser.ParserConfiguration;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.Name;
import com.github.javaparser.ast.expr.VariableDeclarationExpr;
import com.github.javaparser.ast.nodeTypes.NodeWithName;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.serialization.JavaParserJsonSerializer;
import com.github.javaparser.symbolsolver.utils.SymbolSolverCollectionStrategy;
import com.github.javaparser.utils.ProjectRoot;
import io.nextree.analysis.staticanalysis.analyzer.StaticAnalyzer;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonGeneratorFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface ASTAnalyzer {
    String PACKAGE_CLASS_DELIMITER = ".";

    default String serialize(Node node, boolean prettyPrint) {
        Map<String, ?> config = new HashMap<>();
        if (prettyPrint) {
            config.put(JsonGenerator.PRETTY_PRINTING, null);
        }
        JsonGeneratorFactory generatorFactory = Json.createGeneratorFactory(config);
        JavaParserJsonSerializer serializer = new JavaParserJsonSerializer();
        StringWriter jsonWriter = new StringWriter();
        try (JsonGenerator generator = generatorFactory.createGenerator(jsonWriter)) {
            serializer.serialize(node, generator);
        }
        return jsonWriter.toString();
    }

    default CompilationUnit parse(Path path, ParserConfiguration configuration) throws IOException {
        StaticJavaParser.setConfiguration(configuration);
        return StaticJavaParser.parse(path);
    }

    default Map<String, CompilationUnit> parseProject(Path path, ParserConfiguration configuration) throws IOException {
        SymbolSolverCollectionStrategy symbolSolverCollectionStrategy = new SymbolSolverCollectionStrategy(configuration);
        ProjectRoot projectRoot = symbolSolverCollectionStrategy.collect(path);
        // TODO
        return projectRoot.getSourceRoots().stream().flatMap(sourceRoot -> {
            try {
                sourceRoot.tryToParse();
                return sourceRoot.getCompilationUnits().stream();
            } catch (IOException e) {
                e.fillInStackTrace();
                return Stream.of();
            }
        }).collect(Collectors.toMap(
                (unit) -> packageAndClass(unit),
                Function.identity()
        ));
    }

    default ClassOrInterfaceDeclaration type(CompilationUnit cu) {
        return (ClassOrInterfaceDeclaration) cu.getType(0);
    }

    default Optional<String> packageName(CompilationUnit cu) {
        return cu.getChildNodes()
                .stream().filter(node -> node.getClass().equals(PackageDeclaration.class))
                .findFirst().map(n -> ((PackageDeclaration) n).getNameAsString());
    }

    default Optional<String> className(CompilationUnit cu) {
        return cu.getChildNodes()
                .stream().filter(node -> node.getClass().equals(ClassOrInterfaceDeclaration.class))
                .findFirst().map(n -> ((ClassOrInterfaceDeclaration) n).getNameAsString());
    }

    default String canonicalName(CompilationUnit cu) {
        return cu.getType(0).resolve().getQualifiedName();
    }

    default List<String> classAnnotations(CompilationUnit cu) {
        List<AnnotationExpr> annotationList = cu.getType(0).getAnnotations();
        return annotationList.stream().map(NodeWithName::getNameAsString).collect(Collectors.toList());
    }

    default Map<String, String> imports(CompilationUnit cu) {
        return cu.getImports().stream().map(importDeclaration -> importDeclaration.getName()).collect(Collectors.toMap(Name::getIdentifier, Name::asString));
    }

    default List<FieldDeclaration> fields(ClassOrInterfaceDeclaration clazz, Predicate<FieldDeclaration> predicate) {
        return clazz.getFields().stream().filter(predicate).collect(Collectors.toList());
    }

    default List<FieldDeclaration> fields(ClassOrInterfaceDeclaration clazz) {
        return fields(clazz, f -> true);
    }

    default List<MethodDeclaration> methods(ClassOrInterfaceDeclaration clazz, Predicate<MethodDeclaration> predicate) {
        return clazz.getMethods().stream().filter(predicate).collect(Collectors.toList());
    }

    default List<MethodDeclaration> methods(ClassOrInterfaceDeclaration clazz) {
        return methods(clazz, m -> true);
    }

    default List<VariableDeclarator> variables(ClassOrInterfaceDeclaration clazz) {
        return fields(clazz).stream().map(field -> field.getVariable(0)).collect(Collectors.toList());
    }

    default List<VariableDeclarator> variables(MethodDeclaration method) {
        Optional<BlockStmt> body = method.getBody();
        if(body.isEmpty()) return null;

        List<VariableDeclarator> variableList = new  ArrayList<>();
        List<Node> nodes = body.get().getChildNodes();

        for (Node node : nodes) {
            List<VariableDeclarationExpr> found = variables(node);
            if(found.size() == 0) continue;
            found.forEach(expr -> {
                variableList.add(expr.getVariable(0));
            });
        }

        return variableList;
    }

    default List<VariableDeclarationExpr> variables(Node node) {
        List<VariableDeclarationExpr> exprs = new ArrayList<>();

        if(node.getChildNodes().size() != 0) {
            for (Node childNode : node.getChildNodes()) {
                if(childNode instanceof VariableDeclarationExpr)
                    exprs.add(((VariableDeclarationExpr) childNode).asVariableDeclarationExpr());
                else {
                    List<VariableDeclarationExpr> found = variables(childNode);
                    if(found != null) exprs.addAll(found);
                }
            }
        }

        return exprs;
    }

    default CompilationUnit unit(Node node) {
        Node rootNode = node;
        while (rootNode.getParentNode().isPresent()) {
            rootNode = rootNode.getParentNode().get();
        }
        return (CompilationUnit) rootNode;
    }

    // TODO: import packageA.package2.* 이런 형식으로 import했다면 현재는 찾을 수 없음
    default String packageAndClass(FieldDeclaration node, VariableDeclarator var) {
        String className = var.getTypeAsString();
        CompilationUnit unit = unit(node);
        List<ImportDeclaration> imports = unit.findAll(ImportDeclaration.class);
        Optional<ImportDeclaration> importDeclaration = imports.stream().filter(id -> id.getNameAsString().contains(className)).findFirst();
        if (importDeclaration.isEmpty()) {
            return packageName(unit) + PACKAGE_CLASS_DELIMITER + className;
        } else return importDeclaration.get().getNameAsString();
    }

    default String packageAndClass(CompilationUnit unit) {
        return packageName(unit) + PACKAGE_CLASS_DELIMITER + className(unit);
    }
}
