package io.nextree.analysis.plantuml.sequence.converter.vo;

import io.nextree.analysis.plantuml.sequence.model.arrow.Arrow;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ArrowLeaf extends ArrowNode {
    private Arrow arrow;

    public static ArrowLeaf of(Arrow arrow) {
        ArrowLeaf leaf = new ArrowLeaf();
        leaf.key = arrow.getKey();
        leaf.arrow = arrow;
        return leaf;
    }
}
