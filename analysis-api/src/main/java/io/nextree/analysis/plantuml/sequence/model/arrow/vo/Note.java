package io.nextree.analysis.plantuml.sequence.model.arrow.vo;

import io.nextree.analysis.plantuml.sequence.model.common.Alignment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Note {
    private Alignment align;
    private String content;

    public static Note sample() {
        return new Note(Alignment.right, "sample_note");
    }
}
