package io.nextree.analysis.plantuml.sequence.converter.vo;

import io.nextree.analysis.plantuml.sequence.model.arrow.Arrow;
import io.nextree.analysis.plantuml.sequence.model.participant.Participant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
public class SequenceData {
    private Map<String, Participant> participants;
    private ArrowNode arrowNode;
}
