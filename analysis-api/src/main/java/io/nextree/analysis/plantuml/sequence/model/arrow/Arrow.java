package io.nextree.analysis.plantuml.sequence.model.arrow;

import io.nextree.analysis.plantuml.sequence.model.common.SequenceObject;
import io.nextree.analysis.plantuml.sequence.model.arrow.vo.Activation;
import io.nextree.analysis.plantuml.sequence.model.arrow.vo.ArrowType;
import io.nextree.analysis.plantuml.sequence.model.arrow.vo.FromTo;
import io.nextree.analysis.plantuml.sequence.model.arrow.vo.Note;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Arrow extends SequenceObject {
    private ArrowType arrowType;
    private List<Note> notes;
    private Activation activation;
    private FromTo fromTo;

    @Override
    public String nameWithCover() {
        return String.format("\"\"%s\"\"", this.getName());
    }

    public Arrow(String id, ArrowType type, String name, List<Note> notes, String from, String to, Activation activation) {

        super(id, name);
        this.arrowType = type;
        this.notes = notes;
        this.activation = activation;
        this.fromTo = FromTo.newInstance(from, to);
    }
    public static Arrow sample(String leftId, String rightId) {
        List<Note> sampleNotes = List.of(Note.sample());
        return new Arrow("sample_arrow_id", ArrowType.RIGHT, "sample_arrow_desc" , sampleNotes, leftId, rightId, null);
    }
    public static Arrow sample(int id, String leftId, String rightId) {
        List<Note> sampleNotes = List.of(Note.sample(), Note.sample());
        String sampleId = Integer.toString(id);
        return new Arrow(sampleId, ArrowType.RIGHT, "sample_arrow_desc_" + sampleId, sampleNotes, leftId, rightId, null);
    }
}
