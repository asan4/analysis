package io.nextree.analysis.plantuml.sequence.model.arrow.vo;

public enum Activation {
    ACTIVATE("++"),
    DEACTIVATE("--"),
    RETURN("--"),
    DESTROY("!!");

    private final String value;

    Activation(String value) {
        this.value = value;
    }

    public String get() {
        return value;
    }
}
