package io.nextree.analysis.plantuml.sequence.model.participant.vo;

public enum ParticipantType {
    participant,
    actor,
    boundary,
    control,
    entity,
    database,
    collections,
    queue
}
