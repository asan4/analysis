package io.nextree.analysis.plantuml.sequence.model.participant;

import io.nextree.analysis.plantuml.sequence.model.common.SequenceObject;
import io.nextree.analysis.plantuml.sequence.model.participant.vo.Location;
import io.nextree.analysis.plantuml.sequence.model.participant.vo.ParticipantType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Participant extends SequenceObject {
    private ParticipantType type;
    private Location location;
    private boolean visible;
    public Participant(String id, String name, ParticipantType type) {
        super(id, name);
        this.type = type;
    }

    public String nameWithCover() {
        return String.format("\"%s\"", this.getName());
    }


    public void replaceLocation(Participant participant, long depth, long sequence) {
        Location location = participant.getLocation();
        if (location == null) {
            participant.setLocation(Location.of(depth, sequence));
        } else {
            Location compare = Location.of(depth, sequence);
            participant.location = location.compareTo(compare) < 0 ? location : compare;
        }
    }

    public static Participant sample() {
        return new Participant("sample_participant_id", "sample_participant", ParticipantType.participant);
    }

    public static Participant sample(int id) {
        String sampleId = Integer.toString(id);
        return new Participant(sampleId, "sample_participant_" + sampleId, ParticipantType.participant);
    }
}
