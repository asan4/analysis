package io.nextree.analysis.plantuml.sequence.model.arrow.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class FromTo {
    private String from;
    private String to;

    public static FromTo newInstance(String from, String to) {
        return from != null || to != null ? new FromTo(from, to) : null;
    }
}
