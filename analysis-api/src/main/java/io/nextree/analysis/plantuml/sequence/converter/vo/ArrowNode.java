package io.nextree.analysis.plantuml.sequence.converter.vo;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class ArrowNode {
    protected String key;

    public static ArrowNode of(String key) {
        ArrowNode node = new ArrowNode();
        node.setKey(key);
        return node;
    }
}
