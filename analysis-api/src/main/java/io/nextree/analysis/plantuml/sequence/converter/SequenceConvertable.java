package io.nextree.analysis.plantuml.sequence.converter;

import io.nextree.analysis.plantuml.sequence.builder.SequenceBuilder;
import io.nextree.analysis.plantuml.sequence.converter.vo.SequenceData;

public interface SequenceConvertable<T> {

    default SequenceBuilder from(T from) {
        return new SequenceBuilder().sequenceData(parse(from));
    }
    SequenceData parse(T from);
}
