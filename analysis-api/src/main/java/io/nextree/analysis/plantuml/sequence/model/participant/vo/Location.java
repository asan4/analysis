package io.nextree.analysis.plantuml.sequence.model.participant.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Location implements Comparable<Location> {
    private long depth;
    private long sequence;

    public static Location of(long depth, long sequence) {
        return new Location(depth, sequence);
    }

    @Override
    public int compareTo(Location o) {
        if (depth < o.depth) return -1;
        else if (depth > o.depth) return 1;
        else return Long.compare(sequence, o.sequence);
    }
}
