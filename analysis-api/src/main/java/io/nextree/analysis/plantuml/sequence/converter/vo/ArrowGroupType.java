package io.nextree.analysis.plantuml.sequence.converter.vo;

import io.nextree.analysis.flow.WorkflowType;

public enum ArrowGroupType {
    Root,
    Else,
    Condition,
    Then;

    public static ArrowGroupType fromWorkflowType(WorkflowType type) {
        switch (type) {
            case Else:
                return Else;
            case Then:
                return Then;
            case Condition:
            case ConditionGroup:
                return Condition;
            case Throw:
            case Switch:
            case Return:
            case Flow:
            default:
                throw new IllegalArgumentException();
        }
    }
}
