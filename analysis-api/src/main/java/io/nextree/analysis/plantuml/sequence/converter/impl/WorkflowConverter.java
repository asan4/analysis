package io.nextree.analysis.plantuml.sequence.converter.impl;

import io.nextree.analysis.callhierarchy.ref.CallMethodKey;
import io.nextree.analysis.plantuml.sequence.converter.SequenceConvertable;
import io.nextree.analysis.plantuml.sequence.converter.vo.*;
import io.nextree.analysis.plantuml.sequence.model.arrow.Arrow;
import io.nextree.analysis.plantuml.sequence.model.arrow.vo.ArrowType;
import io.nextree.analysis.plantuml.sequence.model.arrow.vo.Note;
import io.nextree.analysis.plantuml.sequence.model.arrow.vo.Activation;
import io.nextree.analysis.plantuml.sequence.model.participant.Participant;
import io.nextree.analysis.plantuml.sequence.model.participant.vo.Location;
import io.nextree.analysis.plantuml.sequence.model.participant.vo.ParticipantType;
import io.nextree.analysis.workflow.Workflow;
import io.nextree.analysis.flow.WorkflowType;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

import static io.nextree.analysis.plantuml.sequence.converter.factory.WorkflowFunctionFactory.*;

public class WorkflowConverter implements SequenceConvertable<Workflow> {
    private final Function<Workflow, List<Note>> noteSelector;
    private final Function<Workflow, ParticipantType> participantTypeSelector;
    private final Predicate<Workflow> participantFilter;
    private Map<String, Participant> participantsMap = new LinkedHashMap<>();
    public static final String HIERARCHY_DELIMITER = ":";
    public static final String HIERARCHY_DELIMITER_RESOLVER = "@";
    public static final String ARGUMENT_DELIMITER_RESOLVER = ".";

    public WorkflowConverter(
            Function<Workflow, List<Note>> noteSelector,
            Function<Workflow, ParticipantType> participantTypeSelector,
            Predicate<Workflow> participantFilter
    ) {
        this.noteSelector = noteSelector;
        this.participantTypeSelector = participantTypeSelector;
        this.participantFilter = participantFilter;
    }

    public WorkflowConverter() {
        this.noteSelector = defaultNoteSelector;
        this.participantTypeSelector = defaultParticipantTypeSelector;
        this.participantFilter = defaultParticipantFilter;
    }

    @Override
    public SequenceData parse(Workflow workflow) {
        participantsMap = new LinkedHashMap<>();
        ArrowNode arrowTree = ArrowGroup.of(ArrowGroupType.Root, "", resolveKeySyntax(workflow.getSourceRef()));
        workflow.getChildren().forEach(child -> _parseWorkflow(arrowTree, child, 0, 0));
        return new SequenceData(new LinkedHashMap<>(participantsMap), arrowTree);
    }

    private void _parseWorkflow(ArrowNode arrowNode, Workflow workflow, long depth, long sequence) {
        addParticipant(workflow, depth, sequence);
        if (workflow.getType() == WorkflowType.ConditionGroup) {
            _parseConditionFlow(arrowNode, workflow, depth);
        } else if (arrowNode instanceof ArrowGroup && workflow.getType() != WorkflowType.Condition) {
            ((ArrowGroup) arrowNode).getChildren().add(process(workflow.getParent(), workflow));
            int childSeq = 0;
            for (Workflow child : workflow.getChildren()) {
                _parseWorkflow(arrowNode, child, depth + 1, childSeq);
            }
            ((ArrowGroup) arrowNode).getChildren().add(terminate(workflow.getParent(), workflow));
        }
    }

    private void _parseConditionFlow(ArrowNode group, Workflow flow, long depth) {
        Workflow from = findCaller(flow);
        int sequence = 0;
        ArrowGroup newGroup = ArrowGroup.of(ArrowGroupType.fromWorkflowType(flow.getType()), flow.getDescription(), from.getSourceRef());
        for (Workflow child : flow.getChildren()) {
            switch (flow.getType()) {
                case ConditionGroup:
                    if (!((ArrowGroup) group).getChildren().contains(newGroup)) {
                        ((ArrowGroup) group).getChildren().add(newGroup);
                    }
                    _parseConditionFlow(newGroup, child, depth + 1);
                    break;
                case Else:
                    if (!((ArrowGroup) group).getChildren().contains(newGroup)) {
                        ((ArrowGroup) group).getChildren().add(newGroup);
                    }
                    _parseWorkflow(newGroup, child, depth + 1, sequence);
                    break;
                case Then:
                    _parseWorkflow(group, child, depth, sequence);
                    break;
                case Condition:
                default:
                    break;
            }
            sequence++;
        }
    }

    private Workflow findCaller(Workflow callee) {
        Workflow caller = callee;
        while (!caller.getType().equals(WorkflowType.Flow)) caller = caller.getParent();
        return caller;
    }

    private void addParticipant(Workflow workflow, long depth, long sequence) {
        if (!workflow.getType().equals(WorkflowType.Flow)) return;
        String className = getClassName(workflow);
        Participant participant;
        if (participantsMap.get(className) == null) {
            participant = new Participant(className, className, participantTypeSelector.apply(workflow));
            participant.setLocation(Location.of(depth, sequence));
            participantsMap.put(participant.getKey(), participant);
        } else participant = participantsMap.get(className);
        participant.replaceLocation(participant, depth, sequence);
        participant.setVisible(!participantFilter.test(workflow));
    }

    private ArrowLeaf process(Workflow parent, Workflow child) {
        return ArrowLeaf.of(
                new Arrow(
                        resolveKeySyntax(parent.getSourceRef()) + "->" + resolveKeySyntax(child.getSourceRef()),
                        ArrowType.RIGHT,
                        getMethodName(child) + String.format("(%s)", String.join(", ", getParameters(child))),
                        noteSelector.apply(child),
                        participantFilter.test(parent) ? null : getClassName(parent),
                        participantFilter.test(child) ? null : getClassName(child),
                        Activation.ACTIVATE
                )
        );
    }

    private ArrowLeaf terminate(Workflow parent, Workflow child) {
        return ArrowLeaf.of(
                new Arrow(
                        resolveKeySyntax(parent.getSourceRef()) + "<-" + resolveKeySyntax(child.getSourceRef()),
                        ArrowType.DOT_LEFT,
                        "",
                        null,
                        participantFilter.test(parent) ? null : getClassName(parent),
                        participantFilter.test(child) ? null : getClassName(child),
                        child.getChildren() == null || child.getChildren().isEmpty() ? Activation.RETURN : Activation.DEACTIVATE)
        );
    }

    private String getClassName(Workflow workflow) {
        try {
            return CallMethodKey.fromKey(findCaller(workflow).getSourceRef()).getClassName();
        } catch (IllegalArgumentException e) {
            return workflow.getSourceRef();
        }
    }

    private String getMethodName(Workflow workflow) {
        try {
            return CallMethodKey.fromKey((workflow).getSourceRef()).getMethodName();
        } catch (IllegalArgumentException e) {
            return workflow.getSourceRef();
        }
    }

    private List<String> getParameters(Workflow workflow) {
        try {
            return CallMethodKey.fromKey((workflow).getSourceRef()).getParamTypes();
        } catch (IllegalArgumentException e) {
            return List.of();
        }
    }

    private String resolveKeySyntax(String origin) {
        try {
            CallMethodKey hierarchyKey = CallMethodKey.fromKey(origin);
            return String.format(
                    "%s" + HIERARCHY_DELIMITER_RESOLVER + "%s" + HIERARCHY_DELIMITER_RESOLVER + "%s",
                    hierarchyKey.getClassName(), hierarchyKey.getMethodName(), String.join(ARGUMENT_DELIMITER_RESOLVER, hierarchyKey.getParamTypes()));
        } catch (IllegalArgumentException e) {
            return origin;
        }
    }
}
