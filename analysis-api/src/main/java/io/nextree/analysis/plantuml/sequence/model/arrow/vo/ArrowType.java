package io.nextree.analysis.plantuml.sequence.model.arrow.vo;

public enum ArrowType {
    RIGHT("->"),
    LEFT("<-"),
    DOT_RIGHT("-->"),
    DOT_LEFT("<--"),
    EXT_RIGHT("->]"),
    EXT_LEFT("[<--");


    private final String value;

    ArrowType(String value) { this.value = value; }

    public String get() { return value; }
}
