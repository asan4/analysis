package io.nextree.analysis.plantuml.sequence.model.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class SequenceObject {
    private String key;
    private String name;

    public abstract String nameWithCover();
}
