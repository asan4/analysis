package io.nextree.analysis.plantuml.sequence.model.common;

public enum Alignment {
    right,
    left,
    center
}
