package io.nextree.analysis.plantuml.sequence.builder;

import com.github.javaparser.quality.NotNull;
import io.nextree.analysis.plantuml.sequence.converter.vo.*;
import io.nextree.analysis.plantuml.sequence.model.arrow.Arrow;
import io.nextree.analysis.plantuml.sequence.model.participant.Participant;
import io.nextree.analysis.plantuml.sequence.model.arrow.vo.Activation;
import io.nextree.analysis.plantuml.sequence.model.arrow.vo.Note;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SequenceBuilder extends PlantUMLBuilder {
    private final StringBuilder builder = new StringBuilder();
    private static final String startUML = "@startuml";
    private static final String endUML = "@enduml";
    private static final String delimiter = ":";
    private static final String line = System.getProperty("line.separator");
    private static final String space = " ";
    private static final String as = "as";
    private static final String noteDeclaration = "note";
    private static final String noteEnd = "end note";
    private static final String altStart = "alt";
    private static final String elseStart = "else";
    private static final String groupEnd = "end";

    public SequenceBuilder() {
        line(startUML);
    }

    public String build() {
        return line(endUML).builder.toString()
                .replaceAll(space + space, space)
                .replaceAll(space + line, line)
                .replaceAll(line + line, line);
    }

    public SequenceBuilder sequenceData(SequenceData data) {
        List<Participant> participants = data.getParticipants()
                .values().stream().sorted(Comparator.comparing(Participant::getLocation))
                .filter(Participant::isVisible)
                .collect(Collectors.toList());
        participants.forEach(this::participant);
        return this.arrow(data.getArrowNode());
    }

    public SequenceBuilder participant(Participant participant) {
        return space(participant.getType().name())
                .space(participant.nameWithCover())
                .space(as)
                .line(participant.getKey());
    }

    public SequenceBuilder arrow(ArrowNode arrowNode) {
        if (arrowNode instanceof ArrowLeaf) arrow(((ArrowLeaf) arrowNode).getArrow());
        else if (arrowNode instanceof ArrowGroup) arrowGroup((ArrowGroup) arrowNode);
//        else arrowNode.getChildren().forEach(this::arrow);
        return this;
    }

    public SequenceBuilder arrow(Arrow arrow) {
        return arrow.getFromTo() != null ?
                add(arrow.getFromTo().getFrom() == null ? "[" : arrow.getFromTo().getFrom())
                        .add(arrow.getArrowType().get())
                        .space(arrow.getFromTo().getTo() == null ? "]" : arrow.getFromTo().getTo())
                        .activation(arrow.getActivation())
                        .arrowName(arrow.nameWithCover())
                        .notes(arrow.getNotes())
                : this;
    }

    private SequenceBuilder arrowName(String name) {
        return name == null || name.isBlank() ?
                space(null)
                :
                space(delimiter)
                        .line(name);
    }

    private SequenceBuilder arrowGroup(ArrowGroup group) {
        boolean isElse = group.getGroupType().equals(ArrowGroupType.Else);
        space(isElse ? elseStart : altStart).line(group.getDescription());
        group.getChildren().forEach(this::arrow);
        return isElse ? this : this.line(groupEnd);
    }


    private SequenceBuilder activation(Activation activation) {
        return activation != null ? space(activation.get()) : this;
    }

    private SequenceBuilder notes(List<Note> notes) {
        if (notes != null && !notes.isEmpty()) {
            for (Note note : notes) {
                space(noteDeclaration)
                        .line(note.getAlign().name())
                        .line(note.getContent())
                        .line(noteEnd);
            }
        }
        return line(null);
    }

    private SequenceBuilder add(@NotNull String str) {
        builder.append(str);
        return this;
    }

    private SequenceBuilder line(@Nullable String str) {
        if (str != null && !str.isBlank()) builder.append(str);
        builder.append(line);
        return this;
    }

    private SequenceBuilder space(@Nullable String str) {
        if (str != null && !str.isBlank()) builder.append(str);
        builder.append(space);
        return this;
    }

    public boolean save(File file) {
        try (RandomAccessFile raFile = new RandomAccessFile(file, "rw")) {
            if (!file.createNewFile()) raFile.setLength(0);
            raFile.write(build().getBytes());
            return true;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}