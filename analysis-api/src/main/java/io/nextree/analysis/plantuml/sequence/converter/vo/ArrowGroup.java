package io.nextree.analysis.plantuml.sequence.converter.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ArrowGroup extends ArrowNode {
    private ArrowGroupType groupType;
    private String description;
    private String groupRef;
    private List<ArrowNode> children = new ArrayList<>();
    public static ArrowGroup of(ArrowGroupType groupName, String description, String groupRef) {
        ArrowGroup arrow = new ArrowGroup();
        arrow.setGroupType(groupName);
        arrow.setDescription(description);
        arrow.setGroupRef(groupRef);
        return arrow;
    }
}
