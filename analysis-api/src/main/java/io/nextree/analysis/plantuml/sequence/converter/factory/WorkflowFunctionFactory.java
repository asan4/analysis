package io.nextree.analysis.plantuml.sequence.converter.factory;

import io.nextree.analysis.plantuml.sequence.converter.impl.WorkflowConverter;
import io.nextree.analysis.plantuml.sequence.model.common.Alignment;
import io.nextree.analysis.plantuml.sequence.model.arrow.vo.Note;
import io.nextree.analysis.plantuml.sequence.model.participant.vo.ParticipantType;
import io.nextree.analysis.workflow.Workflow;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

@AllArgsConstructor
public class WorkflowFunctionFactory {
    public static final Function<Workflow, ParticipantType> defaultParticipantTypeSelector;
    public static final Function<Workflow, List<Note>> defaultNoteSelector;
    public static final Predicate<Workflow> defaultParticipantFilter;
    static {
        defaultNoteSelector = (flow) -> flow.getKey().isEmpty() ? null : List.of(new Note(Alignment.right, flow.getKey()));
        defaultParticipantTypeSelector = (flow) -> flow.getSourceRef().split(WorkflowConverter.HIERARCHY_DELIMITER)[0].endsWith("Mapper") ? ParticipantType.database : ParticipantType.participant;
        defaultParticipantFilter = (flow) -> flow.getSourceRef().split(WorkflowConverter.HIERARCHY_DELIMITER)[0].endsWith("ExtService");
    }


}
