package io.nextree.anaylsis.sequencediagram;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.nextree.analysis.callhierarchy.key.CallMethodKey;
import io.nextree.analysis.callhierarchy.qualification.QualifiedMethodSignature;
import io.nextree.analysis.plantuml.sequence.builder.SequenceBuilder;
import io.nextree.analysis.plantuml.sequence.converter.impl.WorkflowConverter;
import io.nextree.analysis.staticanalysis.StaticAnalysis;
import io.nextree.analysis.workflow.Workflow;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;

public class ConditionTest {
    private static final String sourcePath = "C:/Users/_nextree/admis/sources/as-is/mc-oo/mc-oo-service";
    private static final String artifactPath = "src/test/resources/experiment";
    private static final File checkSignProcessInfo = new File("src/test/resources/experiment/checkSignProcessInfo.json");

    @Test
    public void test() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Workflow workflow = mapper.readValue(checkSignProcessInfo, Workflow.class);
        WorkflowConverter converter = new WorkflowConverter();
        String uml = new SequenceBuilder().sequenceData(converter.parse(workflow)).build();
        System.out.println(uml);
    }

    @Test
    public void sequenceTest() throws IOException {

        QualifiedMethodSignature entryPoint = QualifiedMethodSignature.fromKey("kr.amc.amis.mc.oo.od.service.CrtfcOrdrBizService.checkSignProcessInfo(kr.amc.amis.mc.oo.od.entity.OrdrElecCrtfcHdrDTO, kr.amc.amis.share.entity.mc.oo.od.PatOrdrSaveRsltDTO)");

        StaticAnalysis.Artifacts artifacts = StaticAnalysis.start(Path.of(sourcePath), new ArrayList<>())
                .parse()
                .callDictionary()
                .callHierarchy(entryPoint)
                .workflow(entryPoint)
                .plantUML()
                .artifacts();
        artifacts.exportArtifacts(artifactPath);
    }
}
