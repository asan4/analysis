package io.nextree.anaylsis.callhierarchy;

import com.github.javaparser.ast.CompilationUnit;
import io.nextree.analysis.callhierarchy.dictionary.Dictionary;
import io.nextree.analysis.callhierarchy.CallHierarchy;
import io.nextree.analysis.callhierarchy.MethodHierarchy;
import io.nextree.analysis.callhierarchy.ref.CallMethodKey;
import io.nextree.analysis.staticanalysis.analyzer.StaticAnalyzer;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;

@Slf4j
class ExtractTest {
    // 1. 프로젝트 경로 지정
    private static final Path EXPERIMENT_PATH = Paths.get(String.join(File.separator, "..", "..", "mc-oo", "mc-oo-service"));

    @Test
    void extractMapper() throws IOException {

        Map<String, CompilationUnit> units = StaticAnalyzer.analyze().parseProject(EXPERIMENT_PATH);
        Dictionary dictionary = StaticAnalyzer.analyze().makeCallDictionary(units);

        // 2. 클래스명:메소드명:파라미터1,파라미터2... 지정
        String methodKey = "ConsultOrdrManageBizService:registerConsultOrdr:ConsultOrdrDTO";

        Set<CallHierarchy> mapperSet = new HashSet<>();

        //3. 조건 지정
        Predicate<CallHierarchy> predicate = callHierarchy -> {
            return callHierarchy instanceof MethodHierarchy && ((MethodHierarchy) callHierarchy).getClassSignature().getFeature().isMapper();
        };
        Consumer<CallHierarchy> consumer = callHierarchy -> {
            if (predicate.test(callHierarchy)){
                mapperSet.add(callHierarchy);
            }
        };

//        CallHierarchy callHierarchy = CallHierarchy.makeCallHierarchy(dictionary, CallMethodKey.fromKey(methodKey), consumer);

        mapperSet.forEach(mapper -> System.out.println(mapper.getKey()));
    }
}
