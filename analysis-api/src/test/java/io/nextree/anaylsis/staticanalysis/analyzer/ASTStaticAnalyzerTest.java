package io.nextree.anaylsis.staticanalysis.analyzer;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

@Slf4j
class ASTStaticAnalyzerTest implements TestContext {
    final String EXPERIMENT_TARGET = "PatOrdrManageBizService.java";


    /**
     * EXPERIMENT_TARGET 로 명시된 클래스 파일을 파싱하여 json으로 출력
     */
    @Test
    void jsonTest() {
        CompilationUnit cu = parse(EXPERIMENT_TARGET);
        //
        String json = STATIC_ANALYZER.serialize(cu, true);
        //
        try(FileOutputStream fos = new FileOutputStream(String.join(File.separator, EXPERIMENT_PATH, EXPERIMENT_TARGET+".json"))) {
            fos.write(json.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * EXPERIMENT_TARGET 로 명시된 클래스 파일을 파싱하여 field 정보 출력
     */
    @Test
    void fieldTest() {
        CompilationUnit cu = parse(EXPERIMENT_TARGET);
        //
        ClassOrInterfaceDeclaration clazz = STATIC_ANALYZER.type(cu);
        //
        List<FieldDeclaration> fields = STATIC_ANALYZER.fields(clazz);
        fields.forEach(f -> {
            log.debug(f.toString());
        });
    }

    /**
     * EXPERIMENT_TARGET 로 명시된 클래스 파일을 파싱하여 조건에 부합하는 field 정보 출력
     */
    @Test
    void conditionalFieldTest() {
        CompilationUnit cu = parse(EXPERIMENT_TARGET);
        //
        ClassOrInterfaceDeclaration clazz = STATIC_ANALYZER.type(cu);
        //
        Predicate<FieldDeclaration> isExtService = f -> {
            Optional<VariableDeclarator> variableDeclarator = f.getChildNodes().stream().filter(node -> node instanceof VariableDeclarator).findFirst().map(n -> (VariableDeclarator) n);
            if (variableDeclarator.isPresent()) {
                return variableDeclarator.get().getName().getId().contains("Ext");
            } else {
                return false;
            }
        };

        List<FieldDeclaration> fields = STATIC_ANALYZER.fields(clazz, isExtService);
        fields.forEach(f -> {
            log.debug(f.toString());
        });
    }


    /**
     * EXPERIMENT_TARGET 로 명시된 클래스 파일을 파싱하여 method 정보 출력
     */
    @Test
    void methodTest() {
        CompilationUnit cu = parse(EXPERIMENT_TARGET);
        //
        ClassOrInterfaceDeclaration clazz = STATIC_ANALYZER.type(cu);
        //
        List<MethodDeclaration> methods = STATIC_ANALYZER.methods(clazz);
        methods.forEach(m -> {
            log.debug(m.getName().toString());
        });
    }
}
