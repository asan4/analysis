package io.nextree.anaylsis.staticanalysis.analyzer;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.List;

@Slf4j
class CommentStaticAnalyzerTest implements TestContext {
//    final String EXPERIMENT_TARGET = "CommentedSimpleService.java";
    final String EXPERIMENT_TARGET = "PatOrdrManageBizService.java";

    @Test
    void commentTest() {
        CompilationUnit cu = parse(EXPERIMENT_TARGET);
        cu.getComment().ifPresent(
                comment -> {
                    log.debug("+++++++++++++++ Module Comment Info:\n{}", STATIC_ANALYZER.comment(comment.asJavadocComment()));
                }
        );
        //
        ClassOrInterfaceDeclaration clazz = STATIC_ANALYZER.type(cu);
        clazz.getJavadocComment().ifPresent(
                comment -> {
                    log.debug("+++++++++++++++ Class Comment Info:\n{}", STATIC_ANALYZER.comment(comment));
                }
        );
        //
        List<MethodDeclaration> methods = STATIC_ANALYZER.methods(clazz);
        methods.forEach(method -> {
          method.getJavadocComment().ifPresent(
                  comment -> {
                      log.debug("+++++++++++++++ Method[{}] Comment Info\n{}", method.getName(), STATIC_ANALYZER.comment(comment));
                      method.getBody().ifPresent(blockStmt -> blockStmt.getStatements().forEach(statement -> {
                          statement.getComment().ifPresent(cmt -> {
                              if (cmt.isJavadocComment()) {
                                  log.debug("++++++++++++++++++++++++++++++ Statement[{}] Comment Info\n{}", statement, STATIC_ANALYZER.comment(cmt.asJavadocComment()));
                              }
                          });
                      }));
                  }
          );
        });
        //
    }
}
