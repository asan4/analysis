package io.nextree.anaylsis.staticanalysis.analyzer;

import com.github.javaparser.ast.CompilationUnit;
import io.nextree.analysis.staticanalysis.analyzer.StaticAnalyzer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public interface TestContext {
    String EXPERIMENT_PATH = String.join(File.separator, "src", "test", "resources", "experiment");
    StaticAnalyzer STATIC_ANALYZER = StaticAnalyzer.analyze();
    default File file(String filename) {
        return new File(String.join(File.separator, EXPERIMENT_PATH, filename));
    }

    default CompilationUnit parse(String filename) {
        try(FileInputStream fis = new FileInputStream(file(filename))) {
            return STATIC_ANALYZER.parse(fis);
        }catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
