package io.nextree.anaylsis.staticanalysis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javaparser.ast.CompilationUnit;
import io.nextree.analysis.callhierarchy.dictionary.Dictionary;
import io.nextree.analysis.callhierarchy.CallHierarchy;
import io.nextree.analysis.staticanalysis.analyzer.StaticAnalyzer;
import io.nextree.analysis.workflow.Workflow;
import io.nextree.analysis.callhierarchy.ref.CallMethodKey;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

@Slf4j
class StaticAnalysisTest {
    private final Path TARGET_PROJECT_PATH = Paths.get(String.join(File.separator, "..", "..", "mc-oo", "mc-oo-service"));
    private final CallMethodKey ENTRYPOINT = new CallMethodKey("CrtfcOrdrBizService", "checkSignProcessInfo", List.of("OrdrElecCrtfcHdrDTO", "PatOrdrSaveRsltDTO"));
    //
    private final Function<Path, Map<String, CompilationUnit>> compilationUnitsBuilder = path -> {
        try {
            return StaticAnalyzer.analyze().parseProject(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    };
    private final Function<Map<String, CompilationUnit>, Dictionary> dictionaryBuilder = cuList -> {
        return StaticAnalyzer.analyze().makeCallDictionary(cuList);
    };
    private final BiFunction<Dictionary, CallMethodKey, CallHierarchy> hierarchyBuilder = ((callDictionary, callMethodKey) -> CallHierarchy.makeCallHierarchy(callDictionary, callMethodKey.toKey()));

    @Test
    void workflowTest() throws JsonProcessingException {
        boolean print = false;

        log.debug("정적 분석 시작...\n");

        log.debug("정적 분석[1/4] 프로젝트 전체 소스코드 파싱 시작...");
        /**
         * @phase 프로젝트 전체 소스코드 파싱
         * @param 프로젝트 루트 경로
         * @return 프로젝트의 모든 파싱 결과
         */
        Path path = TARGET_PROJECT_PATH;
        Map<String, CompilationUnit> compilationUnits = compilationUnitsBuilder.apply(path);
        log.debug("정적 분석[1/4] {}개의 소스코드 파싱 완료...", compilationUnits.size());
        log.debug("정적 분석[1/4] 프로젝트 전체 소스코드 파싱 종료...\n");

        log.debug("정적 분석[2/4] Call Dictionary 생성 시작...");
        /**
         * @phase Call Dictionary 생성
         * @param 프로젝트의 모든 파싱 결과
         * @return Call Dictionary
         */
        Dictionary dictionary = dictionaryBuilder.apply(compilationUnits);
        log.debug("정적 분석[2/4] {}개의 클래스, {}개의 메소드 Dictionary 생성 완료...", dictionary.sizeOfClassMetaMap(), dictionary.sizeOfMethodMetaMap());
        log.debug("정적 분석[2/4] Call Dictionary 생성 종료...\n");

        log.debug("정적 분석[3/4] 특정 EntryPoint로부터 Call Hierarchy 생성 시작...");
        /**
         * @phase 특정 EntryPoint 로부터 Call Hierarchy 생성
         * @param Call Dictionary
         * @param Call Method Key (EntryPoint)
         * @return Call Hierarchy
         */
        CallHierarchy callHierarchy = hierarchyBuilder.apply(dictionary, ENTRYPOINT);
        writeAsJson(callHierarchy, print);
        log.debug("정적 분석[3/4] 특정 EntryPoint로부터 Call Hierarchy 생성 종료...\n");

        log.debug("정적 분석[4/4] Workflow 생성 시작...");
        /**
         * @phase Workflow 생성
         * @param Call Hierarchy
         * @return Workflow
         */
        Workflow workflow = Workflow.fromCallHierarchy(callHierarchy);
//        Workflow workflow = Workflow.fromCallHierarchy(callHierarchy, Workflow.Predication.isWorkflowDefinedAnyChildren);
//        Workflow workflow = Workflow.fromMethodHierarchy(callHierarchy, Workflow.Predication.isWorkflowDefined);

        writeAsJson(workflow, print);
        log.debug("정적 분석[4/4] Workflow 생성 종료...\n");


        /**
         * TODO
         * @phase plantuml 생성
         * @param Workflow
         * @return PlantUML
         */

        /**
         * TODO
         * @phase plantuml 생성
         * @param Workflow
         * @return BPMN
         */
        log.debug("정적 분석 종료...\n");
    }
    private String toPrettyJson(Object object) throws JsonProcessingException {
        return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(object);
    }

    private void writeAsJson(CallHierarchy callHierarchy, boolean print) {
        try(FileOutputStream fos = new FileOutputStream(String.join(File.separator, "src", "test", "resources", "experiment", "call_hierarchy.json"))) {
            String json = toPrettyJson(callHierarchy);
            if (print) {
                System.out.println(String.format("workflow : \n%s", json));
            }
            fos.write(json.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeAsJson(Workflow workflow, boolean print) {
        try(FileOutputStream fos = new FileOutputStream(String.join(File.separator, "src", "test", "resources", "experiment", "workflow.json"))) {
            String json = toPrettyJson(workflow);
            if (print) {
                System.out.println(String.format("workflow : \n%s", json));
            }
            fos.write(json.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
