package io.nextree.anaylsis.staticanalysis.analyzer;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.List;

@Slf4j
class CognitiveComplexityStaticAnalyzerTest implements TestContext {
    final String EXPERIMENT_TARGET = "PatOrdrManageBizService.java";

    /**
     * EXPERIMENT_TARGET 로 명시된 클래스 파일을 파싱하여 메소드 별 Cognitive Complexity 출력
     */
    @Test
    void cognitiveComplexityTest() {
        CompilationUnit cu = parse(EXPERIMENT_TARGET);
        //
        ClassOrInterfaceDeclaration clazz = STATIC_ANALYZER.type(cu);
        //
        List<MethodDeclaration> methods = STATIC_ANALYZER.methods(clazz);
        //
        log.debug("===== [메소드명] : 복잡도 =====");
        methods.forEach(method -> log.debug("[{}] : {}", method.getName(), STATIC_ANALYZER.cognitiveComplexity(method)));
    }
}
