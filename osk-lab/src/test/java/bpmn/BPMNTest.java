package bpmn;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.nextree.analysis.bpmn.converter.workflow.TestBuilder;
import io.nextree.analysis.workflow.Workflow;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.*;
import org.camunda.bpm.model.bpmn.instance.Process;
import org.camunda.bpm.model.bpmn.instance.bpmndi.BpmnDiagram;
import org.camunda.bpm.model.bpmn.instance.bpmndi.BpmnPlane;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

public class BPMNTest {
    private final File noBuilder = new File("src/main/resources/experiment/bpmn_without_builder.bpmn");
    private final File withBuilder = new File("src/main/resources/experiment/bpmn_with_builder.bpmn");
    @Test
    public void helloWorld() {
        BpmnModelInstance modelInstance = Bpmn
                .createProcess()
                .startEvent("asdf")
                .serviceTask("servicetask")
                .name("servvvvvvvv")
                .eventBasedGateway()
                .camundaAsyncBefore()
                .userTask("task")
                .camundaAsyncAfter()
                .done();
//        BpmnModelInstance modelInstance = Bpmn.createProcess()
//                .startEvent()
//                .serviceTask("servicetask")
//                .camundaAsyncBefore() // multi-instance body
//                .multiInstance()
//                .camundaAsyncBefore() // every instance
//                .parallel()
//                .multiInstanceDone()
//                .endEvent()
//                .done();


        Bpmn.validateModel(modelInstance);
        System.out.println(Bpmn.convertToString(modelInstance));
    }

    @Test
    public void workflowToBPMN() throws IOException {
        File file = new File("src/test/resources/experiment/workflow.json");
        ObjectMapper mapper = new ObjectMapper();
        Workflow workflow = mapper.readValue(file, Workflow.class);
        TestBuilder builder = new TestBuilder();
        String built = builder.workflow(workflow).buildAsString();
        System.out.println(built);
    }

    @Test
    public void sample() {
        // Camunda docs 예제 복붙인데 에러남
        BpmnModelInstance modelInstance = Bpmn.createEmptyModel();
        Definitions definitions = modelInstance.newInstance(Definitions.class);
        definitions.setTargetNamespace("http://camunda.org/schema/1.0/bpmn");
        modelInstance.setDefinitions(definitions);
        Process process = modelInstance.newInstance(Process.class);
        process.setExecutable(true);
        process.setId("process");
        definitions.addChildElement(process);

        // create elements
        StartEvent startEvent = createElement(process, "start", StartEvent.class);
        ParallelGateway fork = createElement(process, "fork", ParallelGateway.class);
        ServiceTask task1 = createElement(process, "task1", ServiceTask.class);
        task1.setName("Service Task");
        UserTask task2 = createElement(process, "task2", UserTask.class);
        task2.setName("User Task");
        ParallelGateway join = createElement(process, "join", ParallelGateway.class);
        EndEvent endEvent = createElement(process, "end", EndEvent.class);

        // create flows
        createSequenceFlow(process, startEvent, fork);
        createSequenceFlow(process, fork, task1);
        createSequenceFlow(process, fork, task2);
        createSequenceFlow(process, task1, join);
        createSequenceFlow(process, task2, join);
        createSequenceFlow(process, join, endEvent);
        BpmnDiagram diagram = modelInstance.newInstance(BpmnDiagram.class);
        BpmnPlane plane = modelInstance.newInstance(BpmnPlane.class);
        plane.setBpmnElement(process);
        diagram.setBpmnPlane(plane);
        definitions.addChildElement(diagram);
        // validate and write model to file
        Bpmn.validateModel(modelInstance);
        System.out.println(Bpmn.convertToString(modelInstance));
        Bpmn.writeModelToFile(noBuilder, modelInstance);
    }

    private <T extends BpmnModelElementInstance> T createElement(BpmnModelElementInstance parentElement, String id, Class<T> elementClass) {
        T element = parentElement.getModelInstance().newInstance(elementClass);
        element.setAttributeValue("id", id, true);
        parentElement.addChildElement(element);
        return element;
    }

    private SequenceFlow createSequenceFlow(Process process, FlowNode from, FlowNode to) {
        String identifier = from.getId() + "-" + to.getId();
        SequenceFlow sequenceFlow = createElement(process, identifier, SequenceFlow.class);
        process.addChildElement(sequenceFlow);
        sequenceFlow.setSource(from);
        from.getOutgoing().add(sequenceFlow);
        sequenceFlow.setTarget(to);
        to.getIncoming().add(sequenceFlow);
        return sequenceFlow;
    }
}
