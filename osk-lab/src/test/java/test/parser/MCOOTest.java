package test.parser;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import constant.TestFilePaths;
import constant.TestPredicates;
import io.nextree.analysis.javaparser.converter.Converter;
import io.nextree.analysis.javaparser.interpreter.Interpreter;
import io.nextree.analysis.javaparser.model.relation.common.LinkedNode;
import io.nextree.analysis.javaparser.model.relation.type.AsanNodeType;
import io.nextree.analysis.javaparser.serializer.UnlinkedNode;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class MCOOTest {
    private static final String EXPERIMENT_PATH = "src/test/resources/experiment/";

//
//    /**
//     * TestFilePath.ooPath를 local의 mc-oo 프로젝트 위치로 변경하여 실행
//     * @author 권오손
//     * */
//    @Test
//    public void convertOOToBasicJavaNode() {
//        List<CompilationUnit> units = ProjectParser.parseProject(Paths.get(TestFilePaths.ooPath));
//        System.out.println("parsed");
//        Map data = new Converter(units, new TestPredicates()).convert();
//        List<ClassOrInterfaceDeclaration> serviceClasses = units.stream().filter(unit -> {
//                    ClassOrInterfaceDeclaration classOrInterface = (ClassOrInterfaceDeclaration) unit.getChildNodes()
//                            .stream().filter(node -> node.getClass().equals(ClassOrInterfaceDeclaration.class))
//                            .findFirst().orElse(null);
//                    if (classOrInterface != null && classOrInterface.getName().getIdentifier().endsWith("Service")) {
//                        return true;
//                    }
//                    return false;
//                }
//        ).map(unit -> unit.findFirst(ClassOrInterfaceDeclaration.class).get()).toList();
//        System.out.println("converted");
//    }
//
//    /**
//     * TestFilePath.ooPath를 local의 mc-oo 프로젝트 위치로 변경하여 실행
//     * @author 권오손
//     * */
//
//    @Test
//    public void findOOCircularDependencies2() {
//        List<CompilationUnit> units = ProjectParser.parseProject(Paths.get(TestFilePaths.ooPath));
//        System.out.println("parsed");
//        Map<String, LinkedNode<AsanNodeType>> data = new Converter<AsanNodeType>(units, new TestPredicates()).convert();
//        System.out.println("converted");
//
//        Interpreter<AsanNodeType> interpreter = new Interpreter<AsanNodeType>(data);
//        Set<String> set = interpreter.findCircularDependencies(true);
//        Set<String> sorted = set.stream().sorted().collect(Collectors.toCollection(LinkedHashSet::new));
////        String joined = circularNames.stream().map(names -> String.join(" -> ", names)).collect(Collectors.joining("\n"));
////        saveAsFile("only_circle_names.txt", String.join("\n", onlyNames).getBytes());
////        saveAsFile("circular_names.txt", (String.join(" -> ", flatten)).getBytes());
////        String file = String.join("\n", new ArrayList<>(sorted));
////        saveAsFile("circular_names.txt", file.getBytes());
//        Set<String> onlyNames = new LinkedHashSet<>();
//        sorted.forEach(s -> {
//            String a = s.split(" -> ")[0];
//            onlyNames.add(a);
//        });
//        saveAsFile("only_circle_names.txt", String.join("\n", onlyNames).getBytes());
//        System.out.println("debug here");
//    }
//
//    @Test
//    public void findCirclesIn_AddOrdrManageBizService() {
//        List<CompilationUnit> units = ProjectParser.parseProject(Paths.get(TestFilePaths.ooPath));
//        System.out.println("parsed");
//        Map<String, LinkedNode<AsanNodeType>> data = new Converter<AsanNodeType>(units, new TestPredicates()).convert();
//        System.out.println("converted");
//
//        Interpreter<AsanNodeType> interpreter = new Interpreter<AsanNodeType>(data);
//        Set<String> set = interpreter.findCircularDependencies(true);
//        Set<String> sorted = set.stream().sorted().collect(Collectors.toCollection(LinkedHashSet::new));
//        saveAsFile("only_circle_names_wow.txt", String.join("\n", sorted).getBytes());
//        System.out.println("debug here");
//    }
//
//    @Test
//    public void serializeMCOOData() throws JsonProcessingException {
//        List<CompilationUnit> units = ProjectParser.parseProject(Paths.get(TestFilePaths.ooPath));
//        System.out.println("parsed");
//        Map<String, LinkedNode<AsanNodeType>> data = new Converter<AsanNodeType>(units, new TestPredicates()).convert();
//        System.out.println("converted");
//        List<UnlinkedNode<AsanNodeType>> nodes = new ArrayList<>();
//
//        for (LinkedNode<AsanNodeType> node : data.values()) {
//            List<String> children = node.getChildren().stream().map(LinkedNode::getId).collect(Collectors.toList());
//            List<String> parents = node.getParents().stream().map(LinkedNode::getId).collect(Collectors.toList());
//            nodes.add(new UnlinkedNode<>(node.getId(), node.getName(), node.getComment(), node.getNodeType(), children, parents));
//        }
//        ObjectMapper mapper = new ObjectMapper();
//        String json = mapper.writeValueAsString(nodes);
//        saveAsFile("mcoo-AdmissionDecisionController.json", json.getBytes());
//    }
//
//    private void saveAsFile(String fileName, byte...bytes) {
//        File file = new File(EXPERIMENT_PATH + fileName);
//        try {
//            if (file.createNewFile()) {
//                new RandomAccessFile(file, "rw").setLength(0);
//            }
//            FileOutputStream stream = new FileOutputStream(file);
//            stream.write(bytes);
//            stream.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//
//    @Test
//    public void asdf() {
//        System.out.println("PatOrdrInqryBizService, OrdrTitlManageBizService, PatOrdrSaveIntfBizService, AddOrdrManageBizService, PatInfoBasicBizService, PatOrdrManageProcService, MediOrdrCheckBizService, MatlOrdrManageBizService, OpOrdrManageBizService, PatOrdrDeliveryBizService, MatlOrdrDtlAdjBizService, PatOrdrMstInfoBizService, PatMdexCdssBizService, PatOrdrCheckCommonBizService, ExamTretOrdrManageBizService, PatOrdrCheckAfterSaveBizService, ConsultOrdrManageBizService, PatOrdrBasicInfoBizService, PatMdexCommonLibraryBizService, PatOrdrManageBizService, CPPatientBizService, PatDiagBizService".split(", ").length);
//    }

//    private String serializeCU(Node targetNode, boolean prettyPrint) {
//        Map<String, ?> config = new HashMap<>();
//        if (prettyPrint) {
//            config.put(JsonGenerator.PRETTY_PRINTING, null);
//        }
//        JsonGeneratorFactory generatorFactory = Json.createGeneratorFactory(config);
//        JavaParserJsonSerializer serializer = new JavaParserJsonSerializer();
//        StringWriter jsonWriter = new StringWriter();
//        try (JsonGenerator generator = generatorFactory.createGenerator(jsonWriter)) {
//            serializer.serialize(targetNode, generator);
//        }
//        return jsonWriter.toString();
//    }

}
