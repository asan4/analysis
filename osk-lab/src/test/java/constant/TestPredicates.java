package constant;

import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.MethodCallExpr;
import io.nextree.analysis.javaparser.converter.predicate.Predicates;

import java.util.List;
import java.util.function.Predicate;

public class TestPredicates extends Predicates {
    private static final List<String> beanAnnotations = List.of("Service", "Controller", "RestController", "Mapper", "Dao");
    public static Predicate<ClassOrInterfaceDeclaration> isExtService = (declaration) -> declaration.getNameAsString().endsWith("ExtService");
    public static Predicate<ClassOrInterfaceDeclaration> isIntService = (declaration) -> declaration.getNameAsString().endsWith("Service") && !declaration.getNameAsString().endsWith("ExtService");
    public static Predicate<ClassOrInterfaceDeclaration> isMapper = (declaration) -> declaration.getNameAsString().endsWith("Mapper");
    public static Predicate<ClassOrInterfaceDeclaration> isAPI = (declaration) -> declaration.getName().getIdentifier().endsWith("Resource");
    public static Predicate<ClassOrInterfaceDeclaration> isBean = (declaration) -> declaration.getAnnotations()
            .stream().anyMatch(annotation -> beanAnnotations.contains(annotation.getNameAsString()));
    public static Predicate<ClassOrInterfaceDeclaration> isLogicNode = (declaration) -> declaration.getNameAsString().endsWith("Logic");


    public static Predicate<MethodCallExpr> isMapperMethod = (methodCallExpr) -> methodCallExpr.getNameAsString().endsWith("Mapper");
    public static Predicate<MethodCallExpr> isServiceMethod = (methodCallExpr) -> methodCallExpr.getNameAsString().endsWith("Service");


    public static Predicate<VariableDeclarator> filterVariables = variable -> variable.getType().toString().endsWith("Service") || variable.getType().toString().endsWith("Mapper") || variable.getType().toString().endsWith("Logic") || variable.getType().toString().endsWith("Resource");

    public TestPredicates() {
        super(isBean, isExtService, isIntService, isMapper, isMapperMethod, isServiceMethod, filterVariables);
    }
}


