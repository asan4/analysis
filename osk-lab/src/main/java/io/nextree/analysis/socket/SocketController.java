package io.nextree.analysis.socket;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RestController;
@Slf4j
@RestController
@RequiredArgsConstructor
public class SocketController {
    private final SimpMessagingTemplate simpMessagingTemplate;

    // endpoint: publish/message
    @MessageMapping("message")
    @SendTo("/topic/file")
    public void receiveMessage() {
    }
}
