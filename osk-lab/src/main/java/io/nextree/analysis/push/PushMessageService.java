package io.nextree.analysis.push;


import lombok.extern.slf4j.Slf4j;
import nl.martijndwars.webpush.Notification;
import nl.martijndwars.webpush.PushService;
import nl.martijndwars.webpush.Subscription;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jose4j.lang.JoseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Security;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Service
@Slf4j
public class PushMessageService {
    // client에 해당 public key가 있어야 함
    @Value("${vapid.key.public}")
    private String publicKey;
    @Value("${vapid.key.private}")
    private String privateKey;
    private PushService pushService;
    private Map<String, Subscription> subscriptions;

    @PostConstruct
    private void init() throws GeneralSecurityException {
        Security.addProvider(new BouncyCastleProvider());
        subscriptions = new LinkedHashMap<>();
        pushService = new PushService(publicKey, privateKey);
    }

    public void subscribe(Subscription subscription) {
        if (!subscriptions.containsKey(subscription.endpoint)) {
            subscriptions.put(subscription.endpoint, subscription);
            log.debug("subscribed to: " + subscription.endpoint);
        }

    }

    public void unsubscribe(String endpoint) {
        log.debug("Unsubscribed from: " + endpoint);
        subscriptions.remove(endpoint);
    }

    public void sendNotification(String messageJson) {
        subscriptions.forEach((endpoint, subscription) -> {
            try {
                pushService.send(new Notification(subscription, messageJson));
            } catch (GeneralSecurityException | IOException | JoseException | ExecutionException |
                     InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    public void sendNotification(Subscription subscription, String messageJson) {
        try {
            pushService.send(new Notification(subscription, messageJson));
        } catch (GeneralSecurityException | IOException | JoseException | ExecutionException
                 | InterruptedException e) {
            e.printStackTrace();
        }
    }
}