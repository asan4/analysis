package io.nextree.analysis.push;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PushMessage {
    protected long time;
    protected MessageType type;
    protected String title;
    protected String body;

    public static PushMessage file(List<File> files) {
        return new PushMessage(
                System.currentTimeMillis(),
                MessageType.FILE,
                "file changed: ",
                files.stream().map(File::getName).collect(Collectors.joining(", "))
        );
    }

    public String toPrettyJson() {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "err";
        }
    }
}
