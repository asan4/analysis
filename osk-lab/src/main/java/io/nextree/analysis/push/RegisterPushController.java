package io.nextree.analysis.push;

import lombok.extern.slf4j.Slf4j;
import nl.martijndwars.webpush.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("push")
@Slf4j
public class RegisterPushController {
    @Autowired
    private PushMessageService pushMessageService;

    @PostMapping("sub")
    public String subscribe(@RequestBody Subscription subscription) {
        pushMessageService.subscribe(subscription);
        return "subscribed";
    }

    @PostMapping("unsub")
    public String unsubscribe(@RequestBody Subscription subscription) {
        pushMessageService.unsubscribe(subscription.endpoint);
        return "unsubscribed";
    }

    @PostMapping("test")
    public String test(@RequestBody Subscription subscription) {
        String json = "{\"msg\": \"asdfasdfasdfasdfasdfasdf\"}";
        pushMessageService.sendNotification(subscription, json);
        return "notified";
    }
}
