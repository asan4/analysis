package io.nextree.analysis.bpmn.converter.workflow;

import io.nextree.analysis.bpmn.converter.BPMNBuildable;
import io.nextree.analysis.bpmn.converter.BPMNBuilder;
import io.nextree.analysis.callhierarchy.ref.CallMethodKey;
import io.nextree.analysis.workflow.Workflow;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.instance.*;
import org.camunda.bpm.model.bpmn.instance.bpmndi.BpmnShape;
import org.camunda.bpm.model.bpmn.instance.dc.Bounds;

import java.io.File;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import static io.nextree.analysis.plantuml.sequence.converter.impl.WorkflowConverter.ARGUMENT_DELIMITER_RESOLVER;

public class WorkflowBPMNBuilder extends BPMNBuilder<Workflow> implements BPMNBuildable<Workflow> {
    private final Predicate<Workflow> externalFilter;
    private final Function<Workflow, String> taskNameGetter;
    public WorkflowBPMNBuilder(Workflow workflow) {
        super(workflow);
        BPMNOptionFactory factory = BPMNOptionFactory.defaultOption();
        externalFilter = factory.getExternalFilter();
        taskNameGetter = factory.getTaskNameGetter();
    }
    public WorkflowBPMNBuilder(
            Workflow workflow,
            BPMNOptionFactory options
    ) {
        super(workflow);
        externalFilter = options.getExternalFilter();
        taskNameGetter = options.getTaskNameGetter();
    }


    @Override
    public BPMNBuilder<Workflow> startEvent(String id, String name) {
        StartEvent start = createChild(StartEvent.class, id, name);
        Optional.of(name).ifPresent(start::setName);
        BpmnShape bpmnShape = createBpmnShape(start);
        Bounds bounds = bpmnShape.getBounds();
        bounds.setX(100);
        bounds.setY(100);
        return this;
    }

    @Override
    public BPMNBuilder<Workflow> tasks(Collection<Workflow> tasks) {
        return this;
    }

    @Override
    public BPMNBuilder<Workflow> task(Workflow task) {
        return null;
    }

    @Override
    public BPMNBuilder<Workflow> task(Workflow from, Workflow to) {
        String id = resolveKeySyntax(to.getSourceRef());
        String name = taskNameGetter.apply(to);
        ServiceTask target = createSibling(ServiceTask.class, id, name);

        BpmnShape targetBpmnShape = createBpmnShape(target);
        setCoordinates(targetBpmnShape);

        connectTarget(target, from.getSourceRef());
        resizeSubProcess(targetBpmnShape);
        return this;
    }

    @Override
    public BPMNBuilder<Workflow> endEvent(Workflow event) {
        return this;
    }

    @Override
    public BPMNBuilder<Workflow> gateway(Class<? extends Gateway> type) {
        return this;
    }

    @Override
    public BPMNBuilder<Workflow> sequenceFlow(Workflow flow) {
        return this;
    }

    @Override
    public BPMNBuilder<Workflow> sequenceFlows(Collection<Workflow> flow) {
        return this;
    }


    public void buildAsFile(File file) {
        Bpmn.writeModelToFile(file, model);
    }

    public String buildAsString() {
        return Bpmn.convertToString(model);
    }

    public String buildAsString(Workflow workflow) {
        data = workflow;
        return Bpmn.convertToString(model);
    }

    private String resolveKeySyntax(String origin) {
        CallMethodKey hierarchyKey = CallMethodKey.fromKey(origin);
        return String.format(
                "%s.%s.%s",
                hierarchyKey.getClassName(), hierarchyKey.getMethodName(), String.join(ARGUMENT_DELIMITER_RESOLVER, hierarchyKey.getParamTypes()));
    }
}
