package io.nextree.analysis.bpmn.converter.workflow;

import io.nextree.analysis.workflow.Workflow;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.function.Function;
import java.util.function.Predicate;

@Getter
@AllArgsConstructor
public class BPMNOptionFactory {
    private final Predicate<Workflow> externalFilter;
    private final Function<Workflow, String> taskNameGetter;

    public static BPMNOptionFactory defaultOption() {
        return new BPMNOptionFactory(
                (w) -> w.getSourceRef().contains("ExtService"),
                Workflow::getKey
        );
    }
}
