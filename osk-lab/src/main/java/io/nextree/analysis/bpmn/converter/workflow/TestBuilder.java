package io.nextree.analysis.bpmn.converter.workflow;

import io.nextree.analysis.callhierarchy.ref.CallMethodKey;
import io.nextree.analysis.workflow.Workflow;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.builder.AbstractBaseElementBuilder;
import org.camunda.bpm.model.bpmn.builder.AbstractTaskBuilder;
import org.camunda.bpm.model.bpmn.builder.ServiceTaskBuilder;

public class TestBuilder {
    private AbstractBaseElementBuilder<?, ?> builder;
    public TestBuilder workflow(Workflow workflow) {
        builder = Bpmn.createProcess().startEvent().serviceTask(syntax(workflow.getSourceRef())).name(workflow.getKey());
        for (Workflow child : workflow.getChildren()) {
            builder = _workflow(child);
        }
        ((AbstractTaskBuilder<?, ?>) builder).endEvent();
        return this;
    }

    private AbstractBaseElementBuilder<?, ?> _workflow(Workflow workflow) {
        builder = ((ServiceTaskBuilder) builder).serviceTask(syntax(workflow.getSourceRef()))
                .name(
                        String.format(
                                "%s\n%s",
                                className(workflow.getSourceRef()), workflow.getKey()
                                ));

        if (workflow.getChildren() == null) return builder;
        for (Workflow child : workflow.getChildren()) {
            builder = _workflow(child);
        }
        return builder;
    }

    private void pool() {
    }

    private String syntax(String origin) {
        CallMethodKey key = CallMethodKey.fromKey(origin);
        return String.format(
                "%s-%s-%s",
                key.getClassName(), key.getMethodName(), String.join("_", key.getParamTypes()
                ));
    }

    private String method(String origin) {
        return CallMethodKey.fromKey(origin).getMethodName();
    }

    private String className(String origin) {
        return CallMethodKey.fromKey(origin).getClassName();
    }

    public String buildAsString() {
        return Bpmn.convertToString(builder.done());
    }
}
