package io.nextree.analysis.bpmn.converter;

import io.nextree.analysis.workflow.Workflow;
import org.camunda.bpm.model.bpmn.instance.Gateway;

import java.util.Collection;

public interface BPMNBuildable<D> {

    BPMNBuilder<D> tasks(Collection<D> tasks);
    BPMNBuilder<D> task(D task);
    BPMNBuilder<D> startEvent(String id, String name);

    BPMNBuilder<Workflow> task(Workflow from, Workflow to);

    BPMNBuilder<D> endEvent(D event);

    BPMNBuilder<D> gateway(Class<? extends Gateway> type);
    BPMNBuilder<D> sequenceFlow(D flow);
    BPMNBuilder<D> sequenceFlows(Collection<D> flow);
}
