package io.nextree.analysis.bpmn.converter;

import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.*;
import org.camunda.bpm.model.bpmn.instance.Process;
import org.camunda.bpm.model.bpmn.instance.bpmndi.BpmnDiagram;
import org.camunda.bpm.model.bpmn.instance.bpmndi.BpmnEdge;
import org.camunda.bpm.model.bpmn.instance.bpmndi.BpmnPlane;
import org.camunda.bpm.model.bpmn.instance.bpmndi.BpmnShape;
import org.camunda.bpm.model.bpmn.instance.dc.Bounds;
import org.camunda.bpm.model.bpmn.instance.di.Diagram;
import org.camunda.bpm.model.bpmn.instance.di.Waypoint;
import org.camunda.bpm.model.xml.instance.ModelElementInstance;

import java.util.Collection;
import java.util.Iterator;

import static org.camunda.bpm.model.bpmn.builder.AbstractBaseElementBuilder.SPACE;
import static org.camunda.bpm.model.bpmn.impl.BpmnModelConstants.BPMN20_NS;
import static org.camunda.bpm.model.bpmn.impl.BpmnModelConstants.CAMUNDA_NS;


public abstract class BPMNBuilder<D> {
    protected BpmnModelInstance model;
    protected D data;
    protected Process process;
    protected Diagram diagram;
    protected BpmnPlane plane;
    protected BpmnModelElementInstance element;
    public BPMNBuilder(D d) {
        data = d;
        model = Bpmn.createEmptyModel();
    }
    protected <E extends BpmnModelElementInstance> E createElement(BpmnModelElementInstance parentElement, String id, String name, Class<E> elementClass) {
        E element = parentElement.getModelInstance().newInstance(elementClass);
        element.setAttributeValue("id", id, true);
        if (name != null && !name.isBlank()) element.setAttributeValue("name", name);
        parentElement.addChildElement(element);
        return element;
    }

    protected <T extends BaseElement> T createInstance(Class<T> typeClass, String identifier, String name) {
        T instance = createInstance(typeClass);
        if (identifier != null) {
            instance.setId(identifier);
            if (instance instanceof FlowElement) {
                ((FlowElement) instance).setName(name);
            }
        }
        return instance;
    }

    protected <T extends BpmnModelElementInstance> T createInstance(Class<T> typeClass) {
        return model.newInstance(typeClass);
    }

    protected <T extends BaseElement> T createChild(BpmnModelElementInstance parent, Class<T> typeClass, String identifier, String name) {
        T instance = createInstance(typeClass, identifier, name);
        parent.addChildElement(instance);
        return instance;
    }

    protected <T extends BaseElement> T createChild(Class<T> typeClass, String identifier, String name) {
        return createChild(element, typeClass, identifier, name);
    }


    public BpmnShape createBpmnShape(FlowNode node) {
        BpmnPlane bpmnPlane = findBpmnPlane();
        if (bpmnPlane != null) {
            BpmnShape bpmnShape = createInstance(BpmnShape.class);
            bpmnShape.setBpmnElement(node);
            Bounds nodeBounds = createInstance(Bounds.class);

            if (node instanceof SubProcess) {
                bpmnShape.setExpanded(true);
                nodeBounds.setWidth(350);
                nodeBounds.setHeight(200);
            } else if (node instanceof Activity) {
                nodeBounds.setWidth(100);
                nodeBounds.setHeight(80);
            } else if (node instanceof Event) {
                nodeBounds.setWidth(36);
                nodeBounds.setHeight(36);
            } else if (node instanceof Gateway) {
                nodeBounds.setWidth(50);
                nodeBounds.setHeight(50);
                if (node instanceof ExclusiveGateway) {
                    bpmnShape.setMarkerVisible(true);
                }
            }

            nodeBounds.setX(0);
            nodeBounds.setY(0);

            bpmnShape.addChildElement(nodeBounds);
            bpmnPlane.addChildElement(bpmnShape);

            return bpmnShape;
        }
        return null;
    }

    protected BpmnPlane findBpmnPlane() {
        Collection<BpmnPlane> planes = model.getModelElementsByType(BpmnPlane.class);
        return planes.iterator().next();
    }

    protected SequenceFlow sequenceFlow(FlowNode from, FlowNode to, String name) {
        String identifier = from.getId() + "->" + to.getId();
        SequenceFlow sequenceFlow = createElement(process, identifier, name, SequenceFlow.class);
        this.process.addChildElement(sequenceFlow);
        sequenceFlow.setSource(from);
        from.getOutgoing().add(sequenceFlow);
        sequenceFlow.setTarget(to);
        to.getIncoming().add(sequenceFlow);
        return sequenceFlow;
    }

    protected BPMNBuilder<D> process() {
        Definitions definitions = model.newInstance(Definitions.class);
        definitions.setTargetNamespace(BPMN20_NS);
        definitions.getDomElement().registerNamespace("nextree", "https://www.nextree.io/schema/bpmn");
        definitions.getDomElement().registerNamespace("camunda", CAMUNDA_NS);
        model.setDefinitions(definitions);
        Process process = model.newInstance(Process.class);
        this.process = process;
        definitions.addChildElement(process);

        BpmnDiagram bpmnDiagram = model.newInstance(BpmnDiagram.class);
        this.diagram = bpmnDiagram;

        BpmnPlane bpmnPlane = model.newInstance(BpmnPlane.class);
        bpmnPlane.setBpmnElement(process);
        this.plane = bpmnPlane;

        bpmnDiagram.addChildElement(bpmnPlane);
        definitions.addChildElement(bpmnDiagram);
        element = process;
        return this;
    }

    protected BpmnShape findBpmnShape(BaseElement node) {
        Collection<BpmnShape> allShapes = model.getModelElementsByType(BpmnShape.class);

        Iterator<BpmnShape> iterator = allShapes.iterator();
        while (iterator.hasNext()) {
            BpmnShape shape = iterator.next();
            if (shape.getBpmnElement().equals(node)) {
                return shape;
            }
        }
        return null;
    }

    protected <T extends BaseElement> T createSibling(Class<T> typeClass, String identifier, String name) {
        T instance = createInstance(typeClass, identifier, name);
        element.getParentElement().addChildElement(instance);
        return instance;
    }

    protected void connectTarget(FlowNode target, String fromId) {
        connectTargetWithSequenceFlow(target, fromId);
    }

    protected void connectTargetWithSequenceFlow(FlowNode to, String fromId) {
        FlowNode from = model.getModelElementById(fromId);
        SequenceFlow flow = model.newInstance(SequenceFlow.class);
        flow.setSource(from);
        flow.setTarget(to);
        from.getOutgoing().add(flow);
        to.getIncoming().add(flow);
        createEdge(flow);
    }

    public BpmnEdge createEdge(BaseElement baseElement) {
        BpmnPlane bpmnPlane = findBpmnPlane();
        if (bpmnPlane != null) {
            BpmnEdge edge = createInstance(BpmnEdge.class);
            edge.setBpmnElement(baseElement);
            setWaypoints(edge);
            bpmnPlane.addChildElement(edge);
            return edge;
        }
        return null;
    }

    protected void setWaypoints(BpmnEdge edge) {
        BaseElement bpmnElement = edge.getBpmnElement();

        FlowNode edgeSource;
        FlowNode edgeTarget;
        if (bpmnElement instanceof SequenceFlow) {

            SequenceFlow sequenceFlow = (SequenceFlow) bpmnElement;

            edgeSource = sequenceFlow.getSource();
            edgeTarget = sequenceFlow.getTarget();

        } else if (bpmnElement instanceof Association) {
            Association association = (Association) bpmnElement;

            edgeSource = (FlowNode) association.getSource();
            edgeTarget = (FlowNode) association.getTarget();
        } else {
            throw new RuntimeException("Bpmn element type not supported");
        }

        setWaypointsWithSourceAndTarget(edge, edgeSource, edgeTarget);
    }

    protected void setWaypointsWithSourceAndTarget(BpmnEdge edge, FlowNode edgeSource, FlowNode edgeTarget) {
        BpmnShape source = findBpmnShape(edgeSource);
        BpmnShape target = findBpmnShape(edgeTarget);

        if (source != null && target != null) {

            Bounds sourceBounds = source.getBounds();
            Bounds targetBounds = target.getBounds();

            double sourceX = sourceBounds.getX();
            double sourceY = sourceBounds.getY();
            double sourceWidth = sourceBounds.getWidth();
            double sourceHeight = sourceBounds.getHeight();

            double targetX = targetBounds.getX();
            double targetY = targetBounds.getY();
            double targetHeight = targetBounds.getHeight();

            Waypoint w1 = createInstance(Waypoint.class);

            if (edgeSource.getOutgoing().size() == 1) {
                w1.setX(sourceX + sourceWidth);
                w1.setY(sourceY + sourceHeight / 2);

                edge.addChildElement(w1);
            } else {
                w1.setX(sourceX + sourceWidth / 2);
                w1.setY(sourceY + sourceHeight);

                edge.addChildElement(w1);

                Waypoint w2 = createInstance(Waypoint.class);
                w2.setX(sourceX + sourceWidth / 2);
                w2.setY(targetY + targetHeight / 2);

                edge.addChildElement(w2);
            }

            Waypoint w3 = createInstance(Waypoint.class);
            w3.setX(targetX);
            w3.setY(targetY + targetHeight / 2);

            edge.addChildElement(w3);
        }
    }



    protected void resizeSubProcess(BpmnShape innerShape) {

        BaseElement innerElement = innerShape.getBpmnElement();
        Bounds innerShapeBounds = innerShape.getBounds();

        ModelElementInstance parent = innerElement.getParentElement();

        while (parent instanceof SubProcess) {

            BpmnShape subProcessShape = findBpmnShape((SubProcess) parent);

            if (subProcessShape != null) {

                Bounds subProcessBounds = subProcessShape.getBounds();
                double innerX = innerShapeBounds.getX();
                double innerWidth = innerShapeBounds.getWidth();
                double innerY = innerShapeBounds.getY();
                double innerHeight = innerShapeBounds.getHeight();

                double subProcessY = subProcessBounds.getY();
                double subProcessHeight = subProcessBounds.getHeight();
                double subProcessX = subProcessBounds.getX();
                double subProcessWidth = subProcessBounds.getWidth();

                double tmpWidth = innerX + innerWidth + SPACE;
                double tmpHeight = innerY + innerHeight + SPACE;

                if (innerY == subProcessY) {
                    subProcessBounds.setY(subProcessY - SPACE);
                    subProcessBounds.setHeight(subProcessHeight + SPACE);
                }

                if (tmpWidth >= subProcessX + subProcessWidth) {
                    double newWidth = tmpWidth - subProcessX;
                    subProcessBounds.setWidth(newWidth);
                }

                if (tmpHeight >= subProcessY + subProcessHeight) {
                    double newHeight = tmpHeight - subProcessY;
                    subProcessBounds.setHeight(newHeight);
                }

                innerElement = (SubProcess) parent;
                innerShapeBounds = subProcessBounds;
                parent = innerElement.getParentElement();
            }
            else {
                break;
            }
        }
    }


    protected void setCoordinates(BpmnShape shape) {
        BpmnShape source = findBpmnShape((BaseElement) element);
        Bounds shapeBounds = shape.getBounds();

        double x = 0;
        double y = 0;

        if (source != null) {
            Bounds sourceBounds = source.getBounds();

            double sourceX = sourceBounds.getX();
            double sourceWidth = sourceBounds.getWidth();
            x = sourceX + sourceWidth + SPACE;

            if (element instanceof FlowNode) {

                FlowNode flowNode = (FlowNode) element;
                Collection<SequenceFlow> outgoing = flowNode.getOutgoing();

                if (outgoing.size() == 0) {
                    double sourceY = sourceBounds.getY();
                    double sourceHeight = sourceBounds.getHeight();
                    double targetHeight = shapeBounds.getHeight();
                    y = sourceY + sourceHeight / 2 - targetHeight / 2;
                }
                else {
                    SequenceFlow[] sequenceFlows = outgoing.toArray(new SequenceFlow[outgoing.size()]);
                    SequenceFlow last = sequenceFlows[outgoing.size() - 1];

                    BpmnShape targetShape = findBpmnShape(last.getTarget());
                    if (targetShape != null) {
                        Bounds targetBounds = targetShape.getBounds();
                        double lastY = targetBounds.getY();
                        double lastHeight = targetBounds.getHeight();
                        y = lastY + lastHeight + SPACE;
                    }

                }
            }
        }

        shapeBounds.setX(x);
        shapeBounds.setY(y);
    }

//    protected void setCoordinates(BpmnShape shape) {
//        BpmnShape source = findBpmnShape(element);
//        Bounds shapeBounds = shape.getBounds();
//
//        double x = 0;
//        double y = 0;
//
//        if (source != null) {
//            Bounds sourceBounds = source.getBounds();
//
//            double sourceX = sourceBounds.getX();
//            double sourceWidth = sourceBounds.getWidth();
//            x = sourceX + sourceWidth + SPACE;
//
//            if (element instanceof FlowNode) {
//
//                FlowNode flowNode = (FlowNode) element;
//                Collection<SequenceFlow> outgoing = flowNode.getOutgoing();
//
//                if (outgoing.size() == 0) {
//                    double sourceY = sourceBounds.getY();
//                    double sourceHeight = sourceBounds.getHeight();
//                    double targetHeight = shapeBounds.getHeight();
//                    y = sourceY + sourceHeight / 2 - targetHeight / 2;
//                }
//                else {
//                    SequenceFlow[] sequenceFlows = outgoing.toArray(new SequenceFlow[outgoing.size()]);
//                    SequenceFlow last = sequenceFlows[outgoing.size() - 1];
//
//                    BpmnShape targetShape = findBpmnShape(last.getTarget());
//                    if (targetShape != null) {
//                        Bounds targetBounds = targetShape.getBounds();
//                        double lastY = targetBounds.getY();
//                        double lastHeight = targetBounds.getHeight();
//                        y = lastY + lastHeight + SPACE;
//                    }
//
//                }
//            }
//        }
//
//        shapeBounds.setX(x);
//        shapeBounds.setY(y);
//    }
}
