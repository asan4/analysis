package io.nextree.analysis.javaparser.converter;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.comments.Comment;
import io.nextree.analysis.javaparser.converter.predicate.Predicates;
import io.nextree.analysis.javaparser.model.relation.ExtServiceNode;
import io.nextree.analysis.javaparser.model.relation.IntServiceNode;
import io.nextree.analysis.javaparser.model.relation.MapperBeanNode;
import io.nextree.analysis.javaparser.model.relation.common.LinkedNode;
import io.nextree.analysis.javaparser.model.relation.common.BeanNode;
import lombok.AllArgsConstructor;

import java.util.*;

@AllArgsConstructor
public class Converter<T extends Enum> {
    public static final String METHOD_JAVA_DELIMITER = ".";
//    public static final String COMMENT_SEPARATOR = "";
    private final List<CompilationUnit> units;
    private final Predicates predicates;


    // TODO: enum이나 annotation은 현재 data에 넣지 않음(파싱은 하되 파싱 후 데이터에 포함되지 않음)
    public Map<String, LinkedNode<T>> convert() {
        Map<String, LinkedNode<T>> data = new HashMap<>();
        Map<String, ClassOrInterfaceDeclaration> declarationMap = new HashMap<>();

        for (CompilationUnit unit : units) {
            ClassOrInterfaceDeclaration classOrInterface = (ClassOrInterfaceDeclaration) unit.getChildNodes()
                    .stream().filter(node -> node.getClass().equals(ClassOrInterfaceDeclaration.class))
                    .findFirst().orElse(null);
            PackageDeclaration packageDeclaration = (PackageDeclaration) unit.getChildNodes()
                    .stream().filter(node -> node.getClass().equals(PackageDeclaration.class))
                    .findFirst().orElse(null);

            if (classOrInterface == null) {
                continue;
            }
            String packageName = packageDeclaration.getNameAsString();
            String className = classOrInterface.getName().getIdentifier();

            String packageAndClassName = joinPackageAndJava(packageName, className);

            LinkedNode<T> javaNode = this.filter(classOrInterface, packageDeclaration);
            if (javaNode == null) {
                continue;
            }
            data.put(packageAndClassName, javaNode);
            declarationMap.put(packageAndClassName, classOrInterface);
        }
        for (Map.Entry<String, ClassOrInterfaceDeclaration> entry : declarationMap.entrySet()) {
            this.connectFamily(data, entry);
        }
        return data;
    }

    private LinkedNode<T> filter(ClassOrInterfaceDeclaration classOrInterfaceDeclaration, PackageDeclaration packageDeclaration) {
        // TODO: 현재 분류 종류가 static함. 이를 유연하게 바꿀 방안 필요 -----------------------
        LinkedNode<T> node;
        if (!this.predicates.getIsBean().test(classOrInterfaceDeclaration)) {
            return null;
        }
        if (this.predicates.getIsIntService().test(classOrInterfaceDeclaration)) {
            node = new IntServiceNode();
        }
        else if (this.predicates.getIsExtService().test(classOrInterfaceDeclaration)) {
            node = new ExtServiceNode();
        } else if (this.predicates.getIsMapper().test(classOrInterfaceDeclaration)) {
            node = new MapperBeanNode();
        } else {
            node = new BeanNode();
        }
        node.setChildren(new ArrayList<>());
        node.setParents(new ArrayList<>());
        node.setName(classOrInterfaceDeclaration.getNameAsString());
        node.setId(joinPackageAndJava(packageDeclaration.getNameAsString(), classOrInterfaceDeclaration.getNameAsString()));
        String comment = classOrInterfaceDeclaration.getComment().map(Comment::getContent).orElse(null);
        node.setComment(comment);
        return node;
        // -------------------------------------------------------------------------------
    }

    private void connectFamily(Map<String, LinkedNode<T>> data, Map.Entry<String, ClassOrInterfaceDeclaration> entry) {

        String parentKey = entry.getKey();
        ClassOrInterfaceDeclaration parentClass = entry.getValue();

        List<FieldDeclaration> fieldDeclarationList = new ArrayList<>();
        for (Node node : parentClass.getChildNodes()) {
            if (node.getClass().equals(FieldDeclaration.class)) {
                fieldDeclarationList.add((FieldDeclaration) node);
            }
        }
        /*
        ---------------------------------------------------------------------------------------------
        TODO:
         private String a, b
         attribute가 이런 식으로 정의되었다면 버그가 생길 것으로 추정됨
         */
        for (FieldDeclaration node : fieldDeclarationList) {
            VariableDeclarator variable = node.getVariables().stream().findFirst().orElse(null);
            if (variable == null) {
                continue;
            }
        //-------------------------------------------------------------------------------------------

            String childClassName = variable.getType().toString();
            String childKey = findPackageNameAndClassNameWithVariable(node, variable);

            if (childClassName == null || !this.predicates.getVariableFilter().test(variable)) {
                continue;
            }
            if (childClassName.endsWith("ExtService") && !data.containsKey(childKey)) {
                ExtServiceNode extNode = new ExtServiceNode();
                extNode.setId(childKey);
                extNode.setName(childClassName);
                extNode.setChildren(new ArrayList<>());
                extNode.setParents(new ArrayList<>());
                data.put(childKey, extNode);
            }

            data.get(parentKey).getChildren().add(data.get(childKey));
            data.get(childKey).getParents().add(data.get(parentKey));
        }
    }

    // TODO: import packageA.package2.* 이런 형식으로 import했다면 현재는 찾을 수 없음
    private String findPackageNameAndClassNameWithVariable(FieldDeclaration node, VariableDeclarator var) {
        String className = var.getTypeAsString();
//        ClassOrInterfaceDeclaration cd = (ClassOrInterfaceDeclaration) node.getParentNode().orElseThrow();
//        CompilationUnit unit = (CompilationUnit) cd.getParentNode().orElseThrow();
        CompilationUnit unit = this.findCompilationUnitFromAncestor(node);
        List<ImportDeclaration> imports = unit.findAll(ImportDeclaration.class);
        ImportDeclaration id = imports.stream().filter(importDeclaration -> importDeclaration.getNameAsString().contains(className)).findFirst().orElse(null);
        if (id == null) {
            return joinPackageAndJava(unit.findFirst(PackageDeclaration.class).get().getNameAsString(), className);
        }
        return id.getNameAsString();
    }

    public CompilationUnit findCompilationUnitFromAncestor(Node node) {
        Node rootNode = node;
        while (rootNode.getParentNode().isPresent()) {
            rootNode = rootNode.getParentNode().get();
        }
        return (CompilationUnit) rootNode;
    }

    public static String joinPackageAndJava(String ...arguments) {
        return String.join(METHOD_JAVA_DELIMITER, arguments);
    }
}
