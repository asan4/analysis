package io.nextree.analysis.javaparser.model.relation.type;

public enum AsanNodeType {
    API,
    INT_SPRING_BEAN,
    EXT_SERVICE_BEAN,
    SERVICE_METHOD,
    MAPPER_BEAN,
    MAPPER_METHOD
}
