package io.nextree.analysis.javaparser.serializer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UnlinkedNode<T extends Enum> {
    private String id;
    private String name;
    private String comment;

    private T nodeType;
    private List<String> childrenIds;
    private List<String> parentsIds;
}
