package io.nextree.analysis.javaparser.converter.predicate;

import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.MethodCallExpr;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.function.Predicate;


@Getter
@AllArgsConstructor
public class Predicates {
    private final Predicate<ClassOrInterfaceDeclaration> isBean;
    private final Predicate<ClassOrInterfaceDeclaration> isExtService;
    private final Predicate<ClassOrInterfaceDeclaration> isIntService;
    private final Predicate<ClassOrInterfaceDeclaration> isMapper;
    private final Predicate<MethodCallExpr> isMapperMethod;
    private final Predicate<MethodCallExpr> isServiceMethod;
    private final Predicate<VariableDeclarator> variableFilter;

//    boolean isBean(ClassOrInterfaceDeclaration declaration);
//    boolean isIntService(ClassOrInterfaceDeclaration declaration);
//    boolean isExtService(ClassOrInterfaceDeclaration declaration);
//    boolean isMapper(ClassOrInterfaceDeclaration declaration);
//    boolean isMapperMethod(ClassOrInterfaceDeclaration declaration);
//    boolean isServiceMethod(ClassOrInterfaceDeclaration declaration);
}
