package io.nextree.analysis.javaparser.model.relation.common;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
// TODO: T extends Enum ?
public abstract class LinkedNode<T extends Enum> {
//public abstract class BasicJavaNode implements Cloneable {
    private String id;
    private String name;
    private T nodeType;
    private String comment;
    private List<LinkedNode<T>> children;
    private List<LinkedNode<T>> parents;
    @Override
    public LinkedNode<T> clone() {
        try {
            LinkedNode clone = (LinkedNode) super.clone();
            clone.setChildren(null);
            clone.setParents(null);
            clone.setId(this.id);
            clone.setName(this.name);
            clone.setComment(this.comment);

            // TODO: 부모-자식이 서로 circular 참조관계면 현재 복사할 수 없음
            /*
            List<BasicJavaNode> children = new ArrayList<>();
            for (BasicJavaNode node : this.getChildren()) {
                children.add(node.clone());
            }
            clone.setChildren(children);

            List<BasicJavaNode> parents = new ArrayList<>();
            for (BasicJavaNode node : this.getParents()) {
                parents.add(node.clone());
            }
            clone.setParents(parents);
            */

            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
