package io.nextree.analysis.javaparser.model.relation.common;


import io.nextree.analysis.javaparser.model.relation.type.AsanNodeType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BeanNode extends LinkedNode {
    private AsanNodeType nodeType;
}
