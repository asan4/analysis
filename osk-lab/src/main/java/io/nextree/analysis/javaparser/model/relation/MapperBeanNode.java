package io.nextree.analysis.javaparser.model.relation;

import io.nextree.analysis.javaparser.model.relation.common.LinkedNode;
import io.nextree.analysis.javaparser.model.relation.type.AsanNodeType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MapperBeanNode extends LinkedNode {
    private AsanNodeType nodeType = AsanNodeType.MAPPER_BEAN;
}
