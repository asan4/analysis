package io.nextree.analysis.javaparser.model.relation.type;

public enum ServiceType {
    EXTERNAL,
    INTERNAL
}
