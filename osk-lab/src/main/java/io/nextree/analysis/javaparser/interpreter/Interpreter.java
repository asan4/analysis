package io.nextree.analysis.javaparser.interpreter;

import io.nextree.analysis.javaparser.model.relation.common.LinkedNode;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.*;

@Getter
@AllArgsConstructor
public class Interpreter<T extends Enum> {
    private final Map<String, LinkedNode<T>> data;

    // TODO: error: java.lang.OutOfMemoryError: Java heap space
    //                  at io.nextree.javaparser.interpreter.Interpreter._findCircles(Interpreter.java:31)
    private void _findCircles(LinkedNode<T> record, Set<String> addTo, Set<String> checked, List<String> log, boolean listNames) {
        String currentId = record.getId();
        List<String> currentDILog = new ArrayList<>(log);
        if (currentDILog.contains(currentId)) {
//            int idx = currentDILog.lastIndexOf(currentId);
//            currentDILog.subList(0, idx).clear();
//            currentDILog.add(currentId);
//            List<String> list = listNames ? currentDILog.stream().map(n -> data.get(n).getName()).collect(Collectors.toList()) : currentDILog;
//            addTo.add(String.join(" -> ", list));
            checked.add(currentId);
            return;
        }
//        if (checked.contains(currentId)) {
//            return;
//        }
        currentDILog.add(currentId);
        if (!record.getChildren().isEmpty()) {
            for (LinkedNode<T> child : record.getChildren()) {
                _findCircles(child, addTo, checked, currentDILog, listNames);
            }
        }
    }
    public Set<String> findCircularDependencies(boolean listNames) {
        Set<String> circles = new LinkedHashSet<>();
        Set<String> checked = new HashSet<>();
        for (LinkedNode<T> node : data.values()) {
            _findCircles(node, circles, checked, new ArrayList<>(), listNames);
        }
        return checked;
    }

    public Set<String> findCircularDependencies(String key, boolean listnames) {
        Set<String> circles = new LinkedHashSet<>();
        Set<String> checked = new HashSet<>();
        LinkedNode node = data.get(key);
        _findCircles(node, circles, checked, new ArrayList<>(), listnames);
        return checked;
    }

    public Set<String> findCircularDependencies2(String key) {
        Set<String> circles = new LinkedHashSet<>();
        Set<String> checked = new HashSet<>();
        Map<String, List<String>> parentAndChildren = new HashMap<>();

        _findCircles(data.get(key), circles, new ArrayList<>(), parentAndChildren);
        return circles;
    }

    public Set<String> findCircularDependencies2() {
        Set<String> circles = new LinkedHashSet<>();
        Set<String> checked = new HashSet<>();
        Map<String, List<String>> parentAndChildren = new HashMap<>();
        for (LinkedNode<T> node : data.values()) {
            _findCircles(node, circles, new ArrayList<>(), parentAndChildren);
        }
        return circles;
    }

    public void _findCircles(LinkedNode<T> record, Set<String> circles, List<String> log, Map<String, List<String>> parentAndChildren) {
        String currentId = record.getId();
        parentAndChildren.computeIfAbsent(currentId, k -> new ArrayList<>());
        List<String> currentDILog = new ArrayList<>(log);
        if (currentDILog.contains(currentId)) {
            circles.add(currentId);
            return;
        }
        currentDILog.add(currentId);
        if (!record.getChildren().isEmpty()) {
            for (LinkedNode<T> child : record.getChildren()) {
                if (parentAndChildren.get(currentId).contains(child.getId())) continue;
                _findCircles(child, circles, currentDILog, parentAndChildren);
                parentAndChildren.get(currentId).add(child.getId());
            }
        }
    }
}
