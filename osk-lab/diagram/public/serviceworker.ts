export default null;
declare let self: ServiceWorkerGlobalScope;
self.addEventListener('push', (event: PushEvent) => {
  // event는 서버에서 payload로 보내준 데이터
  const { title, body } = JSON.parse(event.data?.text() ?? '{}');
  // https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration/showNotification
  self.registration.showNotification(title, { body });
});
self.addEventListener('notificationclick', (event) => {
  event.notification.close();

  const urlToOpen = 'http://127.0.0.1:5173';
  event.waitUntil(
    self.clients
      .matchAll({
        type: 'window',
        includeUncontrolled: true
        // 현재 서비스워커 클라이언트와 동일한 origin의 클라이언트를 포함시킬지 여부
        // 활성화하지 않을 경우, 현재 열린 탭이 있더라도 서비스워커를 활성화시킨 탭이 아니면 client에 포함되지 않음
      })
      .then((clientList) => {
        if (clientList.length > 0) {
          // 이미 열려있는 탭이 있는 경우
          return clientList[0]
            .focus();
        } else return self.clients.openWindow(urlToOpen);
      })
  );
});
