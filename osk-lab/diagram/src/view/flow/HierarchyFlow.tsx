import React, { useCallback, useEffect, useMemo, useState } from 'react';
import ReactFlow, { addEdge, ConnectionLineType, useEdgesState, useNodesState } from 'reactflow';
import 'reactflow/dist/style.css';

import { type Connection } from '@reactflow/core/dist/esm/types/general';
import { makeLayout } from './func/layout';
import { jsx } from '@emotion/react';
import JSX = jsx.JSX;

import workflows from '../../../public/experiment/workflow.json' assert { type: 'json' };
import callHierarchy from '../../../public/experiment/callHierarchy.json' assert { type: 'json' };
import { CallHierarchyConverter, WorkflowConverter } from './func/Converters';
import { useEventCallback } from '@mui/material';

const LayoutFlow = (): JSX.Element => {
  const [dataType, setDataType] = useState(false);

  const [nodes, setNodes, onNodesChange] = useNodesState([]);
  const [edges, setEdges, onEdgesChange] = useEdgesState([]);

  useEffect(() => {
    // @ts-expect-error
    const { nodes, edges } = dataType ? WorkflowConverter.from(workflows) : CallHierarchyConverter.from(callHierarchy);
    setNodes(nodes);
    setEdges(edges);
  }, [dataType]);

  const onConnect = useCallback(
    (params: Connection) => {
      setEdges((egs) =>
        addEdge({ ...params, type: ConnectionLineType.Step, animated: true }, egs)
      );
    }, []);

  const onLayout = useCallback(
    (direction: 'TB' | 'LR') => {
      const { nodes: layoutedNodes, edges: layoutedEdges } = makeLayout(
        nodes,
        edges,
        direction
      );

      setNodes([...layoutedNodes]);
      setEdges([...layoutedEdges]);
    }, [nodes, edges]);

  const onToggleData = useEventCallback(() => setDataType((current) => !current));

  return (
    <div style={{ width: 1500, height: 1000 }}>
      <button onClick={onToggleData}>
        Toggle
      </button>
      <ReactFlow
        nodes={nodes}
        edges={edges}
        onNodesChange={onNodesChange}
        onEdgesChange={onEdgesChange}
        // onConnect={onConnect}
        connectionLineType={ConnectionLineType.SmoothStep}
        fitView
      />
      <div className="controls">
        {/* <button onClick={() => { */}
        {/*  onLayout('TB'); */}
        {/* }} */}
        {/* > */}
        {/*  vertical layout */}
        {/* </button> */}
        {/* <button onClick={() => { */}
        {/*  onLayout('LR'); */}
        {/* }}>horizontal layout */}
        {/* </button> */}
      </div>
    </div>
  );
};

export default LayoutFlow;
