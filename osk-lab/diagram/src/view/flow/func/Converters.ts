import { type CallHierarchy, type Workflow } from '../../../model/flow/flow';
import { ConnectionLineType, type Edge, type Node } from 'reactflow';
import { makeLayout } from './layout';
import { cloneDeep, omit } from 'lodash';

export interface ReactFlowGroup {
  key: string
  children: string[]
}

export class WorkflowConverter {
  groups: Map<number, ReactFlowGroup[]> = new Map<number, ReactFlowGroup[]>();

  workflow: Workflow;
  nodes: Node[];
  edges: Edge[];
  private constructor (workflow: Workflow) {
    this.workflow = workflow;
    const { nodes, edges } = this.nodeAndEdges(workflow);
    const { nodes: layoutNode, edges: layoutEdges } = makeLayout(nodes, edges);
    this.nodes = layoutNode;
    this.edges = layoutEdges;
    // this.nodes = [...this.makeGroupNodes(), ...layoutNode];
  }

  static from (workflow: Workflow): WorkflowConverter {
    return new WorkflowConverter(workflow);
  }

  private copyWithoutChildren (flow: Workflow): Omit<Workflow, 'children'> {
    return cloneDeep(omit(flow, ['children']));
  };

  private makeGroupNodes (): Node[] {
    const groupNodes: Node[] = [];
    for (const [depth, groups] of this.groups.entries()) {
      for (const group of groups) {
        const node: Node = {
          id: `${group.key}-${depth}`,
          data: { label: group.key, children: group.children },
          position: { x: 0, y: 0 }
        };
        const key = `${group.key}-${depth}`;
        this.locateGroupNode(key, node);
        groupNodes.push(node);
      }
    }
    return groupNodes;
  }

  private locateGroupNode (groupKey: string, node: Node): void {
    const defaultPosition = { x: 0, width: 0, y: 0, height: 0 };
    const filtered = this.nodes.filter((node) => node.parentNode === groupKey);
    filtered.forEach((node) => {
      const { width, height, position } = node;
      if (!width || !height) {
        throw new Error('set width and height');
      }
      const { x, y } = position;
      defaultPosition.x = x > defaultPosition.x ? x : defaultPosition.x;
      defaultPosition.y = y < defaultPosition.y ? y : defaultPosition.y;
      defaultPosition.width = width < defaultPosition.width ? defaultPosition.width : width;
      defaultPosition.height = filtered.length * 100 + 30;
    });
    defaultPosition.x = defaultPosition.x - 15;
    defaultPosition.y = defaultPosition.y - 15;
    node.position = defaultPosition;
    node.style = { ...node.style, width: defaultPosition.width + 30, height: defaultPosition.height + 30 };
  }

  private _toFlowNodeAndEdge (parent: Workflow, nodes: Node[], edges: Edge[], depth = 0): void {
    // this.extractFromFlow(parent, depth);
    const parentNode: Node = {
      id: parent.callHierarchyRefKey,
      data: { ...this.copyWithoutChildren(parent), label: `${parent.group.key} \n\n ${parent.key}` },
      width: 200,
      height: 100,
      style: { width: 200, height: 100, whiteSpace: 'pre-line' },
      position: { x: 0, y: 0 }
      // parentNode: `${parent.group.key}-${depth}` || undefined
    };
    nodes.push(parentNode);
    if (parent.children == null) return;
    for (const child of parent.children) {
      const parentId = parent.callHierarchyRefKey;
      const childId = child.callHierarchyRefKey;

      const edge: Edge = {
        id: `${parentId}->${childId}`,
        source: parentId,
        target: childId,
        type: ConnectionLineType.Step
      };
      edges.push(edge);
      this._toFlowNodeAndEdge(child, nodes, edges, depth + 1);
    }
  };

  private nodeAndEdges (data: Workflow): { nodes: Node[], edges: Edge[] } {
    const nodes: Node[] = [];
    const edges: Edge[] = [];
    this._toFlowNodeAndEdge(data, nodes, edges);
    return { nodes, edges };
  }

  private extractFromFlow (workflow: Workflow, depth: number): void {
    const key = workflow.group.key;
    const currentGroup: ReactFlowGroup = this.groups.get(depth)?.find((g) => g.key === key) ?? { key, children: [] };
    currentGroup.children.push(workflow.callHierarchyRefKey);
    if (!this.groups.get(depth)) {
      this.groups.set(depth, [currentGroup]);
    }
    if (workflow.children !== null) {
      workflow.children.forEach((f) => this.extractFromFlow(f, depth));
    }
  };
}

export class CallHierarchyConverter {
  hierarchy: CallHierarchy;
  nodes: Node[];
  edges: Edge[];

  private constructor (hierarchy: CallHierarchy) {
    this.hierarchy = hierarchy;
    const { nodes, edges } = this.nodeAndEdges(hierarchy);
	  const layout = makeLayout(nodes, edges);
    this.nodes = layout.nodes;
    this.edges = layout.edges;
  }

  static from (from: CallHierarchy): CallHierarchyConverter {
    return new CallHierarchyConverter(from);
  }

  private copyWithoutChildren (flow: CallHierarchy): Omit<CallHierarchy, 'children'> {
    return cloneDeep(omit(flow, ['children']));
  };

  private _toFlowNodeAndEdge (parent: CallHierarchy, nodes: Node[], edges: Edge[], depth = 0, sequence = 0): void {
	  const parentId = `${depth}-${sequence}-${parent.key}`;
	  const parentNode: Node = {
      id: parentId,
      data: { ...this.copyWithoutChildren(parent), label: `${parent.classSignature.name}` },
      width: 200,
      height: 100,
      style: { width: 200, height: 100, whiteSpace: 'pre-line' },
      position: { x: 0, y: 0 }
      // parentNode: `${parent.group.key}-${depth}` || undefined
    };
    nodes.push(parentNode);
    if (parent.children == null) return;
    let seq = 0;
    while (seq < parent.children.length) {
      const child = parent.children[seq];
      const childId = `${depth + 1}-${seq}-${child.key}`;

      const edge: Edge = {
        id: `${parentId}->${childId}`,
        source: parentId,
        target: childId,
        type: ConnectionLineType.Step
      };
      edges.push(edge);
      this._toFlowNodeAndEdge(child, nodes, edges, depth + 1, seq);
      seq++;
    }
  };

  private nodeAndEdges (data: CallHierarchy): { nodes: Node[], edges: Edge[] } {
    const nodes: Node[] = [];
    const edges: Edge[] = [];
    this._toFlowNodeAndEdge(data, nodes, edges);
    return { nodes, edges };
  }
}
