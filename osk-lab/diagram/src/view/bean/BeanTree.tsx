import {memo, useEffect, useState} from "react";
import TreeNode from "primereact/treenode";

import data from "../../../public/experiment/mcoo.json";
import {Tree} from "primereact/tree";
import {connectNodes} from "../../model/bean/func";
import {toOrgChartData} from "../../model/bean/orgChartFunc";
import {UnlinkedNode} from "../../model/bean/types";

const linked = connectNodes(new Map<string, UnlinkedNode>(data.map(data => [data.id, data])));
const chart = toOrgChartData(linked, false);

const BeanTree = (() => {
    const [treeNodes, setTreeNodes] = useState<TreeNode[]>([]);
    useEffect(() => setTreeNodes(chart as TreeNode[]), []);
    return (
        <div className="card flex flex-wrap justify-content-center gap-5">
            <Tree value={treeNodes} filter filterMode="strict" filterPlaceholder="Strict Filter" className="w-full md:w-30rem" />
        </div>
    );
})

export default BeanTree;