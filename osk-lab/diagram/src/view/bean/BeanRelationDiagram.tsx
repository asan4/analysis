import {memo, useMemo, useState} from "react";
import data from "../../../public/experiment/mcoo.json" assert { type: 'json' }
import {ForceGraph2D} from "react-force-graph";
import {connectNodes, filterConnectedNodes, toForceGraphData} from "../../model/bean/func";
import {UnlinkedNode} from "../../model/bean/types";



const BeanRelationDiagram = memo(() => {

    const connectedNodes = useMemo(() => connectNodes(new Map<string, UnlinkedNode>(data.map(data => [data.id, data]))), []);
    // const connectedNodes = useMemo(() => connectNodesWithFilter(new Map<string, UnlinkedNode>(data.map(data => [data.id, data])), (node) => node.name === 'connectNodesWithFilter'), []);
    const [graphData, setGraphData] = useState(toForceGraphData(filterConnectedNodes(connectedNodes, node => node.name === "AdmissionDecisionController")));
    return (
        <div>
            <ForceGraph2D
                graphData={graphData}
                nodeLabel={'id'}
                // nodeAutoColorBy="group"
                // nodeCanvasObject={(node, ctx, globalScale) => {
                //     const label = node.id;
                //     const fontSize = 12/globalScale;
                //     ctx.font = `${fontSize}px Sans-Serif`;
                //     const textWidth = ctx.measureText(label as string).width;
                //     const bckgDimensions = [textWidth, fontSize].map(n => n + fontSize * 0.2); // some padding
                //
                //     ctx.fillStyle = 'rgba(255, 255, 255, 0.8)';
                //     ctx.fillRect(node.x? - bckgDimensions[0] / 2, node.y? - bckgDimensions[1] / 2, ...bckgDimensions);
                //
                //     ctx.textAlign = 'center';
                //     ctx.textBaseline = 'middle';
                //     ctx.fillStyle = node.color;
                //     ctx.fillText(label, node.x, node.y);
                 //
                //     node.__bckgDimensions = bckgDimensions; // to re-use in nodePointerAreaPaint
                // }}
                // nodePointerAreaPaint={(node, color, ctx) => {
                //     ctx.fillStyle = color;
                //     const bckgDimensions = node.__bckgDimensions;
                //     bckgDimensions && ctx.fillRect(node.x - bckgDimensions[0] / 2, node.y - bckgDimensions[1] / 2, ...bckgDimensions);
                // }}
            />
        </div>
    )
});

export default BeanRelationDiagram;