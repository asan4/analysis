import React, { memo, useCallback } from 'react';
import ReactFlow, {
  Background,
  useNodesState,
  useEdgesState,
  addEdge
} from 'reactflow';
import 'reactflow/dist/style.css';
import { type Connection } from '@reactflow/core/dist/esm/types/general';
import json from '../../../public/experiment/mcoo.json' assert { type: 'json' };
import { connectNodes } from '../../model/bean/func';
import { toReactFlowData } from '../../model/bean/flowChartFunc';
import { type UnlinkedNode } from '../../model/bean/types';

const initialEdges = [{ id: 'e1-2', source: '1', target: '2' }];
const data = connectNodes(new Map<string, UnlinkedNode>(json.map(data => [data.id, data])));
const tree = toReactFlowData(data);
let idx = 0;
const BeanRelationFlow = memo(() => {
  BeanRelationFlow.displayName = 'BeanRelationFlow';
  const [nodes, setNodes, onNodesChange] = useNodesState(tree[0]);
  console.log(tree[0]);
  const [edges, setEdges, onEdgesChange] = useEdgesState(initialEdges);

  const onConnect = useCallback((params: Connection) => { setEdges((eds) => addEdge(params, eds)); }, [setEdges]);
  const onClickNext = useCallback(() => {
    idx++;
    setNodes(tree[idx]);
  }, [setNodes]);

  return (
    <div style={{ width: 5000, height: 3000 }}>
      <button onClick={onClickNext}>Next</button>
      <ReactFlow
        nodes={nodes}
        edges={edges}
        onNodesChange={onNodesChange}
        // onEdgesChange={onEdgesChange}
        // onConnect={onConnect}
      >
        {/* <MiniMap/> */}
        {/* <Controls/> */}
         <Background/>
      </ReactFlow>
    </div>
  );
});

export default BeanRelationFlow;
