import {memo, useState} from "react";
import {OrganizationChart} from "primereact/organizationchart";
import data from "../../../public/experiment/mcoo.json";
import {connectNodes, countLeafNodes} from "../../model/bean/func";
import {UnlinkedNode} from "../../model/bean/types";
import {toOrgChartData} from "../../model/bean/orgChartFunc";

const linked = connectNodes(new Map<string, UnlinkedNode>(data.map(data => [data.id, data])));
let num = 0;
const node = linked.get('kr.amc.amis.mc.oo.os.rest.AdmissionDecisionController');
const rtn = countLeafNodes(node!, {num: 0, filter: new Set<string>()})
num = num + rtn.num;


const chart = toOrgChartData(linked);
const BeanOrgChart = memo(() => {
    const [chartData, setChartData] = useState(chart);

    return (
        <div className="card overflow-x-auto">

            <OrganizationChart
                value={chartData}
            />
        </div>
    );
})

export default BeanOrgChart;