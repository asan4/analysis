import { VAPID_PUBLIC_KEY } from './key';
import axios from 'axios';

export const subscribePushService: Readonly<Function> = (): void => {
  void Notification.requestPermission().then((status) => {
    if (status === 'denied') {
      alert('Notification 거부됨');
    } else if (navigator.serviceWorker) {
      void navigator.serviceWorker
        .register('./serviceworker.js') // serviceworker 등록
        .then(async (registration) => {
          const subscribeOptions = {
            userVisibleOnly: true,
            // push subscription이 유저에게 항상 보이는지 여부. 알림을 숨기는 등 작업의 포함 여부인데, 크롬에서는 true만 지원
            // https://developers.google.com/web/fundamentals/push-notifications/subscribing-a-user
            applicationServerKey: VAPID_PUBLIC_KEY // 발급받은 vapid public key
          };

          return await registration.pushManager.subscribe(subscribeOptions);
        })
        .then((pushSubscription) => {
          // subscription 정보를 저장할 서버로 전송
          axios.post('/api/push/sub',
            pushSubscription,
            {
              headers: {
                'Access-Control-Allow-Origin': '*'
              }
            }
          )
            .catch(() => alert('push subscription failed'));
        });
    }
  });
};
