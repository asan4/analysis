export const generateColor = (amount: number) => {
    let i = 0;
    const colors: string[] = [];
    while (i < amount) {
        const randomColor = _generateRandomColor();
        if (!colors.find((color) => randomColor === color)) {
            colors.push(randomColor);
            i++;
        }
    }
    return colors;
}

const _generateRandomColor = () => {
    return '#' + Math.round(Math.random() * 0xffffff).toString();
}