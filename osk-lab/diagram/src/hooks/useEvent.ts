import {useCallback, useLayoutEffect, useRef} from "react";


// almost same as useEventCallback
export function useEvent(handler: Function) {
    const handlerRef = useRef(handler)

    useLayoutEffect(() => {
        handlerRef.current = handler
    })

    return useCallback((...args: unknown[]) => {
        const fn = handlerRef.current
        return fn(...args)
    }, [])
}