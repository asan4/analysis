export declare type LinkedBasicNode = {
    id: string;
    name: string;
    comment: string | null;
    children: LinkedBasicNode[];
    parents: LinkedBasicNode[];
}

export declare type UnlinkedNode = Pick<LinkedBasicNode, 'id' | 'name' | 'comment'> & {
    childrenIds: string[];
    parentsIds: string[];
}
