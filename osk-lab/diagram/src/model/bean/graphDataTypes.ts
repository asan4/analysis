
export type NodeObject = object & {
    id?: string | number;
    x?: number;
    y?: number;
    vx?: number;
    vy?: number;
    fx?: number;
    fy?: number;
    [anything: string]: any;
};

export type LinkObject = object & {
    source?: string | number | NodeObject;
    target?: string | number | NodeObject;
};

export interface GraphData {
    nodes: NodeObject[];
    links: LinkObject[];
}
