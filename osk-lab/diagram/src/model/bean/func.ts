import { type LinkedBasicNode, type UnlinkedNode } from './types'
import { type GraphData, type LinkObject, type NodeObject } from './graphDataTypes'

export const convertToEmptyLinkedNode = (node: UnlinkedNode): LinkedBasicNode => ({
  id: node.id,
  name: node.name,
  comment: node.comment,
  parents: [],
  children: []
})
export const connectNodes = (nodes: Map<string, UnlinkedNode>) => {
  const connected = new Map<string, LinkedBasicNode>()
  nodes.forEach((node) => connected.set(node.id, convertToEmptyLinkedNode(node)))
  nodes.forEach((node) => {
    const linked = connected.get(node.id)
    if (linked == null) throw new Error('???????')
    node.childrenIds.forEach((childId) => connected.get(childId)?.parents.push(linked))
    node.parentsIds.forEach((parentId) => connected.get(parentId)?.children.push(linked))
  })
  return connected
}

const _filterMap = (nodes: Map<string, LinkedBasicNode>, targetNode: LinkedBasicNode, to: Map<string, LinkedBasicNode>, filter: Set<string>) => {
  if (filter.has(targetNode.id)) {
    return to
  }
  filter.add(targetNode.id)
  to.set(targetNode.id, targetNode)
  for (const child of targetNode.children) {
    _filterMap(nodes, child, to, filter)
  }
  return to
}

export const filterConnectedNodes = (nodes: Map<string, LinkedBasicNode>, predicate: (node: LinkedBasicNode) => boolean) => {
  const node = Array.from(nodes.values()).find(predicate)
  if (node == null) throw new Error('????????????????????????????????????')
  return _filterMap(nodes, node, new Map(), new Set())
}

export const toForceGraphData = (nodes: Map<string, LinkedBasicNode>): GraphData => {
  const nodeObjects: NodeObject[] = []
  const linkObjects: LinkObject[] = []
  // let group = 1;
  for (const [id, node] of nodes.entries()) {
    const nodeObject: NodeObject = { id }
    nodeObjects.push(nodeObject)
    if (node.children.length > 0) {
      linkObjects.push(
        ...node.children.map(node => ({
          source: nodeObject,
          target: node.id
        }))
      )
    }

    // group++
  }
  return { nodes: nodeObjects, links: linkObjects }
}

export const countLeafNodes = (node: LinkedBasicNode, rtn: { filter: Set<string>, num: number }) => {
  if (node.children.length === 0) {
    rtn.num = rtn.num + 1
  }
  if (rtn.filter.has(node.id)) {
    return rtn
  }
  rtn.filter.add(node.id)
  for (const child of node.children) {
    countLeafNodes(child, rtn)
  }
  return rtn
}
