import { type LinkedBasicNode } from './types';
import { ConnectionLineType, type Edge, type Node } from 'reactflow';
import { type Workflow } from '../flow/flow';
import { cloneDeep, omit } from 'lodash';
import { makeLayout } from '../../view/flow/func/layout';

interface TreeNode {
  id: string
  name: string
  comment: string | null
  parentNode: TreeNode | null
  children: TreeNode[]
}

const _applyChildren = (parentNode: LinkedBasicNode | null, currentNode: LinkedBasicNode, addTo: Node[], x: number, y: number, duplicationFilter: Set<string>): Node[] => {
  const { id, name, comment, parents } = currentNode;
  const flowNode: Node = {
    id, position: { x: x * 200, y: y * 60 }, data: { label: name, comment }
    // parentNode: parentNode?.id
  };
  if (duplicationFilter.has(flowNode.id)) {
    addTo.push({ ...flowNode, id: `${id}`, data: { label: `${name}` } });
    return addTo;
  }
  duplicationFilter.add(id);
  addTo.push(flowNode);
  if (currentNode.children.length > 0) {
    currentNode.children.forEach((_node, idx) => {
      if (idx !== 0) x++;
      _applyChildren(currentNode, _node, addTo, x, y + 1, duplicationFilter);
    });
  } else duplicationFilter = new Set<string>();
  return addTo;
};

const _linkedNodeToTreeNode = (parentNode: TreeNode | null, childNode: LinkedBasicNode, circleFilter: Set<string>): TreeNode => {
  const { id, name, comment, children } = childNode;
  const node: TreeNode = { id, name, comment, parentNode, children: [] };
  // const node: TreeNode = { id, name, comment,  children: [] };
  if (circleFilter.has(id)) return { ...node, id: `${id}`, name: `${name}` };
  circleFilter.add(id);
  for (const child of children) {
    node.children.push(_linkedNodeToTreeNode(node, child, circleFilter));
  }
  return node;
};

export const toReactFlowData = (data: Map<string, LinkedBasicNode>): Node[][] => {
  const resourceFlowNodes: Node[][] = [];

  const controllers = Array.from(data.values()).filter((node) => node.name.endsWith('Controller'));
  controllers.forEach((rsc, idx) => {
    resourceFlowNodes.push(_applyChildren(null, rsc, [], 0, 0, new Set<string>()));
  });
  return resourceFlowNodes;
};
const copyWithoutChildren = (flow: Workflow): Omit<Workflow, 'children'> => cloneDeep(omit(flow, ['children']));
const extractFromFlow = (result: Record<string, string[]>, workflow: Workflow): Record<string, string[]> => {
  const groupKey = workflow.group.key;
  if (result[groupKey] !== undefined) {
    result[groupKey].push(workflow.callHierarchyRefKey);
    return result;
  } else {
    result[groupKey] = [workflow.callHierarchyRefKey];
  }
  if (workflow.children !== null) {
    workflow.children.forEach((f) => extractFromFlow(result, f));
  }
  return result;
};
const _findGroups = (flow: Workflow | Workflow[]): Record<string, string[]> => {
  const result: Record<string, string[]> = {};
  if (Array.isArray(flow)) {
    flow.forEach((workflow) => extractFromFlow(result, workflow));
  } else {
    extractFromFlow(result, flow);
  }
  return result;
};
const _toFlowNodeAndEdge = (parent: Workflow, nodes: Node[], edges: Edge[]): void => {
  const parentNode: Node = {
    id: parent.callHierarchyRefKey,
    data: { ...copyWithoutChildren(parent), label: `${parent.group.key} \n\n ${parent.key}` },
    width: 200,
    height: 100,
    style: { width: 200, height: 100, whiteSpace: 'pre-line' },
    position: { x: 0, y: 0 }
  };
  nodes.push(parentNode);
  if (parent.children == null) return;
  for (const child of parent.children) {
    const parentId = parent.callHierarchyRefKey;
    const childId = child.callHierarchyRefKey;

    const edge: Edge = {
      id: `${parentId}->${childId}`,
      source: parentId,
      target: childId,
      type: ConnectionLineType.Step
    };
    edges.push(edge);
    _toFlowNodeAndEdge(child, nodes, edges);
  }
};

export const toFlowNodeAndEdge = (data: Workflow): { nodes: Node[], edges: Edge[] } => {
  const nodes: Node[] = [];
  const edges: Edge[] = [];
  _toFlowNodeAndEdge(data, nodes, edges);
  return { nodes, edges };
};
