import {OrganizationChartNodeData} from "primereact/organizationchart";
import {LinkedBasicNode, UnlinkedNode} from "./types";
import {circles} from "../../temp";
import {CSSProperties} from "react";
import {generateColor} from "../../functions";

const colors = generateColor(circles.length);
const colorMap = new Map(circles.map((circle, idx) => [circle, colors[idx]]))
let asdf = 0;
const _toOrgChartData = (linkedData: Map<string, LinkedBasicNode>, node: LinkedBasicNode, dupFilter: Set<string>, color: boolean) => {
    const { id, name, comment, children } = node;
    const chartNode: OrganizationChartNodeData & { style?: CSSProperties, icon?: string, key: string, data: any } = {
        label: name,
        // label: `${name}${asdf}`,
        expanded: true,
        // id: colors.includes(id) ? id + `${Math.floor(Math.random())}` : id,
        key: crypto.randomUUID(),
        data: name,
        icon: 'pi pi-fw pi-calendar-plus',
        style: color ? {backgroundColor: colorMap.get(name)} : { },
    };
    if (!children.length) {
        asdf++
        chartNode.label = `${chartNode.label}${asdf}`
    }
    // asdf++
    if (circles.includes(name)) {
        chartNode.style = { backgroundColor: colorMap.get(name) };
    }
    if (dupFilter.has(id)) {
        return chartNode;
    }

    dupFilter.add(id);
    if (children.length) {
        chartNode.children = [];
        for (const child of children) {
            chartNode.children.push(_toOrgChartData(linkedData, child, dupFilter, color));
        }
    }
    return chartNode;
}
export const toOrgChartData = (linkedData: Map<string, LinkedBasicNode>, color: boolean = false): OrganizationChartNodeData[] => {
    const chartData: OrganizationChartNodeData[] = [];
    // for (const [key, value] of linkedData.entries()) {
    //     _toOrgChartData(linkedData, value, chartData);
    // }
    const target = Array.from(linkedData.values()).find((value) => value.name === 'AdmissionDecisionController');
    if (!target) throw new Error("?????????????");
    const orgData = _toOrgChartData(linkedData, target, new Set<string>(), color);
    return [orgData];
}