export interface Workflow {
  key: string
  tags: string[] | null
  description: string
  callHierarchyRefKey: string
  group: WorkflowGroup
  children: Workflow[] | null
}

export interface WorkflowGroup {
  key: string
  tags: string[]
  description: string
};

export interface CallHierarchy {
  key: string
  classSignature: CallClassSignature
  methodSignature: CallMethodSignature
  children: CallHierarchy[] | null
};

export interface CallClass {
  key: string
  name: string
  feature: {
    isController: boolean
    isService: boolean
    isExtService: boolean
    isMapper: boolean
  }
  methods: CallMethod[]
  fieldMap: Record<string, string>
};

export interface CallClassSignature {
  key: string
  name: string
  feature: null | {
    isController: boolean
    isService: boolean
    isExtService: boolean
    isMapper: boolean
  }
  taggedInfo: TaggedInfo | null
}

export interface CallMethodSignature {
  key: string
  name: string
  paramTypes: string[] | null
  returnType: string
  taggedInfo: TaggedInfo | null
};

export type TaggedInfo = Record<string, any> | null;

export interface CallMethod {
  key: string
  name: string
  feature: {
    isGetter: boolean
    isSetter: boolean
  }
  paramTypes: string[]
  returnType: string
  // TODO: serialize가 안 될 텐데
  methodDeclaration: unknown
  nextCallMethodKeys: string[]
  taggedInfo: TaggedInfo
  variableMap: Record<string, string>
};
