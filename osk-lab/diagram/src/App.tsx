import React, { useMemo, useState } from 'react';
import './App.css';
import './index.css';
import 'primereact/resources/themes/lara-light-indigo/theme.css';
import 'primereact/resources/primereact.css';
import 'primeicons/primeicons.css';
import { FormControl, MenuItem, Select, type SelectChangeEvent, useEventCallback } from '@mui/material';
import BeanOrgChart from './view/bean/BeanOrgChart';
import BeanRelationDiagram from './view/bean/BeanRelationDiagram';
import BeanTree from './view/bean/BeanTree';
import BeanRelationFlow from './view/bean/BeanRelationFlow';
import LayoutFlow from './view/flow/HierarchyFlow';

enum Current {
  Relation = 'Relation', Org = 'Org', Tree = 'Tree', Flow = 'Flow', Hierarchy = 'Hierarchy'
}
function App (): JSX.Element {
  const [current, setCurrent] = useState<Current>(Current.Relation);

  const currentPage = useMemo(() => {
    switch (current) {
      case Current.Org:
        return <BeanOrgChart key={'org'}/>;
      case Current.Relation:
        return <BeanRelationDiagram key={'relation'}/>;
      case Current.Tree:
        return <BeanTree key={'tree'}/>;
      case Current.Flow:
        return <BeanRelationFlow key={'flow'}/>;
      case Current.Hierarchy:
        return <LayoutFlow key={'hierarchy'}/>;
      default:
        throw new Error('?!?!?!?!?!?!?!?!?!?!?!');
    }
  }, [current]);

  const onChangeCurrent = useEventCallback((event: SelectChangeEvent<Current>) => {
    setCurrent(event.target.value as Current);
  });

  return (
    <div className="App">
      <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
      <Select
          onChange={onChangeCurrent}
          label={'type'}
          value={current}
      >
        {Object.values(Current).map((cur) => <MenuItem value={cur} key={cur}>{cur}</MenuItem>)}
      </Select>
      </FormControl>
      {currentPage}
    </div>
  );
}

export default App;
