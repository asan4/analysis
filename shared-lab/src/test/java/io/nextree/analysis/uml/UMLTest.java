package io.nextree.analysis.uml;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.nextree.analysis.plantuml.sequence.builder.SequenceBuilder;
import io.nextree.analysis.plantuml.sequence.converter.factory.WorkflowFunctionFactory;
import io.nextree.analysis.plantuml.sequence.converter.impl.WorkflowConverter;
import io.nextree.analysis.plantuml.sequence.model.arrow.Arrow;
import io.nextree.analysis.plantuml.sequence.model.common.Alignment;
import io.nextree.analysis.plantuml.sequence.model.participant.Participant;
import io.nextree.analysis.plantuml.sequence.model.participant.vo.ParticipantType;
import io.nextree.analysis.plantuml.sequence.model.arrow.vo.Note;
import io.nextree.analysis.workflow.Workflow;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class UMLTest {

    private static final String del = ":";
    @Test
    public void makeTestSequence() {
        List<Participant> participants = new ArrayList<>();
        List<Arrow> arrows = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            participants.add(Participant.sample(i));
        }
        for (int i = 0; i < 4; i++) {
            arrows.add(Arrow.sample(i, participants.get(i).getKey(), participants.get(i + 1).getKey()));

        }
        SequenceBuilder builder = new SequenceBuilder();
        participants.forEach(builder::participant);
        for (int i = 0; i < 4; i++) {
            builder.arrow(arrows.get(i));
        }
        String string = builder.build();
        System.out.println(string);
    }

    @Test
    public void makeWorkflowUML() throws IOException {
        File file = new File("src/test/resources/experiment/workflow.json");
        ObjectMapper mapper = new ObjectMapper();
        Workflow workflow = mapper.readValue(file, Workflow.class);
        WorkflowConverter proxy = new WorkflowConverter(
                WorkflowFunctionFactory.defaultNoteSelector,
                WorkflowFunctionFactory.defaultParticipantTypeSelector,
                WorkflowFunctionFactory.defaultParticipantFilter
                );
        SequenceBuilder builder = proxy.from(workflow);
        String puml = builder.build();
        System.out.println(puml);
        writeAsText(puml);
        System.out.println("debug here");
    }

    @Test
    public void makeWorkflowWithoutMapper() throws IOException {
        File file = new File("src/test/resources/experiment/workflow.json");
        ObjectMapper mapper = new ObjectMapper();
        Workflow workflow = mapper.readValue(file, Workflow.class);
        WorkflowConverter proxy = new WorkflowConverter(
                WorkflowFunctionFactory.defaultNoteSelector,
                WorkflowFunctionFactory.defaultParticipantTypeSelector,
                (flow) -> flow.getSourceRef().split(del)[0].endsWith("Mapper")
        );
        SequenceBuilder builder = proxy.from(workflow);
        System.out.println(builder.build());
        System.out.println("debug here");
    }

    @Test
    public void makeWorkflowWithoutService() throws IOException {
        File file = new File("src/test/resources/experiment/workflow.json");
        ObjectMapper mapper = new ObjectMapper();
        Workflow workflow = mapper.readValue(file, Workflow.class);
        WorkflowConverter proxy = new WorkflowConverter(
                (flow) -> List.of(new Note(Alignment.right, flow.getKey())),
                (flow) -> flow.getSourceRef().split(del)[0].endsWith("Mapper") ? ParticipantType.database : ParticipantType.participant,
                (flow) -> flow.getSourceRef().split(del)[0].endsWith("Service")
        );
        SequenceBuilder builder = proxy.from(workflow);
        System.out.println(builder.build());
        System.out.println("debug here");
    }

    @Test
    public void test() {
        String a = "a";
        String b = "b";
        System.out.println(String.format("%s" + del + "%s", a, b));

    }

    private void writeAsText(String puml) {
        try(FileOutputStream fos = new FileOutputStream(String.join(File.separator, "src", "test", "resources", "experiment", "sequnece_diagram.puml"))) {
            fos.write(puml.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
