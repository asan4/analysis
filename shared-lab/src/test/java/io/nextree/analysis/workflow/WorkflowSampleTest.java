package io.nextree.analysis.workflow;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

class WorkflowSampleTest {

//    @Test
//    void sample() throws JsonProcessingException {
//        Workflow 처방전자인증_필요대상_Client_확인 = Workflow.builder()
//                .key("처방전자인증 필요대상 Client 확인")
//                .description("인증서 로그인 필요여부\n일괄인증 데이터서생성 필요여부")
//                .callHierarchyRefKey("CrtfcOrdrBizService:checkSignProcessInfo:OrdrElecCrtfcHdrDTO,PatOrdrSaveRsltDTO")
//                .children(List.of(
//                        Workflow.builder()
//                                .key("사용자 직군 체크")
//                                .description("사용자가 의사 또는 간호사 인지 여부 체크")
//                                .callHierarchyRefKey("CrtfcOrdrBizService:verifyUser:OrdrElecCrtfcHdrDTO")
//                                .children(List.of(
//                                        Workflow.builder()
//                                                .key("사용자 체크")
//                                                .description("사용자 체크")
//                                                .callHierarchyRefKey("CrtfcOrdrMapper:retrieveVerifyUser:OrdrElecCrtfcHdrDTO")
//                                                .build()
//                                ))
//                                .build(),
//                        Workflow.builder()
//                                .key("전자인증 필요여부")
//                                .description("전자인증이 필요한 화면인지 체크")
//                                .callHierarchyRefKey("CrtfcOrdrBizService:checkNeedCrtfc:OrdrElecCrtfcHdrDTO,String")
//                                .children(List.of(
//                                        Workflow.builder()
//                                                .key("처방 화면별 전자인증 적용여부 확인")
//                                                .description("처방 화면별 전자인증 적용여부 확인")
//                                                .callHierarchyRefKey("CrtfcOrdrMapper:retrieveCheckNeedCrtfc:OrdrElecCrtfcHdrDTO")
//                                                .build()
//                                ))
//                                .build()
//                ))
//
//                .build();
//
//        Workflow 환자정보조회 = Workflow.builder()
//                .key("환자정보 조회")
//                .description("환자정보 조회")
//                .callHierarchyRefKey("PatInfoBasicBizService:retrievePatInfoOrdr:PatInfoOrdrInqryParamDTO")
//                .children(List.of(
//                        Workflow.builder()
//                                .key("처방저장용 환자 정보 조회")
//                                .description("환자 조회")
//                                .callHierarchyRefKey("PatInfoBasicMapper:retrievePatInfoOrdr:PatInfoOrdrDTO")
//                                .build(),
//                        Workflow.builder()
//                                .key("전과일수조회")
//                                .description("입원환자의 경우 전과일수 조회")
//                                .callHierarchyRefKey("PatInfoBasicMapper:retrieveDeptrDays:PatInfoBasicDTO")
//                                .build()
//                ))
//                .build();
//
//
//
//
//
//        Workflow workflow = Workflow.builder()
//                .key("환자 처방 저장")
//                .description("환자에 대한 처방을 통합적으로 관리하기 위한 Biz Service이다.\n처방등록 화면에서 사용하는 서비스로써, TPN처방과 타과진료의뢰처방은 아래 서비스를 타지 않는다.")
//                .callHierarchyRefKey("PatOrdrManageBizService:savePatOrdr:PatOrdrSaveTO")
//                .children(List.of(
//                        처방전자인증_필요대상_Client_확인,
//                        환자정보조회
//                ))
//                .build();
//        String json = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(workflow);
//        System.out.println(json);
//
//        try(FileOutputStream fos = new FileOutputStream(String.join(File.separator, "src", "test", "resources", "experiment", "workflow.json"))) {
//            fos.write(json.getBytes(StandardCharsets.UTF_8));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
