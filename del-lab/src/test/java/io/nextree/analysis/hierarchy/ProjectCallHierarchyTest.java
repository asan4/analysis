package io.nextree.analysis.hierarchy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javaparser.ast.CompilationUnit;
import io.nextree.analysis.callhierarchy.dictionary.Dictionary;
import io.nextree.analysis.callhierarchy.CallHierarchy;
import io.nextree.analysis.callhierarchy.dictionary.MethodMeta;
import io.nextree.analysis.callhierarchy.qualification.QualifiedMethodSignature;
import io.nextree.analysis.staticanalysis.analyzer.StaticAnalyzer;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

public class ProjectCallHierarchyTest {
    private static final Path EXPERIMENT_PATH = Paths.get("src/test/resources/mc-oo/mc-oo");
    @Test
    void projectCallHierarchy() throws Exception{

        Map<String, CompilationUnit> units = StaticAnalyzer.analyze().parseProject(EXPERIMENT_PATH);
        Dictionary dictionary = StaticAnalyzer.analyze().makeCallDictionary(units);
        String methodKey = "kr.amc.amis.mc.oo.os.service.ProprietEvalBizService.saveProprietyEval_GLYC(kr.amc.amis.mc.oo.os.entity.ProprietyEvalDTO, java.lang.Integer)";

        CallHierarchy callHierarchy = CallHierarchy.makeCallHierarchy(dictionary, QualifiedMethodSignature.fromKey(methodKey));
        System.out.println("");
    }
}
