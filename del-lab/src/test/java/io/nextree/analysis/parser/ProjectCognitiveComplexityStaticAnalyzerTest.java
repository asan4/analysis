package io.nextree.analysis.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javaparser.ast.CompilationUnit;
import io.nextree.analysis.complexity.Analyzer;
import io.nextree.analysis.complexity.ClassType;
import io.nextree.analysis.complexity.ProjectCognitiveComplexityAnalyzer;
import io.nextree.analysis.complexity.SortingCriteria;
import io.nextree.analysis.complexity.dto.ProjectComplexity;
import io.nextree.analysis.staticanalysis.StaticAnalysis;
import io.nextree.analysis.staticanalysis.analyzer.StaticAnalyzer;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProjectCognitiveComplexityStaticAnalyzerTest {

    private static final Path EXPERIMENT_PATH = Paths.get("C:/Users/DAEUN/Desktop/amis/mc-oo/mc-oo-service");
    private static final ClassType CLASS_TYPE = ClassType.SERVICE;

    private static final String OUT_PATH = String.join(File.separator, "src", "main", "resources", "experiment", "output.json");
    private static final SortingCriteria SORTING_CRITERIA = SortingCriteria.AVG;

    @Test
    void cognitiveComplexity() throws Exception{
        Map<String, CompilationUnit> units = StaticAnalyzer.analyze().parseProject(EXPERIMENT_PATH);

        ProjectCognitiveComplexityAnalyzer projectCognitiveComplexityAnalyzer = Analyzer.analyze();
        ProjectComplexity projectComplexity = projectCognitiveComplexityAnalyzer.cognitiveComplexityInProject((List) units.values(), CLASS_TYPE, SORTING_CRITERIA);

        printCognitiveComplexity(projectComplexity);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(new File(OUT_PATH), projectComplexity);
    }

    void printCognitiveComplexity(ProjectComplexity projectComplexity) {
        System.out.println(projectComplexity.toString());
    }

    @Test
    public void sortTest() {
        List<Integer> numbers = new ArrayList<>(List.of(1,2,3,4,5,6,7));

        System.out.println(numbers);

        numbers.sort((a,b) -> b.compareTo(a));
        System.out.println(numbers);
    }
}
