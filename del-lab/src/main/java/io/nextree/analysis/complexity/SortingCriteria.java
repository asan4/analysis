package io.nextree.analysis.complexity;

public enum SortingCriteria {
    TOTAL,
    MAX,
    AVG
}
