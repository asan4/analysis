package io.nextree.analysis.complexity.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClassComplexity {
    private String name;
    private List<MethodComplexity> methods;
    private Statistics statistics;

    public ClassComplexity(String name, List<MethodComplexity> methods) {
        this.name = name;
        this.methods = methods;
        this.statistics = new Statistics(this);
    }
}
