package io.nextree.analysis.complexity.dto;

import io.nextree.analysis.complexity.ClassType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProjectComplexity {
    private ClassType type;
    private List<ClassComplexity> classes;

    @Override
    public String toString() {
        return classes.stream().map(clazz -> {
            Statistics classStatistics = clazz.getStatistics();
            return String.join("",
                    String.format("\n=== [%s] count: %d, total: %d, max: %d, avg: %.2f ===\n", clazz.getName(), classStatistics.getCount(), classStatistics.getTotal(), classStatistics.getMax(), classStatistics.getAvg()),
                    clazz.getMethods().stream().map(method -> String.format("[%s] : %d\n", method.getName(), method.getComplexity()))
                        .reduce("", (a, b) -> String.join("", a, b)));
        }).reduce("", (a, b) -> String.join("", a, b));
    }
}
