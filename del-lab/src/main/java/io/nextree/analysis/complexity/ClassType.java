package io.nextree.analysis.complexity;

public enum ClassType {
    ALL(""),
    MAPPER("Mapper"),
    SERVICE("Service"),
    EXT_SERVICE("ExtService"),
    CONTROLLER("Controller");

    private final String classType;

    ClassType(String classType) {
        this.classType = classType;
    }

    public String getClassType() {
        return classType;
    }
}
