package io.nextree.analysis.complexity.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Statistics {
    private int count;
    private Long total;
    private Long max;
    private double avg;

    public Statistics(ClassComplexity classComplexity) {
        calculate(classComplexity);
    }

    private void calculate(ClassComplexity classComplexity) {
        int count = classComplexity.getMethods().size();
        long total = 0L;
        long max = 0L;
        double avg = 0.0;

        for(MethodComplexity methodComplexity : classComplexity.getMethods()) {
            long complexity = methodComplexity.getComplexity();
            if(complexity > max)
                max = complexity;
            total += complexity;
        }

        if(count != 0)
            avg = total / (double)count;

        this.count = count;
        this.total = total;
        this.max = max;
        this.avg = avg;
    }
}
