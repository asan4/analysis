package io.nextree.analysis.complexity;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import io.nextree.analysis.complexity.dto.ClassComplexity;
import io.nextree.analysis.complexity.dto.MethodComplexity;
import io.nextree.analysis.complexity.dto.ProjectComplexity;
import io.nextree.analysis.staticanalysis.analyzer.StaticAnalyzer;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public interface ProjectCognitiveComplexityAnalyzer {
    List<String> methodBlackList = List.of("toString");
    List<String> classBlackList = List.of(".*TO$", ".*Mapper$");
    StaticAnalyzer STATIC_ANALYZER = StaticAnalyzer.analyze();
    BiFunction<String, ClassType, Boolean> classNamePredicate = (className, classType) -> {
        return classBlackList.stream().map(blackList -> !className.matches(blackList)).reduce(className.matches(".*"+classType.getClassType()+"$"), (a, b) -> a && b);
    };
    Predicate<MethodDeclaration> methodNamePredicate = m -> {
        return !methodBlackList.contains(m.getNameAsString());
    };

    default List<MethodComplexity> cognitiveComplexityInMethod(CompilationUnit cu) {
        ClassOrInterfaceDeclaration clazz = STATIC_ANALYZER.type(cu);
        List<MethodDeclaration> methods = STATIC_ANALYZER.methods(clazz, methodNamePredicate);

        return methods.stream().map(method ->new MethodComplexity(method.getNameAsString(), STATIC_ANALYZER.cognitiveComplexity(method))).collect(Collectors.toList());
    }

    default List<ClassComplexity> cognitiveComplexityInClass(List<CompilationUnit> units, ClassType classType) {
        return units.stream().filter(unit -> classNamePredicate.apply(STATIC_ANALYZER.canonicalName(unit), classType))
                .map(unit -> {
                    String className = STATIC_ANALYZER.canonicalName(unit);
                    List<MethodComplexity> methodComplexities = cognitiveComplexityInMethod(unit);
                    return new ClassComplexity(className, methodComplexities);
                }).collect(Collectors.toList());
    }

    default ProjectComplexity cognitiveComplexityInProject(List<CompilationUnit> units, ClassType classType, SortingCriteria sortingCriteria) {
        List<ClassComplexity> classComplexities = cognitiveComplexityInClass(units, classType);
        classComplexities = sortingStatistics(classComplexities, sortingCriteria);

        return new ProjectComplexity(classType, classComplexities);
    }

    default List<ClassComplexity> sortingStatistics(List<ClassComplexity> classComplexities, SortingCriteria sortingCriteria) {
        List<ClassComplexity> sortedClassComplexities = new ArrayList<>(classComplexities);

        switch (sortingCriteria){
            case TOTAL:
                sortedClassComplexities.sort(new TotalComparator());
                break;
            case MAX:
                sortedClassComplexities.sort(new MaxComparator());
                break;
            case AVG:
                sortedClassComplexities.sort(new AvgComparator());
                break;
        }
        return sortedClassComplexities;
    }

    class TotalComparator implements Comparator<ClassComplexity>{
        @Override
        public int compare(ClassComplexity o1, ClassComplexity o2) {
            if(o1.getStatistics().getTotal() < o2.getStatistics().getTotal())
                return 1;
            else
                return -1;
        }
    }

    class MaxComparator implements Comparator<ClassComplexity>{
        @Override
        public int compare(ClassComplexity o1, ClassComplexity o2) {
            if(o1.getStatistics().getMax() < o2.getStatistics().getMax())
                return 1;
            else
                return -1;
        }
    }

    class AvgComparator implements Comparator<ClassComplexity>{
        @Override
        public int compare(ClassComplexity o1, ClassComplexity o2) {
            if(o1.getStatistics().getAvg() < o2.getStatistics().getAvg())
                return 1;
            else
                return -1;
        }
    }

}
